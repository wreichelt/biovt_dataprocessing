%% Configure Standard Settings for Plotting 
% The new defaults will not take effect if there are any open figures. To
% use them, we close all figures, and then repeat the first example.
close all;

% Defaults for this blog post
alw = 0.75;     % AxesLineWidth
fsz = 12;       % Fontsize
fsxy = 12;      % Fontsize xyaxis
lw = 1.5;       % LineWidth
msz = 15;       % MarkerSize

% The properties we've been using in the figures
set(0,'defaultLineLineWidth',lw);       % set the default line width to lw
set(0,'defaultLineMarkerSize',msz);     % set the default line marker size to msz
set(0,'DefaultTextFontSize',fsz)        % set the default Fontsize to fsz
set(0,'DefaultAxesFontSize',fsxy)       % set the default axis fontsize to fsxy
set(0,'defaultAxesLineWidth',alw);      % set the default line width to lw

%% This script performs a principal component analysis

[r,s]=size(Data);
experiment_selection=[1,4,6,7,8,10,12]; % 
%experiment_selection=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24]; %all Experiments
%experiment_selection=[1,2,3,4,5,6,7,8,9,10,11,12,13,23,24]; % freeflow AP4
%experiment_selection=[14,15,16,17,19,20,21,22]; % controlled AP4
Matrix = Data (experiment_selection, 1:s);
Matrix = Matrix(all(~isnan(Matrix),2),:); %% Remove NaNs
vbls = headers (1, 1:s); % headers
[pc,score,latent,tsquare] = princomp(zscore(Matrix));
explained = (cumsum(latent)./sum(latent))*100;

%% Explained Variance
plot (explained (1:5, 1), '*')
xlabel ('Number of Principal Components [/]');
ylabel ('Explained Variance [%]') 

%% Plot first PC one and two     

biplot(pc(:,1:2),'scores',score(:,1:2),...
                    'varlabels',vbls);   
             
%set(findall(h,'type','text'),'fontSize',14)
%print('-djpeg','-r500')


%% Plot PC one and three     

biplot(pc(:,2:3),'scores',score(:,2:3),...
                    'varlabels',vbls);

%% Plot first three PCs
biplot(pc(:,1:3),'scores',score(:,1:3),...
                    'varlabels',vbls);
                
%% Create Score Plots

figure()
plot(score(:,1),score(:,3),'+')
xlabel('1st Principal Component')
ylabel('2nd Principal Component')





