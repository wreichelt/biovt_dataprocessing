% %% Tool in progress

% Finalise Output Figures (Specific Rates, Yields, Reconciled Rates, Yield) Maybe as function that can be called?  
% integrate sampling!

close all 
clear all


%%
%set path
Ex='WR33';
%path=['S:\CDLMIB\CD-L\03_RT3\03_Results\Experiments\' Experiment work_package '\Matlab Auswertung\'];    %specify path of experimental data
path=['S:\CDLMIB\CD_Data\Module_1\03_RT3\03_Results\Experiments\' Ex '\Matlab Auswertung\'];    %specify path of experimental data
% addpath('S:\BPT\wreichelt\Matlab\biovt_dataprocessing');        %specify path of matlab files
addpath('S:\BPT\BPT SHARED DATA\MSC\Markus Brillmann\bitbucket');
% addpath('S:\BPT\BPT SHARED DATA\MSC\Peter Thurrold\matlab');

cd(path);
%% load data

Ex_group_A=struct;
load(['Ex_' Ex])
eval(['Ex_group_A=Ex_' (Ex) ';'])
online_raw=Ex_group_A.corr;       %starts with induction phase!
offline_raw=Ex_group_A.analytics;

c_s=Ex_group_A.constants.c_s;                 % [g/l] Substrate concentration
mR_start=Ex_group_A.constants.mR_start;     %Reactor Vol [l]
MS=Ex_group_A.constants.MS;              % [g/c-mol] Molecular weight substrate  (30.6966 glycerol; 30 Glucose)
gamma_s=Ex_group_A.constants.gamma_s;          % DoR Substrate (4,667 glycerol; 4 Glucose)
c_n=Ex_group_A.constants.base_mol;        % [mol/l] base concentration
MAc=Ex_group_A.constants.MAc;
MNH4=Ex_group_A.constants.MH4;              %g/mol NH4
ramp_eval=0;

   %% Import Online Data for Rate calculation
k1=1;
Fex=fieldnames(Ex_group_A.induction);
k2=length(Fex);
raw_inter=[];
interpol_interval=0.1;

%Start = 12;    %set induction time according to point of induction of data import_generic

for k =k1:k2

    Fexn=Fex{k};                                         %name experiment
    ind= Ex_group_A.induction.(Fexn);                     %index of induction
    Start = floor(online_raw.(Fexn).time(ind,1)*10)/10;  
    Ende = (floor(max(offline_raw.(Fexn).DCW(:,1))*10))/10;

   xinterpol=(Start:interpol_interval:Ende);        %interpolation achsis starts with induction time!


Fon=fieldnames(online_raw.(Fexn));

    for j=2:length(Fon)                                 %time is skipped 
    
    Fonn=Fon{j};                                        %looping through variables
    raw_inter.(Fexn).(Fonn)(:,1)=xinterpol';
    raw_inter.(Fexn).(Fonn)(:,2)=interp1(online_raw.(Fexn).(Fonn)(:,1),online_raw.(Fexn).(Fonn)(:,2),xinterpol, 'linear');
    
    end   

Foff=fieldnames(offline_raw.(Fexn));
  
    for j=1:length(Foff)                                %time is skipped
    
    Foffn=Foff{j};                                       %looping through variables
    if isempty(offline_raw.(Fexn).(Foffn))              %skipps empty variables
    offline_raw.(Fexn)=rmfield(offline_raw.(Fexn), Foffn); 
    disp( ['removed ' Foffn ' from offline_raw'])       %deletes empty variable from offline_raw
    else
    raw_inter.(Fexn).(Foffn)(:,1)=xinterpol';
    raw_inter.(Fexn).(Foffn)(:,2)=interp1(offline_raw.(Fexn).(Foffn)(:,1),offline_raw.(Fexn).(Foffn)(:,2),xinterpol, 'linear');
    
    end   
    end
    
end





%% rate calculation
calc =[];
y_o2_in_air=20.95;       %  [%] of O2 in inlet air
y_co2_in_air=0.04;       %  [%] of CO2 in inlet air
y_o2_in_ox=100;          %  [%] of O2 in pure Ox
yO2wet=20.7;             % average y wet
MV=22.414;               % mol volume of ideal gas [L/mol]
MX=26.54;                % [g/c-mol] Molecular weight Biomass
gamma_x=4.13;            % DoR of Biomass
gamma_o2=-4;             % DoR of O2
yH2O = (y_o2_in_air-yO2wet)/y_o2_in_air*100;
order=5;                 %magnitude of smoothin higher value -> more smoothing
% %run('S:\BPT\BPT SHARED DATA\Projects\L_Feeding Strategies\02_Material_Methods\Matlab-Scripts\variable_attribution.m')       %for variable attribution
% run('variable_attribution.m')       %for variable attribution
% %run('variable_attribution_wo_product.m')       %for variable attribution
qsacclow=[];


for k=k1:k2
   Fexn=Fex{k};       
   
[temp,time_filtered_gradient]=filtered_gradient(raw_inter.(Fexn).Air(:,1),raw_inter.(Fexn).Air(:,1),order);

calc.(Fexn).time=raw_inter.(Fexn).Air(:,1);
calc.(Fexn).gassing(:,2)=(raw_inter.(Fexn).Air(:,2)+raw_inter.(Fexn).Ox_gassing(:,2));      %[l/h]
calc.(Fexn).y_o2_in(:,2)=(raw_inter.(Fexn).Air(:,2)*y_o2_in_air+raw_inter.(Fexn).Ox_gassing(:,2)*y_o2_in_ox)./calc.(Fexn).gassing(:,2);
calc.(Fexn).y_co2_in(:,2)=(raw_inter.(Fexn).Air(:,2)*y_co2_in_air)./calc.(Fexn).gassing(:,2);

    if Ex_group_A.scale.(Fexn) == 0                                                         % if no scale signal is available
        calc.(Fexn).vol_bal(:,2)=raw_inter.(Fexn).vR(:,2);                                  % [l] volume balance
        calc.(Fexn).sampling(:,2)=raw_inter.(Fexn).sampling(:,2);                           % [l]sampling according to offline data
        calc.(Fexn).sampling_r(:,2)=gradient(calc.(Fexn).sampling(:,2),calc.(Fexn).time,interpol_interval);   % [l/h]sampling rate according to offline data
        calc.(Fexn).mR(:,2)=raw_inter.(Fexn).vR(:,2) - calc.(Fexn).sampling(:,2);           %[l] correct total vol balance for sampling

    else
        calc.(Fexn).mR(:,2)=raw_inter.(Fexn).vR(:,2);                                   % volume reactor according to scale signal [l]
        calc.(Fexn).vol_bal(:,2)=(cumtrapz(raw_inter.(Fexn).Feed(:,1),raw_inter.(Fexn).Feed(:,2)))./1000+(cumtrapz(raw_inter.(Fexn).Base(:,1),raw_inter.(Fexn).Base(:,2))./1000)+calc.(Fexn).mR(1,2); % volume balance [l]
        calc.(Fexn).sampling(:,2)=calc.(Fexn).vol_bal(:,2)-calc.(Fexn).mR(:,2);         % volume out of reactor by sampling [l]
        calc.(Fexn).sampling_r(:,2)=gradient(calc.(Fexn).sampling(:,2),interpol_interval);   % [l/h]sampling rate according to offline data
    end

calc.(Fexn).r_inert(:,2)=(100 - calc.(Fexn).y_o2_in(:,2) - calc.(Fexn).y_co2_in(:,2)) ./ ( 100 - raw_inter.(Fexn).CO2(:,2) - raw_inter.(Fexn).O2(:,2) - yH2O );
calc.(Fexn).DCW(:,2)=raw_inter.(Fexn).DCW(:,2); % [g/l]  c+1 is the column of the interpolated offline Biomass 
% calc.(Fexn).rX(:,2)=[0;diff((calc.(Fexn).DCW(:,2).*calc.(Fexn).mR(:,2))/MX)./diff(raw_inter.(Fexn).Air(:,1))];
calc.(Fexn).rX(:,2)=gradient((calc.(Fexn).DCW(:,2))./MX,interpol_interval);            %rX [mol/l/h]
% calc.(Fexn).rX(:,2)=gradient(calc.(Fexn).time,calc.(Fexn).DCW_smoothed(:,2).*calc.(Fexn).mR(:,2),10)/MX; % [mol/h] c+1 is the column of the interpolated offline Biomass

ind=1;      %skip the first value of each variable in case its zero


    if isfield(raw_inter.(Fexn), 'glucose')
    calc.(Fexn).glc_c(:,2)        =raw_inter.(Fexn).glucose(:,2);                                       % glucose concentration in supernatant [g/L]
    calc.(Fexn).glc_g(:,2)        =raw_inter.(Fexn).glucose(:,2).*calc.(Fexn).mR(:,2);                  % glucose in supernatant total [g]
    calc.(Fexn).glc_m(:,2)        =calc.(Fexn).glc_c(:,2)/MS;                                           % glucose in supernatant total [cmol/L]
    calc.(Fexn).rGlc_acc(:,2)     =gradient(calc.(Fexn).glc_m(:,2),interpol_interval);        % glucose accumulation rate [cmol/L/h]
    else
        msgbox('no glucose data')
    end
    
    if isfield(raw_inter.(Fexn), 'acetate')
    calc.(Fexn).ace_c(:,2)        =raw_inter.(Fexn).acetate(:,2);                                       % acetate concentration in supernatant [g/L]  
    calc.(Fexn).ace_g(:,2)        =raw_inter.(Fexn).acetate(:,2).*calc.(Fexn).mR(:,2);                  % acetate in supernatant total [g]  
    calc.(Fexn).ace_m(:,2)        =calc.(Fexn).ace_c(:,2)/MAc;                                          % acetate in supernatant total [cmol/L]
    calc.(Fexn).rAc(:,2)          =gradient(calc.(Fexn).ace_m(:,2),interpol_interval);        % acetate formation rate [cmol/L/h]
    else
        msgbox('no acetate data')
    end

    if isfield(raw_inter.(Fexn), 'NH4')
    calc.(Fexn).NH4_c(:,2)        =raw_inter.(Fexn).NH4(:,2);                                       % NH4 concentration in supernatant [g/L]
    calc.(Fexn).NH4_g(:,2)        =raw_inter.(Fexn).NH4(:,2).*calc.(Fexn).mR(:,2);                  % NH4 in supernatant total [g]
    calc.(Fexn).NH4_m(:,2)        =calc.(Fexn).NH4(:,2)/MNH4;                                           % NH4 in supernatant total [cmol/L]
    calc.(Fexn).NH4_acc(:,2)      =gradient(calc.(Fexn).NH4_m(:,2),interpol_interval);        % NH4 accumulation rate [cmol/L/h]
    else
        msgbox('no glucose data')
    end

% Substrate
    
calc.(Fexn).rS(:,2)     =   ((raw_inter.(Fexn).Feed(:,2)./1000)./calc.(Fexn).mR(:,2))*c_s/MS;                  %[cmol/l/h] feed rate if no glucose accumulation occured or no data is available
% calc.(Fexn).rS(:,2)     =   (raw_inter.(Fexn).Feed(:,2)./1000)*c_s/MS;
calc.(Fexn).rS_g(:,2)   =   calc.(Fexn).rS(:,2)*MS;                                                        % [g/l/h] feed rate

calc.(Fexn).dS(:,2)     =   cumtrapz(calc.(Fexn).time,calc.(Fexn).rS(:,2));       
% [cmol/l] cum substrate since Start  
calc.(Fexn).dS(:,2)     =   calc.(Fexn).dS(:,2)-calc.(Fexn).dS(ind,2);                                     % [cmol/l] substrate starting from Ind. 
calc.(Fexn).dS_g(:,2)   =   calc.(Fexn).dS(:,2)*MS;                                                        % [g/l] cum. substrate starting from Ind. 
calc.(Fexn).qs_v(:,2)   =   calc.(Fexn).rS(:,2)*MS./(calc.(Fexn).DCW(:,2).*calc.(Fexn).mR(:,2));           % [g/(g*l*h)] qs incremental
calc.(Fexn).qs(:,2)     =   calc.(Fexn).rS(:,2)*MS./calc.(Fexn).DCW(:,2);
calc.(Fexn).qs_cum(:,2) =   cumtrapz(calc.(Fexn).time,calc.(Fexn).rS(:,2).*MS)./(calc.(Fexn).DCW(:,2).*calc.(Fexn).mR(:,2));  % cumulative qs cumulative substrate/real time BM [g/g/l/h]


    if isfield(Ex_group_A.constants, 'base_mol')
        calc.(Fexn).r_n(:,2)           =(raw_inter.(Fexn).Base(:,2)*c_n/1000)./calc.(Fexn).mR(:,2);                       % rate of nitrogen addition [mol/L/h]     
    else
        msgbox('no base molarity')
    end

% Biomass
calc.(Fexn).BM_g(:,2)       =(calc.(Fexn).DCW(:,2).*calc.(Fexn).mR(:,2));                         % [g] BM in 
calc.(Fexn).BM_mol(:,2)     =(calc.(Fexn).DCW(:,2).*calc.(Fexn).mR(:,2)./MX);                   % [cmol] BM in 
calc.(Fexn).my(:,2)         =calc.(Fexn).rX(:,2)*MX./(calc.(Fexn).DCW(:,2)); % [1/h] specific growth rate
calc.(Fexn).Yxs(:,2)        =calc.(Fexn).rX(:,2)*MX./(calc.(Fexn).rS_g(:,2));                           % [g/g] biomass yield
calc.(Fexn).Yxs_cum(:,2)    =(calc.(Fexn).DCW(:,2).*(calc.(Fexn).mR(:,2)+calc.(Fexn).sampling(:,2))-calc.(Fexn).BM_g(ind,2))./(calc.(Fexn).dS_g(:,2).*(calc.(Fexn).mR(:,2)));              % [g/g] cumulative BM yield corrected for the amount of BM removed by sampling
calc.(Fexn).dSg_BMg(:,2)    =(calc.(Fexn).dS_g(:,2)./calc.(Fexn).DCW(:,2));                                % g cumuliertes Substrat / g BM
calc.(Fexn).dS_BMg(:,2)     =(calc.(Fexn).dS(:,2)./calc.(Fexn).DCW(:,2));                                   % cmol cumuliertes Substrat / g BM
calc.(Fexn).dS_BMmol(:,2)   =(calc.(Fexn).dS(:,2)./(calc.(Fexn).DCW(:,2)/MX));                               % cmol cumuliertes Substrat / cmol BM
calc.(Fexn).cumS_xinit(:,2) =calc.(Fexn).dS_g(:,2)./calc.(Fexn).DCW(ind,2);              % g/L cumuliertes Substrat / g/L BM initiell


    if isfield(calc.(Fexn), 'rGlc_acc')
%         calc.(Fexn).rS(:,2)     =  raw_inter.(Fexn).Feed(:,2)./1000.*c_s./MS-calc.(Fexn).rGlc_acc(:,2);    %[cmol/h] feed rate corrected by glucose accumulation
        calc.(Fexn).rScorr(:,2) =   calc.(Fexn).rS_g(:,2)-(calc.(Fexn).rGlc_acc(:,2)/MS);
        calc.(Fexn).qscorr(:,2) =   calc.(Fexn).rScorr(:,2)./(calc.(Fexn).DCW(:,2).*calc.(Fexn).mR(:,2));
        calc.(Fexn).cumS_xinitcorr(:,2)     =(calc.(Fexn).dS_g(:,2)-calc.(Fexn).glc_g(:,2))./calc.(Fexn).BM_g(ind,2);  % g cumuliertes Substrat / g BM initiell
        calc.(Fexn).Yxscorr(:,2)            =calc.(Fexn).rX(:,2)*MX./(calc.(Fexn).rS_g(:,2)-(calc.(Fexn).rGlc_acc(:,2)*MS));        %[
        calc.(Fexn).Yxscorr_cum(:,2)        =(calc.(Fexn).DCW(:,2).*(calc.(Fexn).mR(:,2)+calc.(Fexn).sampling(:,2))-calc.(Fexn).BM_g(ind,2))./(calc.(Fexn).dS_g(:,2)-calc.(Fexn).glc_g(:,2));       % corrected with glucose data
    end

%qs correction
    if ramp_eval==1
calc.(Fexn).qs_max(:,2)                 =-0.0455*calc.(Fexn).cumS_xinit(:,2)+0.5171;                     % qs_max over dSn function [g/g/h]
calc.(Fexn).qsacc(:,2)                  =calc.(Fexn).qs(:,2)-calc.(Fexn).qs_max(:,2);                    % accumulation of substrate [g/g/h]
qsacclow{k}(:,2)                        =find(calc.(Fexn).qsacc(:,2)<0);                                 % indices of positive accumulation rate
calc.(Fexn).qsacc(qsacclow{k}(:,2),2)   =0;                                                              % setting negative accumulation rates to zero
calc.(Fexn).qs_real(:,2)                =calc.(Fexn).qs(:,2)-calc.(Fexn).qsacc(:,2);                     % real qs [g/g/h]
calc.(Fexn).rS_real(:,2)                =calc.(Fexn).qs_real(:,2).*(calc.(Fexn).DCW(:,2).*calc.(Fexn).mR(:,2))/MS; % real rS [mol/h]
    end
    

% Offgas
calc.(Fexn).CER(:,2)         = calc.(Fexn).gassing(:,2)./MV/100.*(raw_inter.(Fexn).CO2(:,2).*calc.(Fexn).r_inert(:,2)-calc.(Fexn).y_co2_in(:,2)).*1000./calc.(Fexn).mR(:,2); %[mmol/l/h]
calc.(Fexn).OUR(:,2)         = -calc.(Fexn).gassing(:,2)./MV/100.*(raw_inter.(Fexn).O2(:,2).*calc.(Fexn).r_inert(:,2)-calc.(Fexn).y_o2_in(:,2)).*1000./calc.(Fexn).mR(:,2); %[mmol/l/h]
calc.(Fexn).Yco2s(:,2)       = (calc.(Fexn).CER(:,2)*44/1000)./calc.(Fexn).rS_g(:,2);                                 %[g/g]
calc.(Fexn).Yo2s(:,2)        = (calc.(Fexn).OUR(:,2) *32/1000)./calc.(Fexn).rS_g(:,2);                                    %[g/g] incremental CO2 yield per substrate
calc.(Fexn).Yco2s_cum(:,2)   = cumtrapz(calc.(Fexn).time,calc.(Fexn).CER(:,2) *44/1000)./calc.(Fexn).dS_g(:,2);               %[g/g] cumulative CO2 yield per substrate
calc.(Fexn).Yco2x_cum(:,2)   = cumtrapz(calc.(Fexn).time,calc.(Fexn).CER(:,2) *44/1000)./(calc.(Fexn).DCW(:,2)); %[g/g] cumulative q CO2 per BM (qco2_cum)
% calc.(Fexn).qo2(:,2)        = (calc.(Fexn).OUR(:,2) *32/1000)./(calc.(Fexn).rX(:,2)*MX.*calc.(Fexn).time);           
calc.(Fexn).qo2(:,2)      = (calc.(Fexn).OUR(:,2) *32/1000)./(calc.(Fexn).DCW(:,2));       % [g/(g*h)]
calc.(Fexn).qco2(:,2)       = (calc.(Fexn).CER(:,2) *44/1000)./(calc.(Fexn).DCW(:,2));   % [g/(g*h)]       
calc.(Fexn).RQ(:,2)         = calc.(Fexn).CER(:,2) ./calc.(Fexn).OUR(:,2) ;                                            %[] respiratory quotient
calc.(Fexn).CO2_cum(:,2)    = cumtrapz(calc.(Fexn).time,(calc.(Fexn).CER(:,2)/1000));                                   %cumulative CO2 [mol/L]



% Protein
    if isfield(raw_inter.(Fexn), 'protein') 
        
    calc.(Fexn).Pro(:,2)        =raw_inter.(Fexn).protein(:,2)/MX;                                              % Protein content [mol/l]  
    calc.(Fexn).r_prot(:,2)     =interp1(time_filtered_gradient,filtered_gradient(raw_inter.(Fexn).Air(:,1),raw_inter.(Fexn).protein(:,2).*calc.(Fexn).mR(:,2),order),calc.(Fexn).time,'linear');   % [g/h] Protein excretion rate into the supernatant (according to TCA-BCA)
    calc.(Fexn).qpro(:,2)       =calc.(Fexn).r_prot(:,2)./calc.(Fexn).BM_g(:,2);      % [g/(g*h)] qpro Protein excretion per Biomass incremental
       
    else
        msgbox('no protein data')
    end 
    
    
%Product                           
    if isfield(raw_inter.(Fexn), 'product');
% in Pellet
    calc.(Fexn).PPel_g(:,2)     = raw_inter.(Fexn).product(:,2);                                     % [g/L] total titer pellet
%     calc.(Fexn).rpp(:,2)        = interp1(time_filtered_gradient,filtered_gradient(raw_inter.(Fexn).Air(:,1),raw_inter.(Fexn).product(:,2).*calc.(Fexn).mR(:,2),order),calc.(Fexn).time,'linear');  % [g/h] rate of product formation in pellet incremental
    calc.(Fexn).rpp(:,2)        = gradient(calc.(Fexn).PPel_g(:,2),interpol_interval);  % [g/(L*h)] rate of product formation in pellet incremental
    calc.(Fexn).PSPel(:,2)      = calc.(Fexn).PPel_g(:,2)./calc.(Fexn).DCW(:,2);                        % [g/g] spezific product (on BM) titer in pellet
    calc.(Fexn).Ypps(:,2)       = calc.(Fexn).rpp(:,2)./(calc.(Fexn).rS_g(:,2));                                  % [g/g] Yield product in pellet/substrate incremental
    calc.(Fexn).Ypps_cum(:,2)   = calc.(Fexn).PPel_g(:,2)./calc.(Fexn).dS_g(:,2);                               % [g/g] Yield product in pellet/substrate since induction
    calc.(Fexn).Ypps_cum(1,2)   = 0;
    calc.(Fexn).qpp(:,2)        = calc.(Fexn).rpp(:,2)./calc.(Fexn).DCW(:,2);                                  % [g/(g*h)] q product in pellet per BM 
    else
        msgbox('no product data')
    end
    
    
% in Supernatant
    if isfield(raw_inter.(Fexn), 'product_sup');                                                                   % proceed if there is Supernatant Data available                 

    calc.(Fexn).PSup_g(:,2)     = raw_inter.(Fexn).product_sup(:,2).*calc.(Fexn).mR(:,2);                        % [g] total titer supernatant in VR (already corrected for biovolume in offline data file)
    calc.(Fexn).rps(:,2)        = interp1(time_filtered_gradient,filtered_gradient(raw_inter.(Fexn).Air(:,1),raw_inter.(Fexn).product_sup(:,2).*calc.(Fexn).mR(:,2),order),calc.(Fexn).time,'linear');  % [g/h] rate of product formation in supernatant incremental
    calc.(Fexn).PSSup(:,2)      = calc.(Fexn).PSup_g(:,2)./calc.(Fexn).BM_g(:,2);                                % [g/g] spezific product (on BM) titer in supernatant
    calc.(Fexn).Ypss(:,2)       = calc.(Fexn).rps(:,2) ./calc.(Fexn).rS_g(:,2);                                  % [g/g] Yield Product in supernatant/Substrat incremental
    calc.(Fexn).Ypss_cum(:,2)   = calc.(Fexn).PSup_g(:,2)./calc.(Fexn).dS_g(:,2);                                % [g/g] Yield product in supernatant/substrate since induction
    calc.(Fexn).qps(:,2)        = calc.(Fexn).rps(:,2)./calc.(Fexn).BM_g(:,2);                                   % [g/(g*h)] q product in Supernatant per BM

    calc.(Fexn).Prod_tot(:,2)   = calc.(Fexn).PSup_g(:,2)+calc.(Fexn).PPel_g(:,2);                               % [g/l] total titer
    calc.(Fexn).rp(:,2)         = calc.(Fexn).rps(:,2)+calc.(Fexn).rpp(:,2) ;                                    % [g/h] total product formation rate (incremental)
    calc.(Fexn).Yps(:,2)        = calc.(Fexn).Ypss(:,2) +calc.(Fexn).Ypps(:,2) ;                                 % [g/g] total product Yield per substrate(incremental)
    calc.(Fexn).Yp_cum(:,2)     = calc.(Fexn).Ypps_cum(:,2)+calc.(Fexn).Ypss_cum(:,2);                           % [g/g] total product Yield per substrate(cumulative)
    calc.(Fexn).qp_tot(:,2)     = calc.(Fexn).qpp(:,2)+calc.(Fexn).qps(:,2);                                     % [g/g] total product Yield per substrate(cumulative)

    elseif isfield(raw_inter.(Fexn), 'product_sup')
    calc.(Fexn).Prod_tot(:,2)=calc.(Fexn).PPel_g(:,2);
    else
    msgbox('no product in supernatant')
    end

    

% reconcilation
for j=1:size(calc.(Fexn).time)
xm=[-calc.(Fexn).rS(j,2)*1000; -calc.(Fexn).OUR(j,2); calc.(Fexn).CER(j,2)];

E=[+1 0 +1 +1;gamma_s gamma_o2 gamma_x 0];
Em=[+1,0,+1;gamma_s,gamma_o2,0]; % First row C balance 2nd row DR balance
Ec =[1; gamma_x]; % Biomass Estimation

e_s=0.03;
e_o2=0.03;
e_co2=0.03;

Xi=[e_s 0 0;0 e_o2 0 ;0 0 e_co2];
   
Ec_star=(inv(Ec'*Ec))*Ec';

R=Em-Ec*Ec_star*Em;
[U,S,V]=svd(R);
Sconv=[1 0];
C=Sconv*S;
K=C*S'*U';
Rred=K*R;
eps = Rred * xm;
sai = diag(diag(xm * xm' * Xi * Xi'));
Phi = Rred * sai *Rred';
delta = (sai*Rred'*inv(Phi) * Rred)* xm;
xmbest=xm-delta;calc
xcbest = -Ec_star*Em*xmbest;
h = eps' * inv(Phi) * eps;

calc.(Fexn).rS_rec(j,2)=xmbest(1);
calc.(Fexn).OUR_rec(j,2)=xmbest(2);
calc.(Fexn).CER_rec(j,2)=xmbest(3);
calc.(Fexn).rX_rec(j,2)=xcbest;
calc.(Fexn).h_value(j,2)=h;

end

    if isfield(raw_inter.(Fexn), 'protein') && isfield(raw_inter.(Fexn), 'protein_sup'); 
    calc.(Fexn).C_bal_offline(:,2)  =(calc.(Fexn).rX(:,2)+calc.(Fexn).CER(:,2) /1000+(calc.(Fexn).r_prot(:,2)./MX)+(calc.(Fexn).rps(:,2)./MX))./calc.(Fexn).rS(:,2); %incl Protein and product
    elseif isfield(raw_inter.(Fexn), 'protein_sup')
    calc.(Fexn).C_bal_offline(:,2)  =(calc.(Fexn).rX(:,2)+calc.(Fexn).CER(:,2) /1000+(calc.(Fexn).rps(:,2)./MX))./calc.(Fexn).rS(:,2);        
    elseif isfield(raw_inter.(Fexn), 'acetate') && isfield(raw_inter.(Fexn), 'glucose');
    calc.(Fexn).C_bal_offline(:,2)=((calc.(Fexn).rX(:,2))+calc.(Fexn).rAc(:,2)+calc.(Fexn).rGlc_acc(:,2)+calc.(Fexn).CER(:,2)/1000)./calc.(Fexn).rS(:,2); %C-balance corrected with acetate & glucose formation rates
    calc.(Fexn).C_bal_cum(:,2)=((calc.(Fexn).DCW(:,2).*(calc.(Fexn).mR(:,2)+calc.(Fexn).sampling(:,2))-calc.(Fexn).BM_g(ind,2))/MX+(calc.(Fexn).glc_m(:,2)+calc.(Fexn).ace_m(:,2)).*(calc.(Fexn).mR(:,2)+calc.(Fexn).sampling(:,2))+calc.(Fexn).CO2_cum(:,2).*(calc.(Fexn).mR(:,2)))./(calc.(Fexn).dS(:,2).*(calc.(Fexn).mR(:,2)));
    else
    calc.(Fexn).C_bal_offline(:,2)  =(calc.(Fexn).rX(:,2)+calc.(Fexn).CER(:,2) /1000)./calc.(Fexn).rS(:,2);
%     calc.(Fexn).C_bal_cum(:,2)=((calc.(Fexn).BM_mol(:,2)-calc.(Fexn).BM_mol(ind,2))+calc.(Fexn).CO2_cum(:,2))./calc.(Fexn).dS(:,2);   %w/o Protein
    calc.(Fexn).C_bal_cum(:,2)=((calc.(Fexn).DCW(:,2).*(calc.(Fexn).mR(:,2)+calc.(Fexn).sampling(:,2))-calc.(Fexn).BM_g(ind,2))/MX+calc.(Fexn).CO2_cum(:,2).*(calc.(Fexn).mR(:,2)))./(calc.(Fexn).dS(:,2).*(calc.(Fexn).mR(:,2)));
    end
    
calc.(Fexn).DoR_bal_offline(:,2)= -calc.(Fexn).rX(:,2).*gamma_x./(-(calc.(Fexn).rS(:,2).*gamma_s+calc.(Fexn).OUR(:,2) ./1000).*gamma_o2) ;  % in order to yield 1 1=(rx*gammax)/(-rs*gammas+(-r02*gamma02)) rs and ro2 as uptake rates are negative

% calc.(Fexn).BM_rec(:,2)         = raw_inter.DCW(:,2)+cumtrapz(calc{k}(2:size(calc{k}(:,1),1),1),calc{k}(2:size(calc{k}(:,1),1),rX_rec)*MX/1000);

calc.(Fexn).BM_rec(:,2)         = raw_inter.(Fexn).DCW(:,2)+cumtrapz(calc.(Fexn).time,calc.(Fexn).rX_rec(:,2)*MX/1000);
calc.(Fexn).qs_rec(:,2)         = -calc.(Fexn).rS_rec(:,2)*MS./calc.(Fexn).BM_rec(:,2)/1000;
calc.(Fexn).my_rec(:,2)         = calc.(Fexn).rX_rec(:,2)*MX./calc.(Fexn).BM_rec(:,2)/1000;
calc.(Fexn).Yxs_rec(:,2)        = -calc.(Fexn).rX_rec(:,2)./calc.(Fexn).rS_rec(:,2);
calc.(Fexn).Yco2s_rec(:,2)      = -calc.(Fexn).CER_rec(:,2)./calc.(Fexn).rS_rec(:,2);
calc.(Fexn).Yo2s_rec(:,2)       = calc.(Fexn).OUR_rec(:,2)./calc.(Fexn).rS_rec(:,2);
end

%add time achsis to every calc variable
 

for k=k1:k2
 Fexn=Fex{k};   
 Fcalc=fieldnames(calc.(Fexn));
 
    for j=1:length(Fcalc)                                %time is skipped
    
        Fcalcn=Fcalc{j};                                       %looping through variables
        calc.(Fexn).(Fcalcn)(:,1)=calc.(Fexn).time;

    end
end

%% NAN removal from calc
   
       
for k=k1:k2
 Fexn=Fex{k};   
 Fcalc=fieldnames(calc.(Fexn));

    for j=2:length(Fcalc)   
        Fcalcn=Fcalc{j};
        nan=[];
        nan=isnan(calc.(Fexn).(Fcalcn)(:,2));
        calc.(Fexn).(Fcalcn)(find(nan==1),:)=[];
  
    end

end

%% form final experimental struct

Ex_group_A.calc=calc;
Ex_group_A.raw_inter=raw_inter;
Ex_group_A.offline_raw=offline_raw;

      
%% Save Calc/offline Matrix to folder

save (['Eval_Ex_' Ex], 'Ex_group_A' )