%%input    

%run infomatrix script first

% The new defaults will not take effect if there are any open figures. To
% use them, we close all figures, and then repeat the first example.
close all;

% Change matlab Defaults
alw = 0.75;    % AxesLineWidth
fsz = 9;      % Fontsize
fsxy = 9;     % Fontsize axis
lw = 1.0;      % LineWidth
msz = 4;       % MarkerSize
width= 10       %cm
height= 18      %cm
lfs = 6         % Legend Font Size

% Color options    
%Colormap (color)
%color=[1 1 0; 1 0 1;0 1 1; 1 0 0; 0 1 0; 0 0 1; 0.5 0.5 0.5; 0.5 0.5 0; 0.5 0 0.5; 0 0.5 0.5; 0 0 0; 0.25 0.25 0.25; 0.25 0.5 0.5; 0.25 0.25 0.5;0.5 0.25 0.5; 1 1 0; 1 0 1;0 1 1; 1 0 0; 0 1 0; 0 0 1; 0.5 0.5 0.5; 0.5 0.5 0; 0.5 0 0.5; 0 0.5 0.5; 0 0 0; 0.25 0.25 0.25; 0.25 0.5 0.5; 0.25 0.25 0.5;0.5 0.25 0.5; 1 1 0; 1 0 1;0 1 1; 1 0 0; 0 1 0; 0 0 1; 0.5 0.5 0.5; 0.5 0.5 0; 0.5 0 0.5; 0 0.5 0.5; 0 0 0; 0.25 0.25 0.25; 0.25 0.5 0.5; 0.25 0.25 0.5;0.5 0.25 0.5   ]; %color vector
%Colormap (gray improved)
color=[0 0 0; 0.2 0.2 0.2; 0.3 0.3 0.3; 0.4 0.4 0.4; 0.6 0.6 0.6; 0.7 0.7 0.7; 0.8 0.8 0.8; 0 0 0; 0.2 0.2 0.2; 0.3 0.3 0.3; 0.4 0.4 0.4; 0.6 0.6 0.6; 0.7 0.7 0.7; 0.8 0.8 0.8; 0 0 0; 0.2 0.2 0.2; 0.3 0.3 0.3; 0.4 0.4 0.4; 0.6 0.6 0.6; 0.7 0.7 0.7; 0.8 0.8 0.8; 0 0 0; 0.2 0.2 0.2; 0.3 0.3 0.3; 0.4 0.4 0.4; 0.6 0.6 0.6; 0.7 0.7 0.7; 0.8 0.8 0.8]; %gray color map
%select Default color map
%color= gray

% Line and Markers styles
    linestyles = cellstr(char(':','--','-','-.','--','-','--','-.','-','--','-.','-','--','-.','-','--','-.','-','--','-.','-','--','-.','-','--','-.','-','--','-.','-','--','-.','-','--','-.','-'));
    Markers=['o','x','+','*','s','d','v','^','<','>','p','h','+','*','o','x','^','<','h','.','>','p','s','d','v','o','x','+','*','s','d','v','^','<','>','p','h','.','o','x','+','*','s','d','v','^','<','>','p','h','.','+','*','o','x','^','<','h','.','>','p','s','d','v','o','x','+','*','s','d','v','^','<','>','p','h','.'];

% automatic set default of properties
set(0,'defaultLineLineWidth',lw);   % set the default line width to lw
set(0,'defaultLineMarkerSize',msz); % set the default line marker size to msz
set(0,'DefaultTextFontSize',fsz)   % set the default line width to fsz
set(0,'DefaultAxesFontSize',fsxy)  % set Font size of axis Labels, Axis numbers
set(0,'defaultAxesLineWidth',alw);   % set the default line width to lw
    
% automatic set the defaults for saving/printing to a file
set(0,'defaultFigureInvertHardcopy','on'); % This is the default anyway
set(0,'defaultFigurePaperUnits','centimeters'); % This is the default anyway
defsize = get(gcf, 'PaperSize');
left = (defsize(1)- width)/2;
bottom = (defsize(2)- height)/2;
defsize = [left, bottom, width, height];
set(0, 'defaultFigurePaperPosition', defsize);
dimensions=[2, 2, 12, 24];          

%specify path for saving/printing plots
cd('S:\BPT\BPT SHARED DATA\Projects\L_Feeding Strategies\03_Results\Matlab plots');

%%

%select factor from headers -> watch that they match!
variable_x1='cumS_xinit';                                      %use differently if x achsis is not meant to be time (DEFAULT) fig1 or dSn fig2
label_x1={'Time after Induction [h]'};
variable_y1='qs';
label_y1={'qs [g/g/h]'};
title1= {'qs trajectories'}

variable_x2='cumS_xinit';  
label_x2={'Time after Induction [h]'};
variable_y2='Yxs_cum';
label_y2={'Yxs_{cum} [g/g]'};
title2={'Yxs trajectories' }

variable_x3='cumS_xinit';                      
label_x3={'Time after Induction [h]'}; 
variable_y3='rS_g';
label_y3={'feed rate [g/h]'};
title3= {'feed profile' }


%in case you are not plotting against time or substrate give x_end a numerical value corresponding to the INDEX of the variable plotted last
%x_end=14;

%select experiments included in one group (one experiment in one row)
all=[1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24];
A=[1 4 6 7 8 10 12];    %group experiments
B=[22 14 20 24 1 13]; %Free Flow vs controlled selected!   
Exp=[B];    % enter experiments to plot
% Exp=[A B];    % enter experiments to plot

% select legend according to experiment selection
%legend_plot ={ 'WR05A'   'WR05B'  'WR05C'  'WR05D'  'WR07A'   'WR07B'  'WR07C'  'WR07D' 'WR09A'   'WR09B'   'WR12A'   'WR12B'  'WR12C'  'WR12D' 'WR17A'   'WR17B'  'WR17C'  'WR17D' 'WR19A'   'WR19B'  'WR19C' 'WR20A'   'WR20B'   'WR20C'   'WR20D'};
legend_plot = { 'con. qs high'   'con. qs mid'  'con. qs low'  'f.f. qs high' 'f.f. qs mid' 'f.f. qs low'};


%% plot against time: smallest common
filename = 'trajectories plot against time smallest common'
    close(figure(1));
    close(figure(2));
    figure(1);  
    set(figure(1),'Units','centimeters','Position',dimensions)     

     x='time';    %x-achse
     End=floor(x_end.(x).min);
     xaxis=[0 End]; 

    grid on
    
 for i=1:length(Exp)    
     f=Exp(i);
     cc=Ferm{f};
     %x_end.(x).index(f)=x_end;
     
     
 subplot(3,1,1);

    hold on
   
    plot(InPh.(cc).(variable_x1)(1:x_end.(x).index(f),1),InPh.(cc).(variable_y1)(1:x_end.(x).index(f),2),[linestyles{i}],'color',color(i,:));
 
    ylabel(label_y1);
    xlabel(label_x1);
    set(gca, 'xlim', xaxis)
   legend(legend_plot ,'fontsize',lfs,'FontAngle','italic','Location','northeast');
    title(title1)
   grid on

subplot(3,1,2);
 
    hold on
    plot(InPh.(cc).(variable_x2)(1:x_end.(x).index(f),1),InPh.(cc).(variable_y2)(1:x_end.(x).index(f),2),[linestyles{i}],'color',color(i,:));

    ylabel(label_y2);
    xlabel(label_x2);    
    set(gca, 'xlim', xaxis)
    legend(legend_plot ,'fontsize',lfs,'FontAngle','italic','Location','northeast');
    title(title2)
    grid on
    %set(gca, 'ylim', [-0.5 1])
    
subplot(3,1,3);

    hold on
    plot(InPh.(cc).(variable_x3)(1:x_end.(x).index(f),1),InPh.(cc).(variable_y3)(1:x_end.(x).index(f),2),[linestyles{i}],'color',color(i,:));
    
    ylabel(label_y3);
    xlabel(label_x3);
    set(gca, 'xlim', xaxis)
    legend(legend_plot ,'fontsize',lfs,'FontAngle','italic','Location','northeast');
    title(title3)
    grid on
    %set(gca, 'ylim', [-0.5 1])    
    hold off
 end    

 print(filename, '-dpng', '-r400');

%% plot against cumulated substrate (dSn) until the greatest common
filename = 'trajectories plot against dSn until the greatest common'

    %close(figure(1));
    figure(2);  
    set(figure(2),'Units','centimeters','Position',dimensions)     

     x='cumS_xinit';    %x-achse

     label_x1={'dSn [g/g]'};
     label_x2={'dSn [g/g]'};
     label_x3={'dSn [g/g]'};
     End=floor(x_end.(x).min);
     xaxis=[0 End]; 

 for i=1:length(Exp)    
     f=Exp(i);
     cc=Ferm{f};
     %x_end.(x).index(f)=x_end;
     
 subplot(3,1,1);

    hold on
    plot(InPh.(cc).(variable_x1)(1:x_end.(x).index(f),2),InPh.(cc).(variable_y1)(1:x_end.(x).index(f),2),[linestyles{i}],'color',color(i,:));

    ylabel(label_y1);
    xlabel(label_x1);    
    set(gca, 'xlim', xaxis)
   legend(legend_plot ,'fontsize',lfs,'FontAngle','italic','Location','northeast');
    title(title1)
   grid on
    set(gca,'Xgrid','off')

subplot(3,1,2);
    
    hold on
    
    plot(InPh.(cc).(variable_x2)(1:x_end.(x).index(f),2),InPh.(cc).(variable_y2)(1:x_end.(x).index(f),2),[linestyles{i}],'color',color(i,:));
    
    ylabel(label_y2);
    xlabel(label_x2);     
    set(gca, 'xlim', xaxis)
    legend(legend_plot ,'fontsize',lfs,'FontAngle','italic','Location','northeast');
    title(title2)
    grid on
    set(gca,'Xgrid','off')
    %set(gca, 'ylim', [0 0.25])
    
    
subplot(3,1,3);
   

    hold on
    
    plot(InPh.(cc).(variable_x3)(1:x_end.(x).index(f),2),InPh.(cc).(variable_y3)(1:x_end.(x).index(f),2),[linestyles{i}],'color',color(i,:));
   
    ylabel(label_y3);
    xlabel(label_x3);
    set(gca, 'xlim', xaxis)
    legend(legend_plot ,'fontsize',lfs,'FontAngle','italic','Location','northeast');
    title(title3)
    grid on
    set(gca,'Xgrid','off')
    %set(gca, 'ylim', [0 0.25])
    
  hold off
 end
 
 print(filename, '-dpng', '-r400');

%% plot against cumulated substrate (dSn) independent of greates common value - whole dataset
filename = 'trajectories plot against Time or dSn until x'

    %close(figure(1));
    figure(4);  
    set(figure(4),'Units','centimeters','Position',dimensions)     

    % for plotting against 'cumS_xinit' -> InPh.(cc).(variable_x1)(1:stop,2)    %x-achse
      % for plotting against 'ctime' -> InPh.(cc).(variable_x1)(1:stop,1)    %x-achse
     x='cumS_xinit';    %x-achse
     label_x1={'dSn [g/g]'};
     label_x2={'dSn [g/g]'};
     label_x3={'dSn [g/g]'};
%      label_x1={'Time after Induction [h]'};
%      label_x2={'Time after Induction [h]'};
%      label_x3={'Time after Induction [h]'};
     xaxis=[0 6]; 

 for i=1:length(Exp)    
     f=Exp(i);
     cc=Ferm{f};
     %x_end.(x).index(f)=x_end;
     
     
 subplot(3,1,1);

    hold on
    stop1=length(InPh.(cc).(variable_x1));
    stop2= length(InPh.(cc).(variable_y1));
    stop=min(stop1,stop2);
    plot(InPh.(cc).(variable_x1)(1:stop,2),InPh.(cc).(variable_y1)(1:stop,2),[linestyles{i}],'color',color(i,:));
    title(title1)
    ylabel(label_y1);
    xlabel(label_x1);    
    set(gca, 'xlim', xaxis)
    legend(legend_plot ,'fontsize',lfs,'FontAngle','italic','Location','northeast');
    grid on
    
subplot(3,1,2);
 
    hold on
    stop1=length(InPh.(cc).(variable_x2));
    stop2= length(InPh.(cc).(variable_y2));
    stop=min(stop1,stop2);
    plot(InPh.(cc).(variable_x2)(1:stop,2),InPh.(cc).(variable_y2)(1:stop,2),[linestyles{i}],'color',color(i,:));;
    title(title2)
    ylabel(label_y2);
    xlabel(label_x2);     
    set(gca, 'xlim', xaxis)
    legend(legend_plot ,'fontsize',lfs,'FontAngle','italic','Location','northeast');
    grid on
    
subplot(3,1,3);

    hold on
    stop1=length(InPh.(cc).(variable_x3));
    stop2= length(InPh.(cc).(variable_y3));
    stop=min(stop1,stop2);
    plot(InPh.(cc).(variable_x3)(1:stop,2),InPh.(cc).(variable_y3)(1:stop,2),[linestyles{i}],'color',color(i,:));
    title(title3)
    ylabel(label_y3);
    xlabel(label_x3);
    set(gca, 'xlim', xaxis)
    legend(legend_plot ,'fontsize',lfs,'FontAngle','italic','Location','northeast');
    
  hold off
 end    

grid on

print(filename, '-dpng', '-r400');


