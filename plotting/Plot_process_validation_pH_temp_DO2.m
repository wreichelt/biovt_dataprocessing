
% Plotting Tool shortly after Process End. Run Data import and Rate clac.

close all 
clear all

Ex='WR24';
work_package='';

%set path
path=['S:\CDLMIB\CD_Data\Module_1\03_RT3\03_Results\Experiments\' Ex '\Matlab Auswertung\'];    %specify path of experimental data
% addpath('S:\BPT\BPT SHARED DATA\Projects\L_Feeding Strategies\02_Material_Methods\Matlab-Scripts');
addpath('S:\BPT\wreichelt\Matlab\biovt_dataprocessing');        %specify path of matlab files
cd(path);



%% load data



load(['constants_' Ex]);
load (['Eval_Ex_' Ex]);     %starts with induction
load (['Ex_' Ex])  ;

k1=1;
Fex=fieldnames(Ex_group_A.induction);
k2=length(Fex);
raw_inter=[];
interpol_interval=0.1;
raw_inter=Ex_group_A.raw_inter;       
calc=Ex_group_A.calc;


Ende = 45; %[h]



%% plotting Process Homogeniety

xaxis = [20,Ende];      %set limits for x-axis

l=2;
f1=14;
f2=16;
set(0,'DefaultAxesFontSize',12)  % set Font size of axis Labels, Legend, Axis numbers

h(1)=figure;
hold all
set(h,'position',[100,100,1049,895]);


% pH
subplot(311)
for k=k1:k2
   Fexn=Fex{k};
    hold all

    C(k) = plot(raw_inter.(Fexn).Air(:,1),raw_inter.(Fexn).pH(:,2),'LineWidth',l);
%     t_induktion = plot([offline_raw{k}(2,1)*24 offline_raw{k}(2,1)*24],[0 200],'k', 'LineWidth',1);

end

xlabel('time', 'FontSize', f1)
ylabel('pH', 'FontSize', f1)
% legend(t_induktion,{'EndFB'},'fontsize',7,'FontName','Arial','FontAngle','italic','Location','NorthEast');
legend( C(k1:k2) , {[Ex 'A'] [Ex 'B'] [Ex 'C'] [Ex 'D']},'fontsize',12,'FontName','Arial','FontAngle','italic','Location','Southeast');
set(gca, 'xlim', xaxis)
set(gca, 'ylim', [6 9])
hold off



% Temp
subplot(312)
for k=k1:k2
   Fexn=Fex{k};
hold all
    
    C(k) = plot(raw_inter.(Fexn).Air(:,1),raw_inter.(Fexn).Temp(:,2),'LineWidth',l);
%     t_induktion = plot([offline_raw{k}(2,1)*24 offline_raw{k}(2,1)*24],[0 200],'k', 'LineWidth',1);

end
xlabel('time', 'FontSize', f1)
ylabel('Temp', 'FontSize', f1)
% title('qs over dSn', 'FontSize', f2)
% % legend(t_induktion,{'EndFB'},'fontsize',7,'FontName','Arial','FontAngle','italic','Location','NorthEast');
legend( C(k1:k2) , {[Ex 'A'] [Ex 'B'] [Ex 'C'] [Ex 'D']},'fontsize',12,'FontName','Arial','FontAngle','italic','Location','Southeast');
set(gca, 'xlim', xaxis)
set(gca, 'ylim', [15 40])
hold off




%DO2
subplot(313)
for k=k1:k2
   Fexn=Fex{k};
hold all
    
    C(k) = plot(raw_inter.(Fexn).Air(:,1),raw_inter.(Fexn).DO2(:,2),'LineWidth',l);
    lim = plot([0 50],[30 30],'k', 'LineWidth',1,'LineStyle','- -');

end
xlabel('time', 'FontSize', f1)
ylabel('DO2', 'FontSize', f1)
% title('qs over dSn', 'FontSize', f2)
% % legend(t_induktion,{'EndFB'},'fontsize',7,'FontName','Arial','FontAngle','italic','Location','NorthEast');
legend( C(k1:k2) , {[Ex 'A'] [Ex 'B'] [Ex 'C'] [Ex 'D']},'fontsize',12,'FontName','Arial','FontAngle','italic','Location','Southeast');
set(gca, 'xlim', xaxis)
set(gca, 'ylim', [0 150])
hold off



%% save Plot - if you want to save DO NOT close the figure!

saveas(h,['pH_Temp_DO2_' [Ex]], 'fig')
saveas(h,['pH_Temp_DO2_' [Ex]], 'jpg')