%%input    

%run infomatrix script first

% The new defaults will not take effect if there are any open figures. To
% use them, we close all figures, and then repeat the first example.
close all;

% Change matlab Defaults for plotting
alw = 0.75;    % AxesLineWidth
fsz = 9;      % Fontsize
fsxy = 9;     % Fontsize axis
lw = 1.0;      % LineWidth
msz = 4;       % MarkerSize
width= 14       %cm !!! has to be changed for single plots 12 for 2x1;15 for 2x2 
height= 24      %cm !!! has to be changed for single plots 24 for 4x1;15 for 2x2
lfs = 5         % Legend Font Size

% Select Color    
%Colormap (color)
%color=[1 1 0; 1 0 1;0 1 1; 1 0 0; 0 1 0; 0 0 1; 0.5 0.5 0.5; 0.5 0.5 0; 0.5 0 0.5; 0 0.5 0.5; 0 0 0; 0.25 0.25 0.25; 0.25 0.5 0.5; 0.25 0.25 0.5;0.5 0.25 0.5; 1 1 0; 1 0 1;0 1 1; 1 0 0; 0 1 0; 0 0 1; 0.5 0.5 0.5; 0.5 0.5 0; 0.5 0 0.5; 0 0.5 0.5; 0 0 0; 0.25 0.25 0.25; 0.25 0.5 0.5; 0.25 0.25 0.5;0.5 0.25 0.5; 1 1 0; 1 0 1;0 1 1; 1 0 0; 0 1 0; 0 0 1; 0.5 0.5 0.5; 0.5 0.5 0; 0.5 0 0.5; 0 0.5 0.5; 0 0 0; 0.25 0.25 0.25; 0.25 0.5 0.5; 0.25 0.25 0.5;0.5 0.25 0.5   ]; %color vector
%Colormap (gray improved)
color=[0 0 0; 0.2 0.2 0.2; 0.3 0.3 0.3; 0.4 0.4 0.4; 0.6 0.6 0.6; 0.7 0.7 0.7; 0.8 0.8 0.8; 0 0 0; 0.2 0.2 0.2; 0.3 0.3 0.3; 0.4 0.4 0.4; 0.6 0.6 0.6; 0.7 0.7 0.7; 0.8 0.8 0.8; 0 0 0; 0.2 0.2 0.2; 0.3 0.3 0.3; 0.4 0.4 0.4; 0.6 0.6 0.6; 0.7 0.7 0.7; 0.8 0.8 0.8; 0 0 0; 0.2 0.2 0.2; 0.3 0.3 0.3; 0.4 0.4 0.4; 0.6 0.6 0.6; 0.7 0.7 0.7; 0.8 0.8 0.8]; %gray color map
%select Default color map
%color= lines

% Line and Markers styles
    linestyles = cellstr(char(':','--','-','-.','--','-','--','-.','-','--','-.','-','--','-.','-','--','-.','-','--','-.','-','--','-.','-','--','-.','-','--','-.','-','--','-.','-','--','-.','-'));
    Markers=['o','x','+','*','s','d','v','^','<','>','p','h','+','*','o','x','^','<','h','.','>','p','s','d','v','o','x','+','*','s','d','v','^','<','>','p','h','.','o','x','+','*','s','d','v','^','<','>','p','h','.','+','*','o','x','^','<','h','.','>','p','s','d','v','o','x','+','*','s','d','v','^','<','>','p','h','.'];

%automatic set default of properties
set(0,'defaultLineLineWidth',lw);   % set the default line width to lw
set(0,'defaultLineMarkerSize',msz); % set the default line marker size to msz
set(0,'DefaultTextFontSize',fsz)   % set the default line width to fsz
set(0,'DefaultAxesFontSize',fsxy)  % set Font size of axis Labels, Axis numbers
set(0,'defaultAxesLineWidth',alw);   % set the default line width to lw
    
%automatic set the defaults for saving/printing to a file
set(0,'defaultFigureInvertHardcopy','on'); % This is the default anyway
set(0,'defaultFigurePaperUnits','centimeters'); % This is the default anyway
defsize = get(gcf, 'PaperSize');
left = (defsize(1)- width)/2;
bottom = (defsize(2)- height)/2;
defsize = [left, bottom, width, height];
set(0, 'defaultFigurePaperPosition', defsize);
dimensions=[2, 2, 12, 24];          

%specify path for saving/printing plots
cd('S:\BPT\BPT SHARED DATA\Projects\L_Feeding Strategies\03_Results\Matlab plots\Paper');

%% Search for offline Points according to offline Measurement based on qscum
         
for k = 1:24 %Set total number of experiments
    
    t_induktion= round(offlinespeicher{k}(1,1)*24*10)/10;             % [h] induction
    diff=abs(calcspeicher{k}(:,1)-t_induktion);
    ind = find(diff==min(diff));  
%     %IndTime{k}(:,1)=calc{k}(:,1)-t_induktion; 

    offline_qscum{k}(:,1)= round(offlinespeicher{k}(:,1)*24*10)/10;  % [h] induction
    [r,c]=size (offlinespeicher{k}(:,:));
    g1=1;
    g2=r;
    for g=g1:g2
    diff=abs(calcspeicher{k}(:,1)- ((offline_qscum{k}(g,1)-offline_qscum{k}(1,1))));   
    offline_qscum{k}(g,2)=find(diff==min(diff)); 
    end
    
    
    
    
end



%% select experiments included in one group (one experiment in one row)

all=[1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24];
A=[1 4 6 7 8 10 12];    %group experiments
B=[22 15 19 24 8 23]; %Free Flow vs controlled selected!   
Exp=[B];    % enter experiments to plot
% Exp=[A B];    % enter experiments to plot

% select legend according to experiment selection
%legend_plot ={ 'WR05A'   'WR05B'  'WR05C'  'WR05D'  'WR07A'   'WR07B'  'WR07C'  'WR07D' 'WR09A'   'WR09B'   'WR12A'   'WR12B'  'WR12C'  'WR12D' 'WR17A'   'WR17B'  'WR17C'  'WR17D' 'WR19A'   'WR19B'  'WR19C' 'WR20A'   'WR20B'   'WR20C'   'WR20D'};
legend_plot = { 'con. qs high'   'con. qs mid'  'con. qs low'  'f.f. qs high' 'f.f. qs mid' 'f.f. qs low'};

% Load variable attribution for variable selection could be improved!!!
run('S:\BPT\BPT SHARED DATA\Projects\L_Feeding Strategies\02_Material_Methods\Matlab-Scripts\variable_attribution.m') 
%% Plotting trajectories based on Offline Data 
filename= 'trajectories based on offline Data'

for i=1:length(Exp)    
     f=Exp(i);
     cc=Ferm{f};

% select variables and labels     
variable_y1= BM_g    
label_x1={'Process Time [h]'};    
label_y1={'BM [g]'};
title1= {'BM offline'}

variable_y2= qs
label_y2={'qs [g/g/h]'};
title2={'qs offline' }

variable_y3= my
label_y3={'� [h-1]'};
title3= {'� offline' }

variable_y4= rX_offline
label_y4={'rx [c-mol/h]'};
title4= {'rX offline' }

%Select x axis insert:
    % plot(calcspeicher{f}(offline_qscum{f}(:,2),1),.... For TIME
    % plot(calcspeicher{f}(offline_qscum{f}(:,2),62),.... For dSn

%Select xlim:    
xaxis=[0 5]; 

subplot(4,1,1); %qs
   
    hold on
    
    plot(calcspeicher{f}(offline_qscum{f}(:,2),62),calcspeicher{f}(offline_qscum{f}(:,2),(variable_y1)),[Markers(i), linestyles{i}],'color',color(i,:)); 
   
    ylabel(label_y1);
    xlabel(label_x1);     
    set(gca, 'xlim', xaxis)
    legend(legend_plot ,'fontsize',lfs,'FontAngle','italic','Location','northeast');
    title(title1)
    grid on
    set(gca,'Xgrid','off')
    set(gca, 'ylim', [0 90]) %,'ytick',[0 0.05 0.1 0.15 0.2])    
     
subplot(4,1,2); %Yxs_cum
   
    hold on
    
    plot(calcspeicher{f}(offline_qscum{f}(:,2),62),calcspeicher{f}(offline_qscum{f}(:,2),(variable_y2)),[Markers(i), linestyles{i}],'color',color(i,:)); 
   
    ylabel(label_y2);
    xlabel(label_x1);     
    set(gca, 'xlim', xaxis)
    legend(legend_plot ,'fontsize',lfs,'FontAngle','italic','Location','northeast');
    title(title2)
    grid on
    set(gca,'Xgrid','off')
    %set(gca, 'ylim', [0 0.25])

subplot(4,1,3); %�
   
    hold on
    
    plot(calcspeicher{f}(offline_qscum{f}(:,2),62),calcspeicher{f}(offline_qscum{f}(:,2),(variable_y3)),[Markers(i), linestyles{i}],'color',color(i,:));
   
    ylabel(label_y3);
    xlabel(label_x1);     
    set(gca, 'xlim', xaxis)
    legend(legend_plot ,'fontsize',lfs,'FontAngle','italic','Location','northeast');
    title(title3)
    grid on
    set(gca,'Xgrid','off')
    %set(gca, 'ylim', [0 0.6])    

subplot(4,1,4); %�
   
    hold on
    
    plot(calcspeicher{f}(offline_qscum{f}(:,2),62),calcspeicher{f}(offline_qscum{f}(:,2),(variable_y4)),[Markers(i), linestyles{i}],'color',color(i,:));
   
    ylabel(label_y4);
    xlabel(label_x1);     
    set(gca, 'xlim', xaxis)
    legend(legend_plot ,'fontsize',lfs,'FontAngle','italic','Location','northeast');
    title(title4)
    grid on
    set(gca,'Xgrid','off')
    %set(gca, 'ylim', [0 0.6])
    
    
  hold off
end

% save plot as an png file
 print(filename, '-dpng', '-r400');

%% Plotting Product Data based on offline Data
%!!!NOT USERfriendly yet!!! 

%Specific and Volumetric titers
filename= 'Product Data'

for i=1:length(Exp)    
     f=Exp(i);
     cc=Ferm{f};

%Select x axis insert:
    % plot(calcspeicher{f}(offline_qscum{f}(:,2),1),.... For TIME
    % plot(calcspeicher{f}(offline_qscum{f}(:,2),62),.... For dSn

%Select xlim:    
xaxis=[0 40];      
     
subplot(221)

    hold all
    plot(calcspeicher{f}(offline_qscum{f}(:,2),1),offlinespeicher{f}(:,9), [linestyles{i},Markers(i)],'color',color(i,:));
   
xlabel('dS_n [g/g]')
ylabel('titer [g/L]')
title('SCF titer [g/L]')
set(gca, 'xlim', xaxis) %Xaxis
set(gca, 'ylim', [0 2.5]) %Yaxis
grid on
set(gca,'Xgrid','off')
hold off

legend(legend_plot ,'fontsize',lfs,'FontAngle','italic','Location','northeast');

subplot(222)

    hold all
    plot(calcspeicher{f}(offline_qscum{f}(:,2),1),offlinespeicher{f}(:,8), [linestyles{i},Markers(i)],'color',color(i,:));

xlabel('dS_n [g/g]')
ylabel('titer [g/L]')
title('CFM titer [g/L]')
set(gca, 'xlim', xaxis) %Xaxis
set(gca, 'ylim', [0 2.5]) %Yaxis
grid on
set(gca,'Xgrid','off')
hold off

legend(legend_plot ,'fontsize',lfs,'FontAngle','italic','Location','northeast');

subplot(223)

    hold all
    plot(calcspeicher{f}(offline_qscum{f}(:,2),1),offlinespeicher{f}(:,11), [linestyles{i},Markers(i)],'color',color(i,:));

xlabel('dS_n [g/g]')
ylabel('specific titer [g/g]')
title('SCF specific titer [g/g]')
set(gca, 'xlim', xaxis) %Xaxis
set(gca, 'ylim', [0 0.05]) %Yaxis
grid on
set(gca,'Xgrid','off')
hold off

legend(legend_plot ,'fontsize',lfs,'FontAngle','italic','Location','northeast');

subplot(224)

    hold all
    plot(calcspeicher{f}(offline_qscum{f}(:,2),1),offlinespeicher{f}(:,10), [linestyles{i},Markers(i)],'color',color(i,:));

xlabel('dS_n [g/g]')
ylabel('specific titer [g/g]')
title('CFM specific titer [g/g]')
set(gca, 'xlim', xaxis) %Xaxis
set(gca, 'ylim', [0 0.05]) %Yaxis
grid on
set(gca,'Xgrid','off')
hold off

legend(legend_plot ,'fontsize',lfs,'FontAngle','italic','Location','northeast');

end

print(filename, '-dpng', '-r400');

%% PRODUCT DATA volumetric SCF titers
filename= 'Product Data vs Time and dSn'
figure(3)
clf reset
for i=1:length(Exp)    
     f=Exp(i);
     cc=Ferm{f};    

subplot(211)

    hold all
    plot((offline_raw{f}(:,1)-offline_raw{f}(2,1))*24,(offline_raw{f}(:,9)),[linestyles{i},Markers(i)],'color',color(i,:));

xlabel('Time after Induction')
ylabel('volumetric titer [g/L]')
title('volumetric SCF titer [g/L]')
set(gca, 'xlim', [0 55]) %Xaxis
set(gca, 'ylim', [0 2.5]) %Yaxis
grid on
set(gca,'Xgrid','off')

hold off

legend(legend_plot ,'fontsize',lfs,'FontAngle','italic','Location','northeast');

subplot(212)


    hold all
    plot(calcspeicher{f}(offline_qscum{f}(:,2),62),(offlinespeicher{f}(:,9)),[linestyles{i}, Markers(i)],'color',color(i,:));

xlabel('dS_n [g/g]')
ylabel('volumetric titer [g/L]')
title('volumetric SCF titer [g/L]')
set(gca, 'xlim', [0 10],'xtick',[1 2 3 4 5 6 7 8 9 10 11 12 13 14 15]) %Xaxis
set(gca, 'ylim', [0 2.5]) %Yaxis
grid on
set(gca,'Xgrid','off')

hold off

legend(legend_plot ,'fontsize',lfs,'FontAngle','italic','Location','northeast');

end

print(filename, '-dpng', '-r400');
