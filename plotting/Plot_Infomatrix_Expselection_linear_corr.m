% The new defaults will not take effect if there are any open figures. To
% use them, we close all figures, and then repeat the first example.
close all;
clear all;

%% Change matlab Defaults
alw = 0.75;    % AxesLineWidth
fsz = 8;      % Fontsize
fsxy = 8;     % Fontsize axis
lw = 1.0;      % LineWidth
msz = 4;       % MarkerSize
width= 14      %cm
height= 7     %cm
lfs = 6         % Legend Font Size

% Color options    
%Colormap (color)
%color=[1 1 0; 1 0 1;0 1 1; 1 0 0; 0 1 0; 0 0 1; 0.5 0.5 0.5; 0.5 0.5 0; 0.5 0 0.5; 0 0.5 0.5; 0 0 0; 0.25 0.25 0.25; 0.25 0.5 0.5; 0.25 0.25 0.5;0.5 0.25 0.5; 1 1 0; 1 0 1;0 1 1; 1 0 0; 0 1 0; 0 0 1; 0.5 0.5 0.5; 0.5 0.5 0; 0.5 0 0.5; 0 0.5 0.5; 0 0 0; 0.25 0.25 0.25; 0.25 0.5 0.5; 0.25 0.25 0.5;0.5 0.25 0.5; 1 1 0; 1 0 1;0 1 1; 1 0 0; 0 1 0; 0 0 1; 0.5 0.5 0.5; 0.5 0.5 0; 0.5 0 0.5; 0 0.5 0.5; 0 0 0; 0.25 0.25 0.25; 0.25 0.5 0.5; 0.25 0.25 0.5;0.5 0.25 0.5   ]; %color vector
%Colormap (gray improved)
color=[0 0 0; 0.2 0.2 0.2; 0.3 0.3 0.3; 0.4 0.4 0.4; 0.6 0.6 0.6; 0.7 0.7 0.7; 0.8 0.8 0.8; 0 0 0; 0.2 0.2 0.2; 0.3 0.3 0.3; 0.4 0.4 0.4; 0.6 0.6 0.6; 0.7 0.7 0.7; 0.8 0.8 0.8; 0 0 0; 0.2 0.2 0.2; 0.3 0.3 0.3; 0.4 0.4 0.4; 0.6 0.6 0.6; 0.7 0.7 0.7; 0.8 0.8 0.8; 0 0 0; 0.2 0.2 0.2; 0.3 0.3 0.3; 0.4 0.4 0.4; 0.6 0.6 0.6; 0.7 0.7 0.7; 0.8 0.8 0.8]; %gray color map
%select Default color map
%color= gray

% Line and Markers styles
    linestyles = cellstr(char(':','--','-','-.','--','-','--','-.','-','--','-.','-','--','-.','-','--','-.','-','--','-.','-','--','-.','-','--','-.','-','--','-.','-','--','-.','-','--','-.','-'));
    Markers=['o','x','+','*','s','d','v','^','<','>','p','h','+','*','o','x','^','<','h','.','>','p','s','d','v','o','x','+','*','s','d','v','^','<','>','p','h','.','o','x','+','*','s','d','v','^','<','>','p','h','.','+','*','o','x','^','<','h','.','>','p','s','d','v','o','x','+','*','s','d','v','^','<','>','p','h','.'];

% Set default of properties
set(0,'defaultLineLineWidth',lw);   % set the default line width to lw
set(0,'defaultLineMarkerSize',msz); % set the default line marker size to msz
set(0,'DefaultTextFontSize',fsz)   % set the default line width to fsz
set(0,'DefaultAxesFontSize',fsxy)  % set Font size of axis Labels, Axis numbers
set(0,'defaultAxesLineWidth',alw);   % set the default line width to lw
    
% Set the defaults for saving/printing to a file
set(0,'defaultFigureInvertHardcopy','on'); % This is the default anyway
set(0,'defaultFigurePaperUnits','centimeters'); % This is the default anyway
defsize = get(gcf, 'PaperSize');
left = (defsize(1)- width)/2;
bottom = (defsize(2)- height)/2;
defsize = [left, bottom, width, height];
set(0, 'defaultFigurePaperPosition', defsize);
dimensions=[2, 2, 12, 24];          

%%
path=['S:\BPT\BPT SHARED DATA\Projects\L_Feeding Strategies\03_Results\Information Matrixes'];
addpath('S:\BPT\BPT SHARED DATA\Projects\L_Feeding Strategies\02_Material_Methods\Matlab-Scripts');
cd(path);
 
% Import Infomatrix.xls file for plotting
file='04-Jul-2014_infomatrix_AP4_cumS_xinit_AP4_WP4_ENDREPORT.xls';
tabname='Sheet1';
%tabname='Matlab_output_dSn';
infomatrix_raw=importdata(file);
infomatrix=infomatrix_raw.data.(tabname)(:,:);
[r,c]=size(infomatrix);

%specify path for saving/printing plots
cd('S:\BPT\BPT SHARED DATA\Projects\L_Feeding Strategies\03_Results\Matlab plots');

%% plotting

%select factor from headers -> watch that they match!
variable_x1={'qs'};
label_x1={'mean qs [g/g/h]'};
variable_y1={'PSPel'};
label_y1={'max spec titer [g/g]'};
input_x1=find(strcmp([variable_x1], infomatrix_raw.textdata.(tabname)(1,:)));
input_y1=find(strcmp([variable_y1], infomatrix_raw.textdata.(tabname)(1,:)));

variable_x2={'my'};
label_x2={'mean � [1/h]'};
variable_y2={'PSPel'};
label_y2={'max spec titer [g/g]'};
input_x2=find(strcmp([variable_x2], infomatrix_raw.textdata.(tabname)(1,:)));
input_y2=find(strcmp([variable_y2], infomatrix_raw.textdata.(tabname)(1,:)));

variable_y3={'qs'};
label_y3={'mean qs [g/g/h]'};
variable_x3={'my'};
label_x3={'mean � [1/h]'};
input_x3=find(strcmp([variable_x3], infomatrix_raw.textdata.(tabname)(1,:)));
input_y3=find(strcmp([variable_y3], infomatrix_raw.textdata.(tabname)(1,:)));


%select experiments included in one group (one experiment in one row)

all=[1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24];
A=[1 2 3 4 5 6 7 8 9 10 11 12 13 23 24 ];    %Free Flow qs
B=[14 15 16 17 18 19 20 21 22 ];            %controlled qs
Exp=[A B];                                  % select different groups
noe= 24 %Number of selected experiments

%select legend according to experiment selection
legend_plot={'free flow qs' 'controlled qs'}


%% Plot two groups of experiments (example free flow vs controlled qs)
filename = 'Correlation plot Free flow and controlled'
figure(1)

subplot(1,3,1)

    bhat=regress(infomatrix(A,input_y1),[ones(15,1) infomatrix(A,input_x1)]); 
    yhat=bhat(1)+infomatrix(A,input_x1)*bhat(2);
    bhat2=regress(infomatrix(B,input_y1),[ones(9,1) infomatrix(B,input_x1)]); 
    yhat2=bhat2(1)+infomatrix(B,input_x1)*bhat2(2);
    
    plot(infomatrix(A,input_x1),infomatrix(A,input_y1),'x',infomatrix(B,input_x1),infomatrix(B,input_y1),'o');
    hold on
    plot(infomatrix(A,input_x1),yhat,infomatrix(B,input_x1),yhat2);
    
    x = get(gca,'xlim');
    set(gca,'Xtick',linspace(x(1),x(2),5), 'Box', 'off');
    y = get(gca,'ylim');
    set(gca,'Ytick',linspace(y(1),y(2),5), 'Box', 'off', 'ylim', [0 0.06]);
    ylabel(label_y1);
    xlabel(label_x1);
    legend(legend_plot,'fontsize',lfs,'FontAngle','italic','Location','northwest');
   %legend('boxoff')
 
subplot(1,3,2)

    bhat=regress(infomatrix(A,input_y2),[ones(15,1) infomatrix(A,input_x2)]); 
    yhat=bhat(1)+infomatrix(A,input_x2)*bhat(2);
    bhat2=regress(infomatrix(B,input_y2),[ones(9,1) infomatrix(B,input_x2)]); 
    yhat2=bhat2(1)+infomatrix(B,input_x2)*bhat2(2);

    plot(infomatrix(A,input_x2),infomatrix(A,input_y2),'x',infomatrix(B,input_x2),infomatrix(B,input_y2),'o');
    hold on
    plot(infomatrix(A,input_x2),yhat,infomatrix(B,input_x2),yhat2);
    
    x = get(gca,'xlim');
    set(gca,'Xtick',[0 0.03 0.06 0.09], 'Box', 'off');
    y = get(gca,'ylim');
    set(gca,'Ytick',linspace(y(1),y(2),5), 'Box', 'off', 'ylim', [0 0.06]);
    ylabel(label_y2);
    xlabel(label_x2);
     legend(legend_plot,'fontsize',lfs,'FontAngle','italic','Location','northwest');
   %legend('boxoff')
    
subplot(1,3,3) %no difference regression line includes all experiments

    bhat=regress(infomatrix(all,input_y3),[ones(noe,1) infomatrix(all,input_x3)]); 
    yhat=bhat(1)+infomatrix(all,input_x3)*bhat(2);
    %bhat2=regress(infomatrix(B,input_y3),[ones(9,1) infomatrix(B,input_x3)]); 
    %yhat2=bhat2(1)+infomatrix(B,input_x3)*bhat2(2);
    
    plot(infomatrix(A,input_x3),infomatrix(A,input_y3),'x',infomatrix(B,input_x3),infomatrix(B,input_y3),'o');
    hold on
    %plot(infomatrix(A,input_x3),yhat,infomatrix(B,input_x3),yhat2);
    plot(infomatrix(all,input_x3),yhat,'color','k');
    
    x = get(gca,'xlim');
    set(gca,'Xtick',[0 0.03 0.06 0.09], 'Box', 'off');
    
    y = get(gca,'ylim');
    set(gca, 'ytick', [0.05 0.1 0.15 0.2 0.25 0.3 0.35], 'Box', 'off', 'ylim', [0 0.4]);
    ylabel(label_y3);
    xlabel(label_x3);

    legend(legend_plot,'fontsize',lfs,'FontAngle','italic','Location','northwest');
   %legend('boxoff')

    print(filename, '-dpng', '-r400');
%% Plot single groups (example all experiments)
figure(1)

subplot(1,3,1)

    bhat=regress(infomatrix(Exp,input_y1),[ones(noe,1) infomatrix(Exp,input_x1)]); 
    yhat=bhat(1)+infomatrix(Exp,input_x1)*bhat(2);
    plot(infomatrix(Exp,input_x1),infomatrix(Exp,input_y1),'x','MarkerSize',8);
    hold on
    plot(infomatrix(Exp,input_x1),yhat,'color','k','linestyle','-');
    x = get(gca,'xlim');
    set(gca,'Xtick',linspace(x(1),x(2),6), 'Box', 'off');
    y = get(gca,'ylim');
    set(gca,'Ytick',linspace(y(1),y(2),6), 'Box', 'off');
    ylabel(label_y1);
    xlabel(label_x1);
    
 
subplot(1,3,2)

    bhat=regress(infomatrix(Exp,input_y2),[ones(noe,1) infomatrix(Exp,input_x2)]); 
    yhat=bhat(1)+infomatrix(Exp,input_x2)*bhat(2);


    plot(infomatrix(Exp,input_x2),infomatrix(Exp,input_y2),'x','MarkerSize',8);
    hold on
    plot(infomatrix(Exp,input_x2),yhat,'color','k','linestyle','-');
    
    x = get(gca,'xlim');
    set(gca,'Xtick',linspace(x(1),x(2),6), 'Box', 'off');
    y = get(gca,'ylim');
    set(gca,'Ytick',linspace(y(1),y(2),6), 'Box', 'off');
    ylabel(label_y2);
    xlabel(label_x2);
    
    
subplot(1,3,3)

    bhat=regress(infomatrix(Exp,input_y3),[ones(noe,1) infomatrix(Exp,input_x3)]); 
    yhat=bhat(1)+infomatrix(Exp,input_x3)*bhat(2);
    plot(infomatrix(Exp,input_x3),infomatrix(Exp,input_y3),'x','MarkerSize',8);
    hold on
    plot(infomatrix(Exp,input_x3),yhat,'color','k','linestyle','-');

    x = get(gca,'xlim');
    set(gca,'Xtick',linspace(x(1),x(2),6), 'Box', 'off');
    y = get(gca,'ylim');
    set(gca,'Ytick',linspace(y(1),y(2),6), 'Box', 'off');
    ylabel(label_y3);
    xlabel(label_x3);

