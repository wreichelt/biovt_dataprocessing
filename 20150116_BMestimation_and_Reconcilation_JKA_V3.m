% additional script based on infomatrix 
% calulatining missing variables 
% permittivity estimated BM
% Feed forward profile
% reconcilation optimisation

run('variable_attribution.m')
% Find Softsensor Startpoint
MS=30.6966;              % [g/c-mol] Molecular weight substrate  (30.6966 glycerol; 30 Glucose)
gamma_s=4.667;          % DoR Substrate (4,667 glycerol; 4 Glucose)
gamma_x=4.13;            
gamma_o2=-4;
MX=26.54;                % [g/c-mol] Molecular weight Biomass
MV= 22.14               %L/mol

for k=1:length(Ferm) %re-calculates rates for reconcilation!
     cc=Ferm{k};
   
   SoftSensorStart(k) = mean(rawspeicher{i}(1:3,71))  %Softsensor Start point [c_mol]
  %calcspeicher{k}(:,5)=((rawspeicher{k}(:,26)*0.01666666+ rawspeicher{k}(:,27)*0.0166666)*60./MV.*((rawspeicher{k}(:,15)./100.*calcspeicher{k}(:,r_inert)-calcspeicher{k}(:,21)./100)))*1000
  %calcspeicher{k}(:,6)=((-(rawspeicher{k}(:,26)+ rawspeicher{k}(:,27))*0.0166666)*60./MV.*((rawspeicher{k}(:,14)./100.*calcspeicher{k}(:,r_inert)-calcspeicher{k}(:,20)./100)))*1000
  
%   calc{k}(:,CER)=(raw_inter{k}(:,rAir)+(raw_inter{k}(:,rOx)))/MV/100.*((raw_inter{k}(:,CO2).*calc{k}(:,r_inert)-calc{k}(:,y_co2_in))*1000./calc{k}(:,mR)); %[mmol/h]
% calc{k}(:,OUR)=-((raw_inter{k}(:,rAir)+(raw_inter{k}(:,rOx)))/MV/100.*(raw_inter{k}(:,O2).*calc{k}(:,r_inert)-calc{k}(:,y_o2_in))*1000./calc{k}(:,mR));
end

%% reconcilation optimisation
for k=1:length(Ferm) %Set total number of experiments
     cc=Ferm{k};

% reconcilation
for j=1:size(calcspeicher{k},1)
xm=[-calcspeicher{k}(j,rS)*1000; -calcspeicher{k}(j,OUR); calcspeicher{k}(j,CER)];

E=[+1 0 +1 +1;gamma_s gamma_o2 gamma_x 0];
Em=[+1,0,+1;gamma_s,gamma_o2,0]; % First row C balance 2nd row DR balance
Ec =[1; gamma_x]; % Biomass Estimation

e_s=0.01;
e_o2=0.15;
e_co2=0.05;

Xi=[e_s 0 0;0 e_o2 0 ;0 0 e_co2];
   
Ec_star=(inv(Ec'*Ec))*Ec';

R=Em-Ec*Ec_star*Em;
[U,S,V]=svd(R);
Sconv=[1 0];
C=Sconv*S;
K=C*S'*U';
Rred=K*R;
eps = Rred * xm;
sai = diag(diag(xm * xm' * Xi * Xi'));
Phi = Rred * sai *Rred';
delta = (sai*Rred'*inv(Phi) * Rred)* xm;
xmbest=xm-delta;calcspeicher
xcbest = -Ec_star*Em*xmbest;
h = eps' * inv(Phi) * eps;
calcspeicher{k}(j,rS_rec)=(xmbest(1))./(rawspeicher{k}(j,2)./1000);   % rS_rec
calcspeicher{k}(j,OUR_rec)=(xmbest(2))./(rawspeicher{k}(j,2)./1000);  % OUR_rec
calcspeicher{k}(j,CER_rec)=(xmbest(3))./(rawspeicher{k}(j,2)./1000);  % CER_rec
calcspeicher{k}(j,rX_rec)=(xcbest)./(rawspeicher{k}(j,2)./1000);      % rX_rec
calcspeicher{k}(j,h_value)=h;          %  h_value
end
calcspeicher{k}(:,73)=(mean(rawspeicher{k}(1:3,71)*1000)+cumtrapz(calcspeicher{k}(:,1),calcspeicher{k}(:,rX_rec)))/1000; %SoftSensor data c_mol
calcspeicher{k}(:,74)=(offlinespeicher{k}(1,2).*calcspeicher{k}(1,3)+(cumtrapz(calcspeicher{k}(:,1),calcspeicher{k}(:,rX_rec))/1000*MX))./calcspeicher{k}(:,3); %reconciled BM based on BM EFB [g/L]
%calcspeicher{k}(:,80)= calcspeicher{k}(:,rX_rec)*0.1
% calcspeicher{k}(:,73)=(mean(rawspeicher{k}(1:3,71)*1000)+cumsum(calcspeicher{k}(:,rX_rec)*0.1))/1000; %SoftSensor data c_mol
% calcspeicher{k}(:,74)=offlinespeicher{k}(1,2)+(cumsum(calcspeicher{k}(:,rX_rec)*0.1)/1000*MX); %reconciled BM based on BM EFB

% calc{k}(:,C_bal_offline)=(calc{k}(:,rX_offline)+calc{k}(:,CER)/1000+(calc{k}(:,r_prot)./MX))./calc{k}(:,rS); %incl Protein
% %calc{k}(:,C_bal_offline)=(calc{k}(:,rX_offline)+calc{k}(:,CER)/1000)./calc{k}(:,rS);
% calc{k}(:,DoR_bal_offline)=-calc{k}(:,rX_offline).*gamma_x./(-(calc{k}(:,rS).*gamma_s+(calc{k}(:,OUR)./1000).*gamma_o2)) ;  % in order to yield 1 1=(rx*gammax)/(-rs*gammas+(-r02*gamma02)) rs and ro2 as uptake rates are negative
% calc{k}(2:size(calc{k}(:,1),1),Biomass_rec)=raw_inter{k}(1,c+1)+cumtrapz(calc{k}(2:size(calc{k}(:,1),1),1),calc{k}(2:size(calc{k}(:,1),1),rX_rec)*MX/1000);
% calc{k}(:,qs_rec)=-calc{k}(:,rS_rec)*MS./calc{k}(:,Biomass_rec)/1000;
% calc{k}(:,my_rec)=calc{k}(:,rX_rec)*MX./calc{k}(:,Biomass_rec)/1000;
% calc{k}(:,Yxs_rec)=-calc{k}(:,rX_rec)./calc{k}(:,rS_rec);
% calc{k}(:,Yco2s_rec)=-calc{k}(:,CER_rec)./calc{k}(:,rS_rec);
% calc{k}(:,Yo2s_rec)=calc{k}(:,OUR_rec)./calc{k}(:,rS_rec);
end

%% Search for offline Points according to offline Measurement based on qscum
         
 for k=1:length(Ferm) %Set total number of experiments
    
    t_induktion= round(offlinespeicher{k}(1,1)*24*10)/10;             % [h] induction
    diff=abs(calcspeicher{k}(:,1)-t_induktion);
    ind = find(diff==min(diff));  
%     %IndTime{k}(:,1)=calc{k}(:,1)-t_induktion; 

    offline_qscum{k}(:,1)= round(offlinespeicher{k}(:,1)*24*10)/10;  % [h] induction
    [r,c]=size (offlinespeicher{k}(:,:));
    g1=1;
    g2=r;
    for g=g1:g2
    diff=abs(calcspeicher{k}(:,1)- ((offline_qscum{k}(g,1)-offline_qscum{k}(1,1))));   
    offline_qscum{k}(g,2)=find(diff==min(diff)); 
    offline_qscum{k}(g,3)=(offline_qscum{k}(g,1)-offline_qscum{k}(1,1));
    end
         
 end
 
%% Biomass estimation modelling

% Group experiments 

all=[1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 ];
FAB1=[1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 33  35 37 38 40];  %Fab1 pASL6 
FAB3=[25 26 27 28 29 30 31 32 34 36 39] %Fab3 pASL1
IB= [41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60] %Inclusion Body

Exp=[FAB3];

PermCalSS={}
PermCalFF={}


% calculation
for i=1:length(Exp) 
    
     f=Exp(i);
     cc=Ferm{f};

     t_induktion= round(offlinespeicher{f}(2,1)*24*10)/10;                                    % [h] induction Time
    diff=abs(calcspeicher{f}(:,1)-t_induktion);
    ind = find(diff==min(diff)); 
    
    % assigns strain specific variables    
    if isequal(Exp,FAB1) == 1
    calcspeicher{f}(:,77)= ((-0.041)* log(calcspeicher{f}(:,62)))+0.2794 %Yxs function Fab1 
    Yxs_FB= 0.47
    elseif isequal(Exp,FAB3) == 1
    calcspeicher{f}(:,77)= ((-0.04)* log(calcspeicher{f}(:,62)))+0.3078 %Yxs function Fab3
    Yxs_FB= 0.47
    elseif isequal(Exp,IB) == 1 
    calcspeicher{f}(:,77)= ((-0.061)* log(calcspeicher{f}(:,62)))+0.2706 %Yxs function IB 
    Yxs_FB= 0.4
    end
    
    % Permittivity signal calibration based on Soft sensor and Feed forward
    % profile during the exp Fed batch
    PermCalSS{f}= polyfit(raw_inter{f} (10:ind,71)*MX./calc_raw{f} (10:ind,mR) , raw_inter{f} (10:ind,41),1) %permittivity calibration based on Soft sensor
    PermCalFF{f}= polyfit((cumtrapz(calc_raw{f}(10:ind,1),calc_raw{f}(10:ind,54)).*Yxs_FB./calc_raw{f}(10:ind,3)+ offline_raw{f}(1,2)), raw_inter{f} (10:ind,41),1) %permittivity calibration based on FF profile
    
    
    calcspeicher{f}(:,75)=((rawspeicher{f}(:,41)-PermCalSS{f}(1,2))./PermCalSS{f}(1,1)); %Permittivity g/L Softsensor Cal
    calcspeicher{f}(:,76)=((rawspeicher{f}(:,41)-PermCalFF{f}(1,2))./PermCalFF{f}(1,1)); %Permittivity g/L Feed Forward Cal
    %calcspeicher{f}(:,78)= (offlinespeicher{f}(1,2)+ (calcspeicher{f}(:,57).*calcspeicher{f}(:,77)))./rawspeicher{f}(:,51)*1000 % BM g/L Yxs function
    %calcspeicher{f} (:,79)= rawspeicher{f} (:,71)*MX./rawspeicher{f}(:,51)*1000 % BM g/L Softsensor based
    
    calcspeicher{f}(:,78)= (offlinespeicher{f}(1,2)+ (calcspeicher{f}(:,57).*calcspeicher{f}(:,77)))./calcspeicher{f}(:,3) % BM g/L Yxs function
    calcspeicher{f} (:,79)= rawspeicher{f} (:,71)*MX./calcspeicher{f}(:,3) % BM g/L Softsensor based

end


% %% Yxs function determination
% 
% for i=1:length(Exp) 
%     
%      f=Exp(i);
%      cc=Ferm{f};
% 
% Yxfunction{f} (:,1) = calcspeicher{f} (:,62)
% Yxfunction{f} (:,2) = calcspeicher{f} (:,63)
% YxfunctionOff{f} (:,1) = calcspeicher{f}(offline_qscum{f}(:,2),62)
% YxfunctionOff{f} (:,2) = calcspeicher{f} (offline_qscum{f}(:,2),63)
% 
% end
% 

%% plot Rate calc volume balance vs weight signal

EXPT= 30

plot (calcspeicher{EXPT}(:,1),calcspeicher{EXPT}(:,3)*1000,calcspeicher{EXPT}(:,1),rawspeicher{EXPT}(:,51))


%% PLot Offline BM vs recBM and softsensorData
Exp=60
for i=1:length(Exp) 
%     s=1:length(S)
     f=Exp(i);
     cc=Ferm{f};

% select variables and labels     

%label_x1={'dSn [g/g]'};
label_x1={'Time after Induction [h]'};

variable_y1= 22;       
label_y1={'BM [g/L]'};
title1= {'Biomass offline'}

variable_y2= 74;
label_y2={'BM Perm SS [g/L]'};

variable_y3= 78;
label_y3={'BM Yxs'};

variable_y4= 79;
label_y4={'BM SS'};



%Select x axis insert:
    % plot(calcspeicher{f}(offline_qscum{f}(:,2),1),.... For TIME
    % plot(calcspeicher{f}(offline_qscum{f}(:,2),62),.... For dSn

%Select xlim:    
xaxis=[]; 

subplot(211)
   
    hold on
    
    plot(calcspeicher{f}(offline_qscum{f}(:,2),1),calcspeicher{f}(offline_qscum{f}(:,2),(variable_y1)),'color','red','linestyle','none','marker','x','markersize',10,'LineWidth',2)
   plot(calcspeicher{f}(:,1),calcspeicher{f}(:,(variable_y2)),'color','black','linestyle','-' ,'LineWidth',1)
  plot(calcspeicher{f}(:,1),calcspeicher{f}(:,(variable_y3)),'color','cyan','linestyle','--','LineWidth',1)
  plot(calcspeicher{f}(:,1),calcspeicher{f}(:,(variable_y4)),'color','blue','linestyle',':','LineWidth',1)

    ylabel(label_y1);
    xlabel(label_x1);     
%     set(gca, 'xlim', xaxis)
     legend({'offline', 'BM rec', 'Yxs' , 'SS'},'fontsize',10,'FontAngle','italic','Location','southeast');
%     title(title1)
%     grid on
%     set(gca,'Xgrid','off')
%     set(legend,'DataAspectRatioMode','manual');
%     set(legend,'DataAspectRatio',lar);
%     set(gca, 'ylim', [0 100]) %,'ytick',[0 0.05 0.1 0.15 0.2])    
% %      

subplot(212)
hold on
    
[AX,H1,H2]=plotyy(calcspeicher{f}(:,1),(calcspeicher{f}(:,27)),calcspeicher{f}(:,1),calcspeicher{f}(:,12)); %Permittivity
set(AX(1),'NextPlot','add') 
H3=plot(AX(1),calcspeicher{f}(:,1),1,'color','black','linestyle','-','LineWidth',1);

set(AX(1),'ylim',[0 2],'ytick',[0 0.5 1 1.5 2 ])
set(AX(2),'ylim',[0 20],'ytick',[0 5 10 15 20 ]) 
set(get(AX(1),'Ylabel'),'String','C-balance')
set(get(AX(2),'Ylabel'),'String','h-value')
labels={'C-balance', 'h value[g/L]'}; 
legend([H1,H2], labels ,'fontsize',10,'FontName','Arial','FontAngle','italic','Location','northwest');
    

 end

%% comparison of BM estimation methods
Exp=27

for i=1:length(Exp) 
%     s=1:length(S)
     f=Exp(i);
     cc=Ferm{f};

% select variables and labels     

%label_x1={'dSn [g/g]'};
label_x1={'Time after Induction [h]'};

variable_y1= 22;       
label_y1={'BM [g/L]'};
title1= {'Biomass offline'}

variable_y2= 75;
label_y2={'BM Perm SS [g/L]'};

variable_y3= 78;
label_y3={'BM Yxs'};


variable_y4= 79;
label_y4={'BM SS'};



%Select x axis insert:
    % plot(calcspeicher{f}(offline_qscum{f}(:,2),1),.... For TIME
    % plot(calcspeicher{f}(offline_qscum{f}(:,2),62),.... For dSn

%Select xlim:    
xaxis=[0 50]; 


   
    hold on
    
    plot(calcspeicher{f}(offline_qscum{f}(:,2),1),calcspeicher{f}(offline_qscum{f}(:,2),(variable_y1)),'color','red','linestyle','none','marker','x','markersize',10,'LineWidth',2)
   plot(calcspeicher{f}(:,1),calcspeicher{f}(:,(variable_y2)),'color','black','linestyle','-' ,'LineWidth',1)
  plot(calcspeicher{f}(:,1),calcspeicher{f}(:,(variable_y3)),'color','cyan','linestyle','--','LineWidth',1)
  plot(calcspeicher{f}(:,1),calcspeicher{f}(:,(variable_y4)),'color','blue','linestyle',':','LineWidth',1)

    ylabel(label_y1);
    xlabel(label_x1);     
%     set(gca, 'xlim', xaxis)
     legend({'offline', 'Perm', 'Yxs' , 'SS'},'fontsize',10,'FontAngle','italic','Location','southeast');
%     title(title1)
%     grid on
%     set(gca,'Xgrid','off')
%     set(legend,'DataAspectRatioMode','manual');
%     set(legend,'DataAspectRatio',lar);
%     set(gca, 'ylim', [0 100]) %,'ytick',[0 0.05 0.1 0.15 0.2])    
% %      

 end
% 
% % save plot as an png file
%  print(filename, '-dpng', '-r400');












