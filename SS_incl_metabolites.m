function [ output ] = MetDec(F_s,F_n,F_a_in,F_o2_in,y_o2_out,y_co2_out,Vol,c_s,c_n,M_s,y_wet,y_o2_in_air,y_co2_in_air,gamma_s,gamma_o2,gamma_x, gamma_met,Z_n) %#codegen

%----------------inputs:---------------------------------------------------------------------------------------------------------------------------------------------------------------------
%0.102,0.055,4,0.53,0.19,0.066,1,647.311,5.1,30.66,0.2077,0.2094,0.00039,4.6667,-4,4.23,3.5,0.23
%----------------inputs:

% calculated variables (internally) 
y_o2_in=(F_a_in.*y_o2_in_air+F_o2_in)./(F_a_in+F_o2_in);
y_co2_in=(F_a_in.*y_co2_in_air)./(F_a_in+F_o2_in);

ex_h2o=1-(y_wet./y_o2_in_air);
Ra_inert = (1-y_o2_in-y_co2_in)./(1-y_o2_out-y_co2_out-ex_h2o);
V_m=22.4;

% calculated rates-----------------------------------------------------------------------------------------------------------------------

r_s = -(F_s*c_s)/M_s;% rsm in C-mol/h
r_n=-(F_n*c_n);% rsm in C-mol/h

r_co2 = (((F_a_in+F_o2_in).*60)./V_m).*(y_co2_out.*Ra_inert-y_co2_in);
r_o2 = (((F_a_in+F_o2_in).*60)./V_m).*(y_o2_out.*Ra_inert-y_o2_in);

%r_s = -2.15;%
%r_co2 =0.8536;
%r_o2 =-1.184;
%r_n=-0.2842;

%gamma_s=4.667;
%gamma_o2=-4;
%gamma_x=4.2;
%gamma_met=3.6;
%Z_n=.2;

% -----------K3S1:Considering metabolite formation in the reaction stoichiometry (oxidoreductive metabolism)
xm=[r_s; r_o2; r_n;r_co2];
Em=[+1,0,0,+1;gamma_s,gamma_o2,0,0;0,0,1,0]; 
Ec =[1,1; gamma_x,gamma_met;Z_n,0]; 

e_s=0.03;
e_o2=0.03;
e_x=0.03;
e_co2=0.03;
e_n=0.03;

Xi=[e_s 0 0 0;0 e_o2 0 0;0 0 e_n 0;0 0 0 e_co2];
   
Ec_star=(inv(Ec'*Ec))*Ec';

R=Em-Ec*Ec_star*Em;
[U,S,V]=svd(R);
Sconv=[1 0 0];
C=Sconv*S;
K=C*S'*U';
Rred=K*R;
eps = Rred * xm;
sai = diag(diag(xm * xm' * Xi * Xi'));
Phi = Rred * sai *Rred';
delta = (sai*Rred'*inv(Phi) * Rred)* xm;
xmbest_oxred=xm-delta; 
xcbest_oxred = -Ec_star*Em*xmbest_oxred;

h_oxred = eps' * inv(Phi) * eps;

% -----------K3S2:Considering no metabolite formation in the reaction stoichiometry (oxidative metabolism)

xm=[r_s; r_o2; r_n;r_co2];
Em=[+1,0,0,+1;gamma_s,gamma_o2,0,0;0,0,1,0]; 
Ec =[1; gamma_x;Z_n]; 

Ec_star=(inv(Ec'*Ec))*Ec';

R=Em-Ec*Ec_star*Em;
RankR = rank(R); 
[U,S,V]=svd(R);
Sconv=[1 0 0;0 1 0];
C=Sconv*S;
K=C*S'*U';
Rred=K*R;
eps = Rred * xm;
sai = diag(diag(xm * xm' * Xi * Xi'));
Phi = Rred * sai *Rred';
delta = (sai*Rred'*inv(Phi) * Rred)* xm;
xmbest_ox=xm-delta; 
xcbest_ox = -Ec_star*Em*xmbest_ox;

h_ox = eps' * inv(Phi) * eps;

h1_ox = 2.77; % K3S2 - 2 Redundanz at 75% confidence level
h2_ox = 7.38; % -----''--------- at 97.5% confidence level

h1_oxred = 1.32; % K3S1 - 1 Redundanz at 75% confidence level
h2_oxred = 5.02; % -----''--------- at 97.5% confidence level

%Metabolite state detection------------------------------------------------------------------------------------

if h_ox < h1_ox && h_oxred < h2_oxred
    flag=1; %(Oxidative metabolism) refuse H0 that there is no ox metabolism = there is ox. && refuse there is no oxidoreductive metabolism = there is oxred.
    elseif h_ox > h2_ox && h_oxred < h2_oxred
    flag=2; %(Oxidoreductive metabolism)accept H0 no ox = no ox && refuse H0 no oxred = oxred.
    elseif h1_ox < h_ox < h2_ox && h_oxred < h2_oxred
    flag=3; %(Undefiend)H0 is not clear, hox is between 75-97.5% = undefined && refuse H0 oxred = oxred.    
    elseif h_ox < h2_ox && h_oxred > h2_oxred
    flag=4; %(Noise in the measurments) refuse no ox = ox && accept no oxred = no oxred. = noise in the measurement
    elseif h_oxred > h2_oxred && h_ox > h2_ox
    flag=5;%(Gross error in the measurments) % accept both ox and oxred state = not possible, gross error.  
    else
    flag =6;
end           


    
% volumetric reconciled rates----------
rs_rec=xmbest_ox(1,1)./Vol;
ro2_rec=xmbest_ox(2,1)./Vol;
rn_rec=xmbest_ox(3,1)./Vol;
rco2_rec=xmbest_ox(4,1)./Vol;
rx_rec=xcbest_ox(1,1)./Vol;
rmet_rec=xcbest_oxred(2,1)./Vol;
% reconciled yields------------
Yx_s=-rx_rec./rs_rec;
Ymet_s=-rmet_rec./rs_rec;
Yn_s=rn_rec./rs_rec;
Y_o2_s=ro2_rec./rs_rec;
Y_co2_s=-rco2_rec./rs_rec;
Y_o2_x=-ro2_rec./rx_rec;
Y_co2_x=rco2_rec./rx_rec;

if h_ox < h1_ox && h_oxred < h2_oxred
    rmet_rec=0;
    Ymet_s=0;
end
   


output=[flag,h_ox,h_oxred,rs_rec,ro2_rec,rn_rec,rco2_rec,rx_rec,rmet_rec,Yx_s,Yn_s,Y_o2_s,Y_co2_s,Ymet_s,Y_o2_x,Y_co2_x];
    
    
end

  
  
 
 
 
 
  
  


