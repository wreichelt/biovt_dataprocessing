%%
qs_amp=[];
qs_amp1=[];
qs_amp2=[];
qs_amp3=[];

for k=1:length(Fex)
    Fexn=[];
    Fexn=Fex{k};
            if  strcmp(Fexn,'xend')        %skip xend
            continue    
            elseif  strcmp(Fexn,'WR31C') 
                amp_set=const_Pool.(Fexn).amp_set.C;            %amplitude setpoints, set for single experiments
            elseif  strcmp(Fexn,'WR31D')
                amp_set=const_Pool.(Fexn).amp_set.D;
            elseif  strcmp(Fexn,'WR33A')
                amp_set=const_Pool.(Fexn).amp_set.A;
            elseif  strcmp(Fexn,'WR35A')
                amp_set=const_Pool.(Fexn).amp_set.A;
            elseif  strcmp(Fexn,'WR35B')
                amp_set=const_Pool.(Fexn).amp_set.B;
            elseif  strcmp(Fexn,'WR35C')
                amp_set=const_Pool.(Fexn).amp_set.C;
            elseif  strcmp(Fexn,'WR35D')
                amp_set=const_Pool.(Fexn).amp_set.D;
            elseif  strcmp(Fexn,'WR37A')
                amp_set=const_Pool.(Fexn).amp_set.A;
            elseif  strcmp(Fexn,'WR37B')
                amp_set=const_Pool.(Fexn).amp_set.B;
            elseif  strcmp(Fexn,'WR37C')
                amp_set=const_Pool.(Fexn).amp_set.C;
            elseif  strcmp(Fexn,'WR37D')
                amp_set=const_Pool.(Fexn).amp_set.D;
            end
                stop=[];
%                 Pool.xend.(x).(Fstrn).(Fvarn)
                stop=Pool.xend.(x).(Fexn).calc.qs;    
    qs_amp{k}(:,1)=Pool.(Fexn).calc.qs(1:stop,2);                 %qs real
    % qs_amp{k}(:,2)=mean(calc{k}(:,13))+(1.3*amp_set);   %soll-wert mean high
    qs_amp{k}(:,2)=mean(Pool.(Fexn).calc.qs(1:stop,2))+(0.65*amp_set);             %obere Grenze oben
    % qs_amp{k}(:,4)=0.85*qs_amp{k}(:,2);                             %untere Grenze oben
    % qs_amp{k}(:,5)=mean(calc{k}(:,13))-(1.3*amp_set);    %soll-wert mean low
    qs_amp{k}(:,3)=mean(Pool.(Fexn).calc.qs(1:stop,2))-(0.65*amp_set);             %obere Grenze unten
    % qs_amp{k}(:,7)=0.7*qs_amp{k}(:,5);             %untere Grenze unten
    qs_amp{k}(:,4)=qs_amp{k}(:,1)>qs_amp{k}(:,2); %find values between upper boundaries
    qs_amp{k}(:,5)=qs_amp{k}(:,1)<qs_amp{k}(:,3); %find values between lower boundaries
    qs_amp{k}(:,6)=mean(Pool.(Fexn).calc.qs(1:stop,2));           %mean qs induction phase
    qs_amp1{k}(:,1)=find(qs_amp{k}(:,4)==1);                        %write indices of values for upper qs level calculation
    qs_amp1{k}(:,2)=mean(qs_amp{k}(qs_amp1{k}(:,1),1));             %calculate mean high qs level
    qs_amp2{k}(:,1)=find(qs_amp{k}(:,5)==1);                        %write indices of values for lower qs level calculation
    qs_amp2{k}(:,2)=mean(qs_amp{k}(qs_amp2{k}(:,1),1));             %calculate mean low qs level
    qs_amp1{k}(:,3)=std(qs_amp{k}(qs_amp1{k}(:,1),1));              %std of qs high
    qs_amp2{k}(:,3)=std(qs_amp{k}(qs_amp2{k}(:,1),1));              %std of qs low
    qs_amp1{k}(:,4)=qs_amp1{k}(:,3)./qs_amp1{k}(:,2);               %error qs high (std / mean)
    qs_amp2{k}(:,4)=qs_amp2{k}(:,3)./qs_amp2{k}(:,2);               %error qs low (std / mean)
    qs_amp2{k}(:,5)=1.2*qs_amp2{k}(:,2);                %qs low + error
    qs_amp2{k}(:,6)=0.8*qs_amp2{k}(:,2);                %qs low - error
    qs_amp1{k}(:,5)=1.2*qs_amp1{k}(:,2);                %qs high + error
    qs_amp1{k}(:,6)=0.8*qs_amp1{k}(:,2);                %qs high - error
    qs_amp3{k}(:,1)=qs_amp1{k}(1,2)-qs_amp{k}(1,6);
    qs_amp3{k}(:,2)=qs_amp{k}(1,6)-qs_amp2{k}(1,2);
    qs_amp3{k}(:,3)=(qs_amp3{k}(1,1)+qs_amp3{k}(1,2))./2;

           
end

% 
% %% plotting
% Ende=31
% xaxis = [17.8,Ende];      %set limits for x-axis
% l=2;
% f1=24;
% f2=22;
% set(0,'DefaultAxesFontSize',12)  % set Font size of axis Labels, Legend, Axis numbers
% 
% % exp A
% subplot(111)
% % for k = k1:k2
% hold all
% 
%     %C(k) = plot(calc{2}(ind:end,1),calc{2}(ind:end,qs),'-*','LineWidth',l);
%     A(1) = plot(calc{1}(ind:end,1),calc{1}(ind:end,13),'-','color','k','LineWidth',1.5);
%     A(2) = plot(calc{1}(ind:end,1),qs_amp{1}(:,2),'-','color','r','LineWidth',1.5);
%     A(3) = plot(calc{1}(ind:end,1),qs_amp{1}(:,3),'-','color','r','LineWidth',1.5);
%     A(4) = line([0 200],[qs_amp1{1}(1,2) qs_amp1{1}(1,2)],'color','m','LineWidth',1.5);
%     A(5) = line([0 200],[qs_amp2{1}(1,2) qs_amp2{1}(1,2)],'color','m','LineWidth',1.5);
% %     line([0 200],[qs_amp2{1}(1,5) qs_amp2{1}(1,5)],'linestyle','--');
% %     line([0 200],[qs_amp2{1}(1,6) qs_amp2{1}(1,6)],'linestyle','--');
% %     line([0 200],[qs_amp1{1}(1,5) qs_amp1{1}(1,5)],'linestyle','--');
% %     line([0 200],[qs_amp1{1}(1,6) qs_amp1{1}(1,6)],'linestyle','--');
%     
% %   D = plot(qs_amp1{1}(:,2),'-');
% %   E = plot(qs_amp2{1}(:,2),'-');
% %     F = plot(qs_amp1{1}(:,5),'-');
% %     G = plot(qs_amp1{1}(:,6),'-');
% %     H = plot(qs_amp2{1}(:,5),'-');
% %     I = plot(qs_amp2{1}(:,6),'-');
% % end
% xlabel('Process Time [h]', 'FontSize', f1)
% ylabel('qs', 'FontSize', f1)
% title('amplitude calculation', 'FontSize', f2)
% % legend(t_induktion,{'EndFB'},'fontsize',7,'FontName','Arial','FontAngle','italic','Location','NorthEast');
% legend( A(1:5) , {['qs trajectory'] ['limit for upper qs level'] ['limit for lower qs level'] ['qs mean high/low']},'fontsize',24,'FontName','Arial','FontAngle','italic','Location','northwest');
% set(gca, 'xlim', xaxis)
% set(gca, 'ylim', [0 0.8])
% hold off
% 
% 
% % 
% % % % exp B
% % subplot(222)
% % % for k = k1:k2
% % hold all
% % 
% %     %C(k) = plot(calc{2}(ind:end,1),calc{2}(ind:end,qs),'-*','LineWidth',l);
% %     A(1) = plot(calc{2}(ind:end,1),calc{2}(ind:end,13),'-','color','k','LineWidth',0.5);
% %     A(2) = plot(calc{2}(ind:end,1),qs_amp{2}(:,2),'-','color','r');
% %     A(3) = plot(calc{2}(ind:end,1),qs_amp{2}(:,3),'-','color','r');
% %     A(4) = line([0 200],[qs_amp1{2}(1,2) qs_amp1{2}(1,2)],'color','m');
% %     A(5) = line([0 200],[qs_amp2{2}(1,2) qs_amp2{2}(1,2)],'color','m');
% % %     line([0 200],[qs_amp2{1}(1,5) qs_amp2{1}(1,5)],'linestyle','--');
% % %     line([0 200],[qs_amp2{1}(1,6) qs_amp2{1}(1,6)],'linestyle','--');
% % %     line([0 200],[qs_amp1{1}(1,5) qs_amp1{1}(1,5)],'linestyle','--');
% % %     line([0 200],[qs_amp1{1}(1,6) qs_amp1{1}(1,6)],'linestyle','--');
% %     
% % %   D = plot(qs_amp1{1}(:,2),'-');
% % %   E = plot(qs_amp2{1}(:,2),'-');
% % %     F = plot(qs_amp1{1}(:,5),'-');
% % %     G = plot(qs_amp1{1}(:,6),'-');
% % %     H = plot(qs_amp2{1}(:,5),'-');
% % %     I = plot(qs_amp2{1}(:,6),'-');
% % % end
% % xlabel('Process Time [h]', 'FontSize', f1)
% % ylabel('qs', 'FontSize', f1)
% % title('amplitude calculation', 'FontSize', f2)
% % % legend(t_induktion,{'EndFB'},'fontsize',7,'FontName','Arial','FontAngle','italic','Location','NorthEast');
% % legend( A(1:5) , {['qs trajectory'] ['limit for upper qs level'] ['limit for lower qs level'] ['qs mean high/low']},'fontsize',12,'FontName','Arial','FontAngle','italic','Location','northwest');
% % set(gca, 'xlim', xaxis)
% % set(gca, 'ylim', [0 0.8])
% % hold off
% % % 
% % % 
% % % 
% % % % exp C
% % subplot(223)
% % % for k = k1:k2
% % hold all
% % 
% %     %C(k) = plot(calc{2}(ind:end,1),calc{2}(ind:end,qs),'-*','LineWidth',l);
% %     A(1) = plot(calc{3}(ind:end,1),calc{3}(ind:end,13),'-','color','k','LineWidth',0.5);
% %     A(2) = plot(calc{3}(ind:end,1),qs_amp{3}(:,2),'-','color','r');
% %     A(3) = plot(calc{3}(ind:end,1),qs_amp{3}(:,3),'-','color','r');
% %     A(4) = line([0 200],[qs_amp1{3}(1,2) qs_amp1{3}(1,2)],'color','m');
% %     A(5) = line([0 200],[qs_amp2{3}(1,2) qs_amp2{3}(1,2)],'color','m');
% % %     line([0 200],[qs_amp2{1}(1,5) qs_amp2{1}(1,5)],'linestyle','--');
% % %     line([0 200],[qs_amp2{1}(1,6) qs_amp2{1}(1,6)],'linestyle','--');
% % %     line([0 200],[qs_amp1{1}(1,5) qs_amp1{1}(1,5)],'linestyle','--');
% % %     line([0 200],[qs_amp1{1}(1,6) qs_amp1{1}(1,6)],'linestyle','--');
% %     
% % %   D = plot(qs_amp1{1}(:,2),'-');
% % %   E = plot(qs_amp2{1}(:,2),'-');
% % %     F = plot(qs_amp1{1}(:,5),'-');
% % %     G = plot(qs_amp1{1}(:,6),'-');
% % %     H = plot(qs_amp2{1}(:,5),'-');
% % %     I = plot(qs_amp2{1}(:,6),'-');
% % % end
% % xlabel('Process Time [h]', 'FontSize', f1)
% % ylabel('qs', 'FontSize', f1)
% % title('amplitude calculation', 'FontSize', f2)
% % % legend(t_induktion,{'EndFB'},'fontsize',7,'FontName','Arial','FontAngle','italic','Location','NorthEast');
% % legend( A(1:5) , {['qs trajectory'] ['limit for upper qs level'] ['limit for lower qs level'] ['qs mean high/low']},'fontsize',12,'FontName','Arial','FontAngle','italic','Location','northwest');
% % set(gca, 'xlim', xaxis)
% % set(gca, 'ylim', [0 0.8])
% % hold off
% % % 
% % % % exp D
% % subplot(224)
% % % for k = k1:k2
% % hold all
% % 
% %     %C(k) = plot(calc{2}(ind:end,1),calc{2}(ind:end,qs),'-*','LineWidth',l);
% %     A(1) = plot(calc{4}(ind:end,1),calc{4}(ind:end,13),'-','color','k','LineWidth',0.5);
% %     A(2) = plot(calc{4}(ind:end,1),qs_amp{4}(:,2),'-','color','r');
% %     A(3) = plot(calc{4}(ind:end,1),qs_amp{4}(:,3),'-','color','r');
% %     A(4) = line([0 200],[qs_amp1{4}(1,2) qs_amp1{4}(1,2)],'color','m');
% %     A(5) = line([0 200],[qs_amp2{4}(1,2) qs_amp2{4}(1,2)],'color','m');
% % %     line([0 200],[qs_amp2{1}(1,5) qs_amp2{1}(1,5)],'linestyle','--');
% % %     line([0 200],[qs_amp2{1}(1,6) qs_amp2{1}(1,6)],'linestyle','--');
% % %     line([0 200],[qs_amp1{1}(1,5) qs_amp1{1}(1,5)],'linestyle','--');
% % %     line([0 200],[qs_amp1{1}(1,6) qs_amp1{1}(1,6)],'linestyle','--');
% %     
% % %   D = plot(qs_amp1{1}(:,2),'-');
% % %   E = plot(qs_amp2{1}(:,2),'-');
% % %     F = plot(qs_amp1{1}(:,5),'-');
% % %     G = plot(qs_amp1{1}(:,6),'-');
% % %     H = plot(qs_amp2{1}(:,5),'-');
% % %     I = plot(qs_amp2{1}(:,6),'-');
% % % end
% % xlabel('Process Time [h]', 'FontSize', f1)
% % ylabel('qs', 'FontSize', f1)
% % title('amplitude calculation', 'FontSize', f2)
% % % legend(t_induktion,{'EndFB'},'fontsize',7,'FontName','Arial','FontAngle','italic','Location','NorthEast');
% % legend( A(1:5) , {['qs trajectory'] ['limit for upper qs level'] ['limit for lower qs level'] ['qs mean high/low']},'fontsize',12,'FontName','Arial','FontAngle','italic','Location','northwest');
% % set(gca, 'xlim', xaxis)
% % set(gca, 'ylim', [0 0.8])
% % hold off
% 
% 
% %% Save
% 
% filename=Experiment;
% save (['qs_amp_' filename], 'qs_amp' )
% save (['qs_amp1_' filename], 'qs_amp1' )
% save (['qs_amp2_' filename], 'qs_amp2' )
% %% 
% % exp A
% subplot(111)
% % for k = k1:k2
% hold all
% 
%     %C(k) = plot(calc{2}(ind:end,1),calc{2}(ind:end,qs),'-*','LineWidth',l);
%     A = plot(calc{1}(ind:end,1),calc{1}(ind:end,13),calc{1}(ind:end,1),qs_amp{1}(:,2),calc{1}(ind:end,1),qs_amp{1}(:,3));
% %     B = plot(calc{1}(ind:end,1),qs_amp{1}(:,2),'-','color','r');
% %     C = plot(calc{1}(ind:end,1),qs_amp{1}(:,3),'-','color','r');
%     line([0 200],[qs_amp1{1}(1,2) qs_amp1{1}(1,2)],'color','m');
%     line([0 200],[qs_amp2{1}(1,2) qs_amp2{1}(1,2)],'color','m');
% %     line([0 200],[qs_amp2{1}(1,5) qs_amp2{1}(1,5)],'linestyle','--');
% %     line([0 200],[qs_amp2{1}(1,6) qs_amp2{1}(1,6)],'linestyle','--');
% %     line([0 200],[qs_amp1{1}(1,5) qs_amp1{1}(1,5)],'linestyle','--');
% %     line([0 200],[qs_amp1{1}(1,6) qs_amp1{1}(1,6)],'linestyle','--');
%     
% %   D = plot(qs_amp1{1}(:,2),'-');
% %   E = plot(qs_amp2{1}(:,2),'-');
% %     F = plot(qs_amp1{1}(:,5),'-');
% %     G = plot(qs_amp1{1}(:,6),'-');
% %     H = plot(qs_amp2{1}(:,5),'-');
% %     I = plot(qs_amp2{1}(:,6),'-');
% xlabel('Process Time [h]', 'FontSize', f1)
% ylabel('qs', 'FontSize', f1)
% title('amplitude calculation', 'FontSize', f2)
% % legend(t_induktion,{'EndFB'},'fontsize',7,'FontName','Arial','FontAngle','italic','Location','NorthEast');
% legend( A , {['qs'] [''] [''] },'fontsize',12,'FontName','Arial','FontAngle','italic','Location','northwest');
% set(gca, 'xlim', xaxis)
% set(gca, 'ylim', [0 0.8])
% hold off
% 
% 
% 
