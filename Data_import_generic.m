%% Data import from L-feeding

% Scale signal must start with Int.W!!!!!
% mR_start automatically (feed-pump rate non zero??)


close all
clear all

Ex='WR38';
%set path
path=['Z:\CDLMIB\CD_Data\Module_1\03_RT3\03_Results\Experiments\' Ex '\Matlab Auswertung\'];    %specify path of experimental data
% path=['S:\BPT\BPT SHARED DATA\Projects\L_Feeding Strategies\03_Results\Feeding profile\'];    %specify path of experimental data
% addpath('S:\BPT\wreichelt\Matlab\biovt_dataprocessing');        %specify path of matlab files
% addpath('S:\BPT\BPT SHARED DATA\MSC\Markus Brillmann\bitbucket');
addpath('Z:\BPT\BPT SHARED DATA\MSC\Peter Thurrold\matlab');

cd(path);
%% set process constants

constants=struct;
constants.MS=30;              % [g/c-mol] Molecular weight substrate  (30.6966 glycerol; 30 Glucose)
constants.gamma_s=4;          % DoR Substrate (4,667 glycerol; 4 Glucose)
constants.yO2wet=20.7;          % DoR Substrate (4,667 glycerol; 4 Glucose)
constants.MAc = 29.525;             % [g/c-mol] Molecular weight acetate

%Experiment
constants.rho_feed = 1209;            % [g/l] Density Feed  not required if pump was calibrated
constants.c_s = 536.8;                  % [g/l] Substrate concentration
constants.mR_start = 1;               % Reactor Vol [l]
constants.base_mol = 6.28;                 % molarity of base (mol/L)
constants.amp_set.A= 0.1;                
constants.amp_set.B= 0.1;
constants.amp_set.C= 0.2;
constants.amp_set.D= 0.2;

save(['constants_' Ex], 'constants')

% save(['constants_' Experiment], 'constants')

%% data import

% Tabs={'1a' '1b' '1c' '1d' '1e' '1f' '1g' };
Tabs={'Data1' 'Data2' 'Data3' 'Data4' 'Offline1' 'Offline2' 'Offline3' 'Offline4'};

k1=1;
k2=length(Tabs);

exp=struct;
exp.num=struct;
exp.txt=struct;

tic
for k=k1:k2
    
    num=[];
    txt=[];
    raw=[];
    [num,txt,raw]=xlsread([path '20150213_WR38_raw_as'],Tabs{k});
    name=Tabs{k};
    exp.num.(['exp_' (name)])=[];
    exp.num.(['exp_' (name)])=num;
    exp.txt.(['exp_' (name)])=[];
    exp.txt.(['exp_' (name)])=txt;
    
    
end
toc




%% experiment specific calculations



% group_A={'1a' '1b' '1c' '1d' '1e' '1f' '1g'};
group_A_online={'Data1' 'Data2' 'Data3' 'Data4' };
group_A_offline={'Offline1' 'Offline2' 'Offline3' 'Offline4' };


exp.analytics=struct;
exp.corr=struct;

for k=k1:length(group_A_online)
    
    name_online=group_A_online{k};
    name_offline=group_A_offline{k};
    c=length(exp.txt.(['exp_' (name_online)])(1,:));    %how many columns  does the header section have
    
    %Time [h]-> h!
    exp.corr.(['exp_' (name_online)])(:,1).time=exp.num.(['exp_' (name_online)])(:,1).*24;
   
   
    %Offgas CO2 ->target unit is [%]
    expression='XCO2......[%]?';                        %specify the name of your variable number of letters MUST match use '.' for arbitrary letters or numbers or spaces
    target=regexpi(exp.txt.(['exp_' (name_online)])(1,:),expression); %compares all strings
    
    for ii=1:c
        if target{ii}==1
            CO2=ii-1;
        end
    end
    
    if exist('CO2', 'var')
        exp.corr.(['exp_' (name_online)]).CO2(:,1)=exp.corr.(['exp_' (name_online)]).time(:,1); %[h]
        exp.corr.(['exp_' (name_online)]).CO2(:,2)=exp.num.(['exp_' (name_online)])(:,CO2); %[%]
        
        
%         t=exp.num.(['exp_' (name_online)])(:,1);           %if calculated from the total volume then smoothing is essential!
%         f=exp.num.(['exp_' (name_online)])(:,CO2);
%         exp.num.(['exp_' (name_online)])(:,CO2)=savGol(f,t,15,15,3);       %smoothing
%         
%         exp.corr.(['exp_' (name_online)]).Air(:,1)=exp.corr.(['exp_' (name_online)]).time(:,1); %[h]
%         exp.corr.(['exp_' (name_online)]).Air(:,2)=exp.num.(['exp_' (name_online)])(:,CO2); 
    else
        msgbox(['CO2 offgas' (expression) 'not found'])
    end
    
    
    %Offgas O2 ->target unit is [%]
    expression='XO2......[%]?';
    target=regexpi(exp.txt.(['exp_' (name_online)])(1,:),expression);
    
    for ii=1:c
        if target{ii}==1
            O2=ii-1;
        end
    end
    
    if exist('O2', 'var')
        exp.corr.(['exp_' (name_online)]).O2(:,1)=exp.corr.(['exp_' (name_online)]).time(:,1); %[h]
        exp.corr.(['exp_' (name_online)]).O2(:,2)=exp.num.(['exp_' (name_online)])(:,O2); %[%]
        
%         t=exp.num.(['exp_' (name_online)])(:,1);           %if calculated from the total volume then smoothing is essential!
%         f=exp.num.(['exp_' (name_online)])(:,O2);
%         exp.num.(['exp_' (name_online)])(:,O2)=savGol(f,t,20,20,4);       %smoothing
%         
%         exp.corr.(['exp_' (name_online)]).O2(:,1)=exp.corr.(['exp_' (name_online)]).time(:,1); %[h]
%         exp.corr.(['exp_' (name_online)]).O2(:,2)=exp.num.(['exp_' (name_online)])(:,CO2); 
%     
        
    else
        msgbox(['O2 offgas' (expression) 'not found'])
    end
    
    
    
    %Gassing Air -> target unit [sl/h]
    expression='FAir..PV[sL/h]?';
    target=regexpi(exp.txt.(['exp_' (name_online)])(1,:),expression);
    
    for ii=1:c
        if target{ii}==1
            Air=ii-1;
        end
    end
    
    if exist('Air', 'var')
  
%         t=exp.num.(['exp_' (name_online)])(:,1);           %if calculated from the total volume then smoothing is essential!
%         f=exp.num.(['exp_' (name_online)])(:,Air);
%         exp.num.(['exp_' (name_online)])(:,Air)=savGol(f,t,17,17,6);       %smoothing
         
        exp.corr.(['exp_' (name_online)]).Air(:,1)=exp.corr.(['exp_' (name_online)]).time(:,1); %[h]
        exp.corr.(['exp_' (name_online)]).Air(:,2)=exp.num.(['exp_' (name_online)])(:,Air); 
%         exp.corr.(['exp_' (name_online)])(:,4)=exp.num.(['exp_'(name_online)])(:,Feed); %[ml/h] -> [ml/h]  
%         WARNING if Feed is used imported in ml/h watch out for reactor weight vol balance!
        
    else
        msgbox(['Air' (expression) 'not found'])
    end
    
    
    %Gassing Oxygen ->target unit [sl/h]
    expression='FO2...PV [sL/h]?';
    target=regexpi(exp.txt.(['exp_' (name_online)])(1,:),expression);
    
    for ii=1:c
        if target{ii}==1
            Ox=ii-1;
        end
    end
    
    if exist('Ox', 'var')

%         t=exp.num.(['exp_' (name_online)])(:,1);           %if calculated from the total volume then smoothing is essential!
%         f=exp.num.(['exp_' (name_online)])(:,Ox);
%         exp.num.(['exp_' (name_online)])(:,Ox)=savGol(f,t,10,10,5);       %smoothing

        exp.corr.(['exp_' (name_online)]).Ox_gassing(:,1)=exp.corr.(['exp_' (name_online)]).time(:,1); %[h]
        exp.corr.(['exp_' (name_online)]).Ox_gassing(:,2)=exp.num.(['exp_' (name_online)])(:,Ox);    % [sl/h]
    else
        msgbox(['Ox gassing' (expression) 'not found'])
    end
    
    %Feeding -> target is ml/h
    expression='VA..PV [mL]?';
    target=regexpi(exp.txt.(['exp_' (name_online)])(1,:),expression);
    
    for ii=1:c
        if target{ii}==1
            Feed=ii-1;
        end
    end
    if exist('Feed', 'var')
        
        t=exp.num.(['exp_' (name_online)])(:,1);           %if calculated from the total volume then smoothing is essential!
        f=exp.num.(['exp_' (name_online)])(:,Feed);
        exp.num.(['exp_' (name_online)])(:,Feed)=savGol(f,t,9,9,2);       %smoothing
        
        exp.corr.(['exp_' (name_online)]).Feed(:,1)=exp.corr.(['exp_' (name_online)]).time(:,1); %[h]
        exp.corr.(['exp_' (name_online)]).Feed(:,2)=[0;diff(exp.num.(['exp_' (name_online)])(:,Feed))./diff(exp.corr.(['exp_' (name_online)]).time(:,1))]; %[ml]->[ml/h] calculate first derivate - *24 if time Achsis is in days
        %exp.corr.(['exp_' (name_online)])(:,4)=exp.num.(['exp_'(name_online)])(:,Feed); %[ml/h] -> [ml/h]  
        %WARNING if Feed is used imported in ml/h watch out for reactor weight vol balance!
        
    else
        msgbox(['Feed' (expression) 'not found'])
    end
    
    %Base  -> target is ml/h
    expression='FB..PV [mL/h]?';
    target=regexpi(exp.txt.(['exp_' (name_online)])(1,:),expression);
    
    for ii=1:c
        if target{ii}==1
            Base=ii-1;
        end
    end
    
    if exist('Base', 'var')
        exp.corr.(['exp_' (name_online)]).Base(:,1)=exp.corr.(['exp_' (name_online)]).time(:,1); %[h]
        exp.corr.(['exp_' (name_online)]).Base(:,2)=exp.num.(['exp_' (name_online)])(:,Base); % [ml/h]
    else
        msgbox(['Base' (expression) 'not found'])
    end
    
    %pH
    expression='pH..PV [pH]?';
    target=regexpi(exp.txt.(['exp_' (name_online)])(1,:),expression);
    
    for ii=1:c
        if target{ii}==1
            pH=ii-1;
        end
    end
    
    if exist('pH', 'var')
        exp.corr.(['exp_' (name_online)]).pH(:,1)=exp.corr.(['exp_' (name_online)]).time(:,1); %[h]
        exp.corr.(['exp_' (name_online)]).pH(:,2)=exp.num.(['exp_' (name_online)])(:,pH);         % [pH]
    else
        msgbox(['pH' (expression) 'not found'])
    end
    
    %DO2%       
    expression='DO..PV [%DO]?';
    target=regexpi(exp.txt.(['exp_' (name_online)])(1,:),expression);
    
    for ii=1:c
        if target{ii}==1
            DO2=ii-1;
        end
    end
    
    if exist('DO2', 'var')
        exp.corr.(['exp_' (name_online)]).DO2(:,1)=exp.corr.(['exp_' (name_online)]).time(:,1); %[h]
        exp.corr.(['exp_' (name_online)]).DO2(:,2)=exp.num.(['exp_' (name_online)])(:,DO2);       %[%]
    else
        msgbox(['Temp' (expression) 'not found'])
    end
    
    %Torque
    expression='N..PV [rpm]?';
    target=regexpi(exp.txt.(['exp_' (name_online)])(1,:),expression);
    
    for ii=1:c
        if target{ii}==1
            Torque=ii-1;
        end
    end
    
    if exist('Torque', 'var')
        exp.corr.(['exp_' (name_online)]).Torque(:,1)=exp.corr.(['exp_' (name_online)]).time(:,1); %[h]
        exp.corr.(['exp_' (name_online)]).Torque(:,2)=exp.num.(['exp_' (name_online)])(:,Torque);    %[rpm]
        
    else
        msgbox(['Torque undefined ' (expression) 'not found'])
    end
    
    
    %Temp
    expression='T..PV [�C]?';
    target=regexpi(exp.txt.(['exp_' (name_online)])(1,:),expression);
    
    for ii=1:c
        if target{ii}==1
            Temp=ii-1;
        end
    end
    
    if exist('Temp', 'var')
        exp.corr.(['exp_' (name_online)]).Temp(:,1)=exp.corr.(['exp_' (name_online)]).time(:,1); %[h]
        exp.corr.(['exp_' (name_online)]).Temp(:,2)=exp.num.(['exp_' (name_online)])(:,Temp);     %[�C]
    else
        msgbox(['Temp' (expression) 'not found'])
    end
    
    
   
    %Reactor Vol [g]      Beforehand it must be standardized!
                  
    expression='Int.W......[g].[]?';
    target=regexpi(exp.txt.(['exp_' (name_online)])(1,:),expression);

    
    for ii=1:c
        if target{ii}==1
            vR=ii-1;
        else
            
        end
        
    end
    
    if exist('vR', 'var')
        
        t=exp.num.(['exp_' (name_online)])(:,1);                %time
        f=exp.num.(['exp_' (name_online)])(:,vR);               %scale signal
        exp.num.(['exp_' (name_online)])(:,vR)=savGol(f,t,15,15,1);       %smoothing
        
        exp.corr.(['exp_' (name_online)]).vR(:,1)=exp.corr.(['exp_' (name_online)]).time(:,1); %[h] time
        exp.corr.(['exp_' (name_online)]).vR(:,2)=exp.num.(['exp_' (name_online)])(:,vR);     %[g]  smoothed scale signal
        scale.(['exp_' (name_online)])=1;
        
    else
        exp.corr.(['exp_' (name_online)]).vR(:,1)=exp.corr.(['exp_' (name_online)]).time(:,1); %[h]
        cum_Feed=exp.num.(['exp_' (name_online)])(:,Feed)./1000;       % [l] feed
        cum_Base=cumtrapz(exp.corr.(['exp_' (name_online)]).time(:,1),exp.num.(['exp_' (name_online)])(:,Base))./1000; %[l] base
        exp.corr.(['exp_' (name_online)]).vR(:,2)=(constants.mR_start+cum_Feed+cum_Base);       %volume balance [l]
        scale.(['exp_' (name_online)])=0;
        msgbox(['no Scale signal ' (expression) ' found, Volume balance calculated'])
    end
    
    
            %inoculation time
    expression='Ino..............[]?';                        %specify the name of your variable number of letters MUST match use '.' for arbitrary letters or numbers or spaces
    target=regexpi(exp.txt.(['exp_' (name_online)])(1,:),expression); %compares all strings
    
    for ii=1:c
        if target{ii}==1
            inoct=ii-1;
        end
    end
    
        
    if exist('inoct', 'var')
        exp.corr.(['exp_' (name_online)]).inoct(:,1)=exp.corr.(['exp_' (name_online)]).time(:,1); %[h]
        exp.corr.(['exp_' (name_online)]).inoct(:,2)=exp.num.(['exp_' (name_online)])(:,inoct)*24;     %[h]
    else
        msgbox(['inoculation time' (expression) 'not found'])
    end

    
    %permittivity
    expression='Ext.........................pf/cm]?';                        %specify the name of your variable number of letters MUST match use '.' for arbitrary letters or numbers or spaces
    target=regexpi(exp.txt.(['exp_' (name_online)])(1,:),expression); %compares all strings
    
    for ii=1:c
        if target{ii}==1
            perm=ii-1;
        end
    end

        
    if exist('perm', 'var')
        exp.corr.(['exp_' (name_online)]).perm(:,1)=exp.corr.(['exp_' (name_online)]).time(:,1); %[h]
        exp.corr.(['exp_' (name_online)]).perm(:,2)=exp.num.(['exp_' (name_online)])(:,perm);     %[h]
        
        
      t=exp.num.(['exp_' (name_online)])(:,1);           %if calculated from the total volume then smoothing is essential!
      f=exp.num.(['exp_' (name_online)])(:,perm);
        exp.num.(['exp_' (name_online)])(:,perm)=savGol(f,t,40,40,1);       %smoothing
        
        exp.corr.(['exp_' (name_online)]).perm(:,1)=exp.corr.(['exp_' (name_online)]).time(:,1); %[h]
        exp.corr.(['exp_' (name_online)]).perm(:,2)=exp.num.(['exp_' (name_online)])(:,perm); %[ml]->[ml/h] calculate first derivate - *24 if time Achsis is in days
%         exp.corr.(['exp_' (name_online)])(:,4)=exp.num.(['exp_'(name_online)])(:,Feed); %[ml/h] -> [ml/h]  
%         WARNING if Feed is used imported in ml/h watch out for reactor weight vol balance!
        
    else
        msgbox(['permittivity signal' (expression) 'not found'])
    end
      

            %Softsensor BM
    expression='Int.softs.........[cmol]?';                        %specify the name of your variable number of letters MUST match use '.' for arbitrary letters or numbers or spaces
    target=regexpi(exp.txt.(['exp_' (name_online)])(1,:),expression); %compares all strings
    
    for ii=1:c
        if target{ii}==1
            Softsens=ii-1;
        end
    end
    
        
    if exist('Softsens', 'var')
        exp.corr.(['exp_' (name_online)]).Softsens(:,1)=exp.corr.(['exp_' (name_online)]).time(:,1); %[h]
        exp.corr.(['exp_' (name_online)]).Softsens(:,2)=exp.num.(['exp_' (name_online)])(:,Softsens)*24;     %[h]
    else
        msgbox(['Softsensor BM' (expression) 'not found'])
    end    
    
        %offline data
        exp.analytics.(['exp_' (name_online)]).DCW(:,1)=exp.num.(['exp_' (name_offline)])(:,1).*24;     %time
        exp.analytics.(['exp_' (name_online)]).DCW(:,2)=exp.num.(['exp_' (name_offline)])(:,2);     %DCW
        
        exp.analytics.(['exp_' (name_online)]).product(:,1)=exp.num.(['exp_' (name_offline)])(:,1).*24;     %time
        exp.analytics.(['exp_' (name_online)]).product(:,2)=exp.num.(['exp_' (name_offline)])(:,9);     %product [g/l]
        
        exp.analytics.(['exp_' (name_online)]).product_sup(:,1)=exp.num.(['exp_' (name_offline)])(:,1).*24;     %time
        exp.analytics.(['exp_' (name_online)]).product_sup(:,2)=exp.num.(['exp_' (name_offline)])(:,8);     %product in supernatant [g/l]
        
        exp.analytics.(['exp_' (name_online)]).sampling(:,1)=exp.num.(['exp_' (name_offline)])(:,1).*24;     %time
        exp.analytics.(['exp_' (name_online)]).sampling(:,2)=exp.num.(['exp_' (name_offline)])(:,5);     %sampling cumulative![ml]
        
        exp.analytics.(['exp_' (name_online)]).acetate(:,1)=exp.num.(['exp_' (name_offline)])(:,1).*24;     %time
        exp.analytics.(['exp_' (name_online)]).acetate(:,2)=exp.num.(['exp_' (name_offline)])(:,10);     %Acetate [g/l]
        
        exp.analytics.(['exp_' (name_online)]).glucose(:,1)=exp.num.(['exp_' (name_offline)])(:,1).*24;     %time
        exp.analytics.(['exp_' (name_online)]).glucose(:,2)=exp.num.(['exp_' (name_offline)])(:,11);     %Glucose [g/l]
        
        exp.analytics.(['exp_' (name_online)]).protein(:,1)=exp.num.(['exp_' (name_offline)])(:,1).*24;     %time
        exp.analytics.(['exp_' (name_online)]).protein(:,2)=exp.num.(['exp_' (name_offline)])(:,7);     %protein [g/l]
        
  
        
        % End and induction def.
%         isnumber=find(isfinite(exp.analytics.(['exp_' (name_offline)]).DCW(:,1)));
%         exp.ind_offline.(['exp_' (name_offline)])=max(isnumber); %end of offline data
%         exp.ind_offline.(['exp_' (name_offline)])=min(isnumber); %end of offline data
end             
        
%% NAN removal from offline Data
        
       

for k=k1:length(group_A_online)

    name_online=group_A_online{k};

    Fn =fieldnames(exp.analytics.(['exp_' (name_online)]));

    for j=1:length(Fn)
        Fnn=Fn{j};
        nan=[];
        nan=isnan(exp.analytics.(['exp_' (name_online)]).(Fnn)(:,2));
        exp.analytics.(['exp_' (name_online)]).(Fnn)(find(nan==1),:)=[];
  
    end

end

        
   
        
%% find induction time
        induction=[];

for k=k1:length(group_A_online)
%     t=[];
%     f=[];
%     dx=[];
%     smoothed=[];
%     smoothed_diff=[];
% 
%     %     if k < 12
%     batch_time=0;
%     %     else batch_time=12;
%     %     end
% 
    name_online=group_A_online{k};
%     stop=floor(0.8*length(exp.corr.(['exp_' (name_online)]).time(:,1)));
%     time=abs(exp.corr.(['exp_' (name_online)]).time(:,1)-batch_time);
%     start=find(min(time) == time);
% 
% 
%     t=exp.corr.(['exp_' (name_online)]).Feed(:,1);
%     f=exp.corr.(['exp_' (name_online)]).Feed(:,2);
% 
%     smoothed=savGol(f,t,10,10,5);
%     exp.corr.(['exp_' (name_online)]).Feed(:,2)=smoothed;
%     dx=diff(smoothed);
%     t(end)=[];
%     start=start(1);
%     z=20;        %ml, nr should be equidistant for SavGol Smoothing
% 
%     smoothed_diff=savGol(dx,t,z,z,2);
%     cut=smoothed_diff(start:stop);
%     %maximum= max(cut(:,1));
%     minimum= min(cut(:,1));

    %automatd ind def.
    % change=find(maximum == abs(smoothed_diff));                                 % automated ind def.
    % change=find(minimum == (smoothed_diff));                                    % automated ind def.
    % induction.(['exp_' (name_online)])=change;                                  % automated ind def.

    %offline ind def.
    t_induktion=16.2; 
%     round(exp.num.(['exp_' (name_offline)])(2,1)*24*10)/10;          % [d] ind. time from offline data uncomment if automated ind def. is insufficient
    diff_time=abs(exp.corr.(['exp_' (name_online)]).time(:,1)-t_induktion);            % uncomment if automated ind def. is insufficient
    induction.(['exp_' (name_online)])=find(diff_time==min(diff_time));           % uncomment if automated ind def. is insufficient

end

        
%% check if induction selected induction time fits expectations
        
        
        
        close all
        for f=1:length(group_A_online)
            
            
            name_online=group_A_online{f};
            time=exp.corr.(['exp_' (name_online)]).time;
            ind=induction.(['exp_' (name_online)]) ;
            figure (f)
            plot(exp.corr.(['exp_' (name_online)]).Feed(:,1),exp.corr.(['exp_' (name_online)]).Feed(:,2))
            hold on
            plot([(time(ind,1)) (time(ind,1))],[0 200])
            
            set(gca,'ylim',[0 200])
            set(gca,'xlim',[0 50])
            
        end
        
%% transfer columns

        eval(['Ex_' (Ex) '.constants=constants;'])
        eval(['Ex_' (Ex) '.analytics=exp.analytics;'])
        eval(['Ex_' (Ex) '.induction=induction;'])
        eval(['Ex_' (Ex) '.corr=exp.corr;'])
        eval(['Ex_' (Ex) '.scale=scale;'])

        
%% Save
        save(['constants_' Ex], 'constants')
        save (['Ex_' Ex], ['Ex_' Ex] )
