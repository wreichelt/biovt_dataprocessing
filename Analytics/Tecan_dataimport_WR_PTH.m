%% Data import and evaluation of time measurements of Tecan Multiplatereader
%Wieland Reichelt & PTH

clc, clear all, close all

%Set Path


path=['Z:\BPT\BPT SHARED DATA\MSC\Peter Thurrold'];    %specify path of experimental data
addpath('Z:\BPT\BPT SHARED DATA\MSC\Peter Thurrold\biovt dataprocessing new');        %specify path of matlab files
cd(path);


    %% Load data to Matlab
tic

filename='20150113_WR-18_Sol.Kin_BYI.xlsx';             %specify filname of DasGip Export file
sheet_data='WR-18-SOLKIN-RAW DATA';                                       %specify sheet with process data 
[data_import,names,raw]=xlsread(filename,sheet_data);      %matrix where online data is stored

toc


%% assign layout timely measurement

time_sec=[];
time_min=[];

%time
first=find(strcmp('Cycle Nr.' ,raw(:,1)));
time_sec=cell2mat(raw(first+1,2:end)); %sec
time_min=time_sec./60; %min

%Data A1=1
last=find(strcmp('End Time:' ,raw(:,1)));
last=last-4;
tecan_data=cell2mat(raw(first+3:last,2:end)); %data only without time achsis

[r,c]=size(tecan_data);





%% define groups and Samples for calculation

i=1;
A=(i:1:i+12);
B=(1+12:1:i+24);
C=(i+24:1:i+36);
D=(i+36:1:i+48);
E=(i+48:1:i+60);
F=(i+60:1:i+72);
G=(i+72:1:i+84);
H=(i+84:1:i+96);

%% assign the protein concentrations according to the HPLC analysis after solubilisation
%adjust to 96-well plate setup
 Group=struct;
% Group.a1.rows=A(1:3);
% Group.a1.conc=0;
% Group.a1.rawmean=mean(tecan_data(1:3,:));
% Group.a2.rows=A(4:6);
% Group.a2.conc=0;
% Group.a2.rawmean=mean(tecan_data(4:6,:));
% Group.a3.rows=B(7:9);
% Group.a3.conc=0;
% Group.a3.rawmean=mean(tecan_data(7:9,:));
% Group.a4.rows=B(10:12);
% Group.a4.conc=0;
% Group.a4.rawmean=mean(tecan_data(10:12,:));


Group.b1.rows=B(1:3);
Group.b1.conc=0;
Group.b1.rawmean=mean(tecan_data(13:15,:));
Group.b2.rows=B(4:6);
Group.b2.conc=0;
Group.b2.rawmean=mean(tecan_data(16:18,:));
Group.b3.rows=B(7:9);
Group.b3.conc=0;
Group.b3.rawmean=mean(tecan_data(19:21,:));
Group.b4.rows=B(10:12);
Group.b4.conc=0;
Group.b4.rawmean=mean(tecan_data(22:24,:));

Group.c1.rows=C(1:3);
Group.c1.conc=0;
Group.c1.rawmean=mean(tecan_data(25:27,:));
Group.c2.rows=C(4:6);
Group.c2.conc=0;
Group.c2.rawmean=mean(tecan_data(28:30,:));
Group.c3.rows=C(7:9);
Group.c3.conc=0;
Group.c3.rawmean=mean(tecan_data(31:33,:));
Group.c4.rows=C(10:12);
Group.c4.conc=0;
Group.c4.rawmean=mean(tecan_data(34:36,:));

Group.d1.rows=D(1:3);
Group.d1.conc=0;
Group.d1.rawmean=mean(tecan_data(37:39,:));
Group.d2.rows=D(4:6);
Group.d2.conc=0;
Group.d2.rawmean=mean(tecan_data(40:42,:));
Group.d3.rows=D(7:9);
Group.d3.conc=0;
Group.d3.rawmean=mean(tecan_data(43:45,:));
Group.d4.rows=D(10:12);
Group.d4.conc=0;
Group.d4.rawmean=mean(tecan_data(46:48,:));

Group.e1.rows=D(1:3);
Group.e1.conc=0;
Group.e1.rawmean=mean(tecan_data(49:51,:));
Group.e2.rows=D(4:6);
Group.e2.conc=0;
Group.e2.rawmean=mean(tecan_data(52:54,:));
Group.e3.rows=D(7:9);
Group.e3.conc=0; 
Group.e3.rawmean=mean(tecan_data(55:57,:));
Group.e4.rows=D(10:12);
Group.e4.conc=0;
Group.e4.rawmean=mean(tecan_data(58:60,:));

Group.f1.rows=D(1:3);
Group.f1.conc=0.49;
Group.f1.rawmean=mean(tecan_data(61:63,:));
Group.f2.rows=D(4:6);
Group.f2.conc=0.25;
Group.f2.rawmean=mean(tecan_data(64:66,:));
Group.f3.rows=D(7:9);
Group.f3.conc=0;
Group.f3.rawmean=mean(tecan_data(67:69,:));
Group.f4.rows=D(10:12);
Group.f4.conc=0;
Group.f4.rawmean=mean(tecan_data(70:72,:));

Group.g1.rows=D(1:3);
Group.g1.conc=0.34;
Group.g1.rawmean=mean(tecan_data(73:75,:));
Group.g2.rows=D(4:6);
Group.g2.conc=0;
Group.g2.rawmean=mean(tecan_data(76:78,:));
Group.g3.rows=D(7:9);
Group.g3.conc=0; 
Group.g3.rawmean=mean(tecan_data(79:81,:));
Group.g4.rows=D(10:12);
Group.g4.conc=0;
Group.g4.rawmean=mean(tecan_data(82:84,:));

% Group.h1.rows=D(1:6);
% Group.h1.conc=1.2;
% Group.h1.rawmean=mean(tecan_data(85:87,:));
% Group.h2.rows=D(7:12);
% Group.h2.conc=0.93;
% Group.h2.rawmean=mean(tecan_data(88:90,:));
% Group.h3.rows=D(7:9);
% Group.h3.conc=0; 
% Group.h3.rawmean=mean(tecan_data(91:93,:));
% Group.h4.rows=D(10:12);
% Group.h4.conc=0;
% Group.h4.rawmean=mean(tecan_data(94:96,:));


%'a1','a2','a3','a4','h1','h2','h3','h4' 
groups={'b1','b2','b3','b4','c1','c2','c3','c4','d1','d2','d3','d4','e1','e2','e3','e4','f1','f2','f3','f4','g1','g2','g3','g4'};
group_number=length(groups);


%mirror the data and calculate protein augmentation
for g=1:r 
    j=1;
    for j=1:group_number
        if sum(ismember(Group.(groups{j}).rows,g))>0
        end_od=mean(tecan_data(g,c-10:c));    %calculate mean OD of last 10 values
        
        prot_od(2,g)=Group.(groups{j}).conc;
        prot_od(1,g)=end_od/Group.(groups{j}).conc;
        %tecan_data_con(g,:)=tecan_data(g,:)./ prot_od(1,g);
        Group.(groups{j}).data(1,1:120)=1;
        [z,f]=size(Group.(groups{j}).data);
        Group.(groups{j}).data(z+1,:)=prot_od(1,g)./ tecan_data(g,:);
        end 
    end        
end
    
%calculate average of groups
for j=1:group_number
Group.(groups{j}).data_mean=mean(Group.(groups{j}).data(2:end,:)) ;
end


%% Solubilisation kinetic plots
kinetic = []
kinetic_parameter=[];


for j=1:group_number
    
%      fitType = 'exp1';
%      myFit = fit(x(1:81),y(1:81),fitType);
     x= [];
     y=[];
     
     x=time_min';                    %x values for calculation of linear regression
     %y=tecan_data(i,:)';             %y for calculation of linear regression
     y=Group.(groups{j}).rawmean;
%       smooth=savGol(y,x,20,20,1);
     y1 =[];
     y1=y';
     xlog=log(x);                    %log the independent variable (time)
%      
%      plot(1./xlog(2:end),y(2:end),'o')
     xx= [xlog(2:end),ones(size(xlog,1)-1,1)];
     size(xx)
     size(y1(2:end))
      
     a=regress(y1(2:end),xx);

       
     yfit=a(1).*xx(:,1)+a(2);        %calculate values based on fit
     Group.(groups{j}).fit=polyfit(x(1:81),yfit(1:81),1);
     Group.(groups{j}).parameter(:,1)=Group.(groups{j}).fit(1,1);
        
     kinetic{j}(:,1) = Group.(groups{j}).parameter(:,1);
        
     kinetic_parameter=kinetic';
     
     yresid=y(1:119)'-yfit;        %R2 calculation plot rsq to get hold of your data quality
     SSresid = sum (yresid.^2);
     SStotal = (length(y)-1) *var(y);
     Group.(groups{j}).rsq= 1-SSresid/SStotal;
      



%      %plot original data and fitted data
    subplot(4,5,j);  
    hold on

        set(figure(j),'name', groups{j});
        title(['acceptance criteria R^2 =', num2str(Group.(groups{j}).rsq)])
        plot(x(1:81),y(1:81), 'color','b','Marker','.')
        plot(x(1:81),yfit(2:82), 'color','r')
        legend(groups{j},'fit','fontsize',15,'FontName','Arial','FontAngle','italic','Location','Northwest');

        
     ylabel('protein [g/l]','fontsize',12,'FontName','Arial');
     xlabel('sol time [min]','fontsize',12,'FontName','Arial');
 hold off
end

 xlswrite ('WR22_kinetic_parameter', kinetic_parameter);


