%%
clear all
% folder='S:\CDLMIB\CD_Data\Module_1\03_RT3\03_Results\IB Analytics\Purity\Messung 20150605\Chip 1';
folder='S:\CDLMIB\CD_Data\Module_1\03_RT3\03_Results\IB Analytics\Purity\Messung 20150605\Chip 2';
% folder='S:\CDLMIB\CD_Data\Module_1\03_RT3\03_Results\IB Analytics\Purity\Messung und Auswertung 20140825\Chip 1';
% folder='S:\CDLMIB\CD_Data\Module_1\03_RT3\03_Results\IB Analytics\Purity\Messung und Auswertung 20150210\Chip 1';%specify folder
% folder='S:\CDLMIB\CD_Data\Module_1\03_RT3\03_Results\IB Analytics\Purity\Messung und Auswertung 20150210\Chip 2'; %specify folder
% folder='S:\CDLMIB\CD_Data\Module_1\03_RT3\03_Results\IB Analytics\Purity\Messung und Auswertung 20150210\Chip 3'; %specify folder
% folder='S:\CDLMIB\CD_Data\Module_1\03_RT3\03_Results\IB Analytics\Purity\Messung und Auswertung 20150330\Chip 1';%specify folder
% folder='S:\CDLMIB\CD_Data\Module_1\03_RT3\03_Results\IB Analytics\Purity\Messung und Auswertung 20150330\Chip 2'; %specify folder
% folder='S:\CDLMIB\CD_Data\Module_1\03_RT3\03_Results\IB Analytics\Purity\Messung und Auswertung 20150330\Chip 3'; %specify folder

cd(folder)
addpath('S:\BPT\BPT SHARED DATA\MSC\Markus Brillmann\scripts workspace');        %specify path of matlab files
Chip_name='WR_30_32_33_4_5_230_timeint';


% Lanes={'Standard';	'WR21-4-A';	'WR21-6-A';	'WR21-6-B';	'WR21-7-A';	'WR30-6-A';	'WR30-7-A';	'WR30-8-B';	'WR30-5-C';	'Standard'};
Lanes={'Standard';	'WR30-6-C';	'WR30-7-D';	'WR30-7-D';	'WR32-8-B';	'WR33-4-A';	'WR33-8-A';	'WR35-11-B'; 'WR36-7-D'; 'Buffer'};
% Lanes={'Standard'; 'WR31-7-C'; 'WR31-8-C'; 'WR31-8-D'; 'WR31-9-D'; 'WR35-11-A'; 'WR35-11-B'; 'WR35-9-C'; 'WR35-9-D'; 'Standard'};
% Lanes={'Standard'; 'WR31-7-C'; 'WR31-8-C'; 'WR31-8-D'; 'WR31-9-D'; 'WR35-11-A'; 'WR35-11-B'; 'WR35-9-C'; 'WR35-9-D'; 'Standard'};
% Lanes={'Standard'; 'WR18-3-B'; 'WR22-5-A'; 'WR22-6-B'; 'WR22-8-A'; 'WR22-7-B'; 'WR22-7-D'; 'WR32-5-D'; 'WR32-10-A'; 'Standard'};
% Lanes={'Standard'; 'WR18-4-B'; 'WR18-6-B'; 'WR18-5-D'; 'WR18-5-A'; 'WR21-6-A'; 'WR21-7-A'; 'WR21-6-B'; 'WR21-4-D'; 'Standard'};
% Lanes={'Standard'; 'WR33-8-A'; 'WR22-6-D'; 'WR32-9-C'; 'WR32-6-B'; 'WR32-8-B'; 'WR32-8-C'; 'WR33-4-A'; 'WR32-7-D'; 'Standard'};
% Lanes={'Standard'; 'WR35-7-D'; 'WR36-8-A'; 'WR36-8-B'; 'WR36-8-C'; 'WR36-8-D'; 'WR36-7-D'; 'WR37-7-A'; 'WR37-7-B'; 'Standard'};
% Lanes={'Standard'; 'WR37-7-C'; 'WR37-7-D'; 'WR37-6-C'; 'WR38-8-A'; 'WR38-8-B'; 'WR38-8-C'; 'WR38-8-D'; 'WR38-7-A'; 'Standard'};

%% import data and standard purity calculation


Product_start=29; %starting integration kDa
Product_end=40; %ending integration kDa
standard_start=13; %starting point for blank integration - AFTER Standard Peak which is approx 16 kDA
standard_end=18;  %enpoint point for blank integration
cutoff_low=10;
cutoff_high=230;

[all_data,number_of_samples]=import_bioanalyzer_data(folder); %call import function

Standards=[];
Standards=[4.5,6.0,7.0,15,28,46,63,95,150,240]';% kDa of ladder
x0=peakfinder(all_data.Ladder.Data); % call peakfinder function
Standards=[all_data.Ladder.Time(x0),Standards]; % peak locations (time) + kDa of ladder
b=regress(Standards(:,2), [ones(size(Standards(:,1),1),1), Standards(:,1), Standards(:,1).^2]); %regress peak locations (time) + kDa of ladder

for i=1:number_of_samples
    Fex=[];
    Fex=Lanes{i};
    if strcmp(Fex,'Standard')
        all_data.(['Sample',num2str(i)]).Size=all_data.(['Sample',num2str(i)]).Time.^2*b(3)+all_data.(['Sample',num2str(i)]).Time*b(2)+b(1); %calculate kDa of ladder from peak locations (time)
        b1=find(all_data.(['Sample',num2str(i)]).Size>standard_start,1,'first'); %start index of product peak
        b2=find(all_data.(['Sample',num2str(i)]).Size<standard_end,1,'last'); %end index of product peak
        b3=find(all_data.(['Sample',num2str(i)]).Size>cutoff_low,1,'first');
        b4=find(all_data.(['Sample',num2str(i)]).Size<cutoff_high,1,'last');
        integral_standard=trapz(all_data.(['Sample',num2str(i)]).Time(b1:b2),all_data.(['Sample',num2str(i)]).Data(b1:b2)); %integration of product peak
        integral_all_protein=trapz(all_data.(['Sample',num2str(i)]).Time(b3:b4),all_data.(['Sample',num2str(i)]).Data(b3:b4)); %integration of whole chromatogram
        integral_blank=integral_all_protein-integral_standard;
        
                
        
        all_data.(['Sample',num2str(i)]).Purity=integral_standard/(integral_all_protein)*100; %calulcation of purity
        all_data.(['Sample',num2str(i)]).area=integral_standard; %save area of product peak
        all_data.(['Sample',num2str(i)]).Name=Lanes{i}; %add name
        figure(2)
   
   subplot(2,6,i)
   hold all
   plot(all_data.(['Sample',num2str(i)]).Time(b1:b2,1),all_data.(['Sample',num2str(i)]).Data(b1:b2,1))
   set(gca, 'ylim', [0 2000])
   set(gca, 'xlim', [13.5 50])   
	
    else
    end
end


%%
for i=1:number_of_samples
    Fex=[];
    Fex=Lanes{i};
    if strcmp(Fex,'Standard')
        
    else
    all_data.(['Sample',num2str(i)]).Size=all_data.(['Sample',num2str(i)]).Time.^2*b(3)+all_data.(['Sample',num2str(i)]).Time*b(2)+b(1); %calculate kDa of ladder from peak locations (time)
    ind1=find(all_data.(['Sample',num2str(i)]).Size>Product_start,1,'first'); %start index of product peak
    ind2=find(all_data.(['Sample',num2str(i)]).Size<Product_end,1,'last'); %end index of product peak
    ind3=find(all_data.(['Sample',num2str(i)]).Size>cutoff_low,1,'first');
    ind4=find(all_data.(['Sample',num2str(i)]).Size<cutoff_high,1,'last');
    integral_product=trapz(all_data.(['Sample',num2str(i)]).Time(ind1:ind2),all_data.(['Sample',num2str(i)]).Data(ind1:ind2)); %integration of product peak
    integral_all_protein=trapz(all_data.(['Sample',num2str(i)]).Time(ind3:ind4),all_data.(['Sample',num2str(i)]).Data(ind3:ind4)); %integration of whole chromatogram
    
    all_data.(['Sample',num2str(i)]).Purity=integral_product/(integral_all_protein)*100; %calulcation of purity
    all_data.(['Sample',num2str(i)]).area=integral_product; %save area of product peak
    all_data.(['Sample',num2str(i)]).Name=Lanes{i}; %add name
    subplot(2,5,i)
   hold all
   C(i)=plot(all_data.(['Sample',num2str(i)]).Time(ind1:ind2,1),all_data.(['Sample',num2str(i)]).Data(ind1:ind2,1),'color','r');
   C(i)=plot(all_data.(['Sample',num2str(i)]).Time(1:ind1,1),all_data.(['Sample',num2str(i)]).Data(1:ind1,1));
   C(i)=plot(all_data.(['Sample',num2str(i)]).Time(ind2:end,1),all_data.(['Sample',num2str(i)]).Data(ind2:end,1),'color','b');
   set(gca, 'ylim', [0 2000])
   set(gca, 'xlim', [13.2 50])
    end
    
end

%% Plotting function



figure(2)
for i=2:number_of_samples-1
    hold all
   plot(all_data.(['Sample',num2str(i)]).Size,all_data.(['Sample',num2str(i)]).Data)
   set(gca, 'ylim', [0 2000])
   set(gca, 'xlim', [0 434])   
end

% figure(3)
% for i=2:number_of_samples-1
%     hold all
%    plot(all_data.(['Sample',num2str(i)]).Time,all_data.(['Sample',num2str(i)]).Data)
%    set(gca, 'ylim', [0 500])
%    set(gca, 'xlim', [13.2 50])   
% end

h=figure;
Purity=[];
for i=1:number_of_samples
  
 Purity=[Purity,all_data.(['Sample',num2str(i)]).Purity];
 
end


bar(Purity)
ylabel('Purity [%]', 'FontSize', 12)
set(gca,'XTickLabel',Lanes);
rotateXLabels( gca(), 45 )
hold off

%%

for i=1:number_of_samples
 Chip{i,1}=all_data.(['Sample',num2str(i)]).Name; %calulcation of purity
 Chip{i,2}=all_data.(['Sample',num2str(i)]).Purity; %save purity
 Chip{i,3}=all_data.(['Sample',num2str(i)]).area;   %save area of product
end


%% Save Data
any2csv(Chip,';',0,[Chip_name '.csv']) %save data
saveas(h,[Chip_name '.fig']) %save figure

