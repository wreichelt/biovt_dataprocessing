%% Data import and evaluation of time measurements of Tecan Multiplatereader
%Wieland Reichelt


%Purpose: import TEcan raw data.
%           calculated concentration and dilutions
%           quality control by internal standards
%           store values in an object


clc, clear all, close all

%Set Path
path='S:\CDLMIB\CD_Data\Module_1\03_RT3\03_Results\Lysis analytics';    %specify path of experimental data
addpath('S:\BPT\wreichelt\Matlab\biovt_dataprocessing');        %specify path of matlab files
cd(path);


%% Load data to Matlab
tic

Ex='WR37';
Reactor=' C';
filename='WR35_C_Rawdata';             %specify filname of DasGip Export file
sheet_data='Sheet T1';                                       %specify sheet with process data of timepoint 1
[data_import,names,raw]=xlsread(filename,sheet_data);      %matrix where online data is stored


toc

%% assign layout


%Start of data
first=find(strcmp('<>' ,raw(:,1)))+1;

%End of Data 
last=find(strcmp('End Time:' ,raw(:,1)));
last=last-4;

%write data into matrice
tecan_data=cell2mat(raw(first:last,2:end)); %data only without time achsis

[r1,c1]=size(tecan_data);



%% INput


%calibration
standards=struct;
standards.conc=[0,0.5,5,10,20,50,100,200]';     % standard concentrations [ng/ml]
standards.pos=[1; 2];                             %columns of standards

s=3;                             %first column of sample data

%samples
samples=struct;
rows={'10^3' '10^4' 'S10^3' 'S10^4' '10^3' '10^4' 'S10^3' 'S10^4'};    %Specifiy the names of the timely measurements
samples.names={[ Ex Reactor '1'] [ Ex Reactor '2'] [ Ex Reactor '3'] [ Ex Reactor '4'] [ Ex Reactor '5'] [ Ex Reactor '6'] [ Ex Reactor '7'] [ Ex Reactor '8']};

%dilution
samples.dilution.factor_1=10^3;         %dilution 1
samples.dilution.rows_1=[1; 3; 5; 7];    %row of diluted 1 samples on the 96 well plate
samples.dilution.factor_2=10^4;         % dilution 2
samples.dilution.rows_2=[2; 4; 6; 8];   %row of diluted 2 samples on the 96 well plate

%spike
samples.spike=10;                   %spike concentrations ng/ml
samples.spike_unit='ng/ml';
samples.spiked.rows=[3 4 7 8]';     %row of spiked samples on the 96 well plate
samples.unspiked.rows=[1 2 5 6]';   %row of UNspiked samples on the 96 well plate


%% calculations

%standards
standards.RFU=[];
standards.calib=[];

for k=1:length(standards.pos)
standards.RFU=[standards.RFU;tecan_data(1:8,standards.pos(k))];
standards.calib=[standards.calib;standards.conc];
end

[calib,error]=polyfit(standards.calib,standards.RFU,1);
standards.slope=calib(1);
standards.offset=calib(2);



%% calculations


% DNA concentration
samples.RFU=[];
samples.DNA=[];
samples.RFU=tecan_data(1:8,s:end);
samples.DNA=(samples.RFU-standards.offset)./(standards.slope);


% Dilution ng/ml -> mg/ml
samples.DNA_undiluted(samples.dilution.rows_1,:)=samples.DNA(samples.dilution.rows_1,:).*samples.dilution.factor_1./10^3;
samples.DNA_undiluted(samples.dilution.rows_2,:)=samples.DNA(samples.dilution.rows_2,:)*samples.dilution.factor_2/10^3;


%% quality control

%spike
samples.spiked.pv=samples.DNA(samples.spiked.rows,:)-samples.DNA(samples.unspiked.rows,:);

figure(1)
labels={'10^3' '10^4' '2_10^3' '2_10^4'};
bar(samples.spiked.pv)
ylabel('spike conc [ng/ml]', 'FontSize', 12)
set(gca,'XTickLabel',labels);



%dilution
figure(2)
labels=rows;
bar(samples.DNA_undiluted)
ylabel('spike conc [mg/ml]', 'FontSize', 12)
set(gca,'XTickLabel',labels);

%% data pooling

%specify lanes for pooling

pz=samples.unspiked.rows;
%pz=[1;2;5];
samples.DNA_mean=[];

for k=1:length(samples.RFU)
        
    samples.DNA_mean(k)=mean(samples.DNA_undiluted(pz,k));
    samples.DNA_stabw(k)=std(samples.DNA_undiluted(pz,k));
    
end



plot(samples.DNA_mean)
ylabel('DNA conc [mg/ml]', 'FontSize', 12)
set(gca,'XTickLabel',samples.names);
rotateXLabels( gca(), 45 )


%% Save

measurement=struct;
measurement.samples=samples;
measurement.standards=standards;

save (['DNA_measurement_' Ex], 'measurement'  )

