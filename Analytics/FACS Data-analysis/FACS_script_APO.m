%% initialize
clear all
% close all
% set working directory
% cd 'S:\CDLMIB\CD-L\04_RT4\02_Material_Methods\Flow-Cytometry\IG_1to200_Calibration_130527'
cd 'S:\CDLMIB\CD_Data\Module_1\03_RT3\03_Results\Experiments\WR39\FACS Analysis WR39'
% addpath
addpath 'S:\CDLMIB\CD_Data\Module_1\03_RT3\02_Material_Methods\Matlab-Scripts\FACS Data-analysis'

%% load data
% filename = 'B4_200.fcs';
filename = 'WR39_2A_10000.fcs';
[fcsdat, fcshdr, fcsdatscaled, fcsdat_comp] = fca_readfcs_APO2(filename);
% convert to log10 scale
% fcsdatscaled_woZeros = fcsdatscaled(all(fcsdatscaled,2),:);
% fcsdatlog10 = log10(fcsdatscaled_woZeros);
fcsdatlog10 = log10(fcsdatscaled);
fcsdatlog10(fcsdatlog10 < 0) = 0;

%% plot data
fig1 = plotFacsData3(fcsdatlog10,100,90,[3 4.5],[2.5 4.5], [0 5], [0 5], [0 5]);

%% integrate
sporeCounts = roiCounts(fcsdatlog10,[3 4.5],[2.5 4.5],[0 5],[0 5],[0 5])
FL3Counts = roiCounts(fcsdatlog10,[3.1 4.5],[2.5 4.5],[1 5],[1 5],[1 5])

%% compensate cross-talk from green to red fluorescence channel
fcsdatlog10_comp = fcsdatlog10;
fcsdatlog10_comp(:,4) = fcsdatlog10_comp(:,4)-fcsdatlog10_comp(:,3);
fig2 = plotFacsData(fcsdatlog10_comp,100,500,[3 4.5],[0 5], [0 5], [0 5], [0 5]);
%% plot compensated data



