function [intResults] = roiCounts(data,roiFWS,roiSWS,roiFL1,roiFL2,roiFL3)

% define input for scatterplots based on rois
roiData = data(roiFWS(1)<data(:,1) & data(:,1) <roiFWS(2) ,:);
roiData = roiData(roiSWS(1)< roiData(:,2) & roiData(:,2) < roiSWS(2) ,:);
roiData = roiData(roiFL1(1)< roiData(:,3) & roiData(:,2) < roiFL1(2) ,:);
roiData = roiData(roiFL2(1)< roiData(:,4) & roiData(:,2) < roiFL2(2) ,:);
roiData = roiData(roiFL3(1)< roiData(:,5) & roiData(:,2) < roiFL3(2) ,:);

intResults = size(roiData,1);
end