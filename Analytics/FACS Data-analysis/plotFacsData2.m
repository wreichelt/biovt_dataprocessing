function [fig] = plotFacsData2(fcsdatlog10,nbars,nbars2,roiFWS,roiSWS,roiFL1,roiFL2,roiFL3)
fig = figure;
%plot FWS
subplot(1,3,1); hist(fcsdatlog10(:,1),nbars)
axis([0 5 0 10000])
yL = get(gca,'YLim');
line([roiFWS(1) roiFWS(1)],yL,'Color','k');
line([roiFWS(2) roiFWS(2)],yL,'Color','k');
set(gca,'FontName','Arial');
set(gca,'FontSize',16);
title ('FWS')
% colormap('cool')

%plot SWS
subplot(1,3,2); hist(fcsdatlog10(:,2),nbars)
axis([0 5 0 10000])
yL = get(gca,'YLim');
line([roiSWS(1) roiSWS(1)],yL,'Color','k');
line([roiSWS(2) roiSWS(2)],yL,'Color','k');
set(gca,'FontName','Arial');
set(gca,'FontSize',16);
title ('SWS')

% %plot FL1
% subplot(3,3,3); hist(fcsdatlog10(:,3),nbars)
% % axis([0 5 0 10000])
% yL = get(gca,'YLim');
% line([roiFL1(1) roiFL1(1)],yL,'Color','k');
% line([roiFL1(2) roiFL1(2)],yL,'Color','k');
% set(gca,'FontName','Arial');
% set(gca,'FontSize',16);
% title ('FL1')
% 
% %plot FL2
% subplot(3,3,4); hist(fcsdatlog10(:,4),nbars)
% % axis([0 5 0 10000])
% yL = get(gca,'YLim');
% line([roiFL2(1) roiFL2(1)],yL,'Color','k');
% line([roiFL2(2) roiFL2(2)],yL,'Color','k');
% set(gca,'FontName','Arial');
% set(gca,'FontSize',16);
% title ('FL2')
% 
% %plot FL3
% subplot(3,3,5); hist(fcsdatlog10(:,5),nbars)
% % axis([0 5 0 10000])
% yL = get(gca,'YLim');
% line([roiFL3(1) roiFL3(1)],yL,'Color','k');
% line([roiFL3(2) roiFL3(2)],yL,'Color','k');
% set(gca,'FontName','Arial');
% set(gca,'FontSize',16);
% title ('FL3')

% define input for scatterplots based on rois
fcsdatscatter = fcsdatlog10(roiFWS(1)<fcsdatlog10(:,1) & fcsdatlog10(:,1) <roiFWS(2) ,:);
fcsdatscatter = fcsdatscatter(roiSWS(1)< fcsdatscatter(:,2) & fcsdatscatter(:,2) < roiSWS(2) ,:);
fcsdatscatter = fcsdatscatter(roiFL1(1)< fcsdatscatter(:,3) & fcsdatscatter(:,2) < roiFL1(2) ,:);
fcsdatscatter = fcsdatscatter(roiFL2(1)< fcsdatscatter(:,4) & fcsdatscatter(:,2) < roiFL2(2) ,:);
fcsdatscatter = fcsdatscatter(roiFL3(1)< fcsdatscatter(:,5) & fcsdatscatter(:,2) < roiFL3(2) ,:);
% 
% %plot FL1 vs. FL3
% [z3,x3,y3] = D2hist(fcsdatscatter(:,3),fcsdatscatter(:,5),nbars2);
% subplot(3,3,6); surfc(x3,y3,z3)
% axis([0 5 0 5])
% set(gca,'FontSize',16);
% title ('FL1 vs. FL3')
% 
% %plot FWS vs. SWS
% [z1,x1,y1] = D2hist(fcsdatscatter(:,1),fcsdatscatter(:,2),nbars2);
% subplot(3,3,7); surfc(x1,y1,z1)
% colormap(hsv);
% axis([0 5 0 5])
% set(gca,'FontSize',16);
% title ('FWS vs. SWS')
%     

%plot FL1 vs. FL2
[z2,x2,y2] = D2hist(fcsdatscatter(:,1),fcsdatscatter(:,4),nbars2);
subplot(1,3,3); h =  surf(x2,y2,z2,'FaceColor','interp','EdgeColor','none', 'FaceLighting','phong')
axis([0 5 0 5])
set(gca,'YTick',[0 5])
set(gca,'XTick',[0 5])
set(gca,'FontSize',16);
title ('FWS vs. FL2')


% %plot FL1 vs. FL2
% [z3,x3,y3] = D2hist(fcsdatscatter(:,3),fcsdatscatter(:,4),nbars2);
% subplot(3,3,9); surfc(x3,y3,z3)
% axis([0 5 0 5])
% set(gca,'FontSize',16);
% title ('FL1 vs. FL2')

end
