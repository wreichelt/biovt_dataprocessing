function [z,xi,yi] = D2hist(yd,xd,n)
xi = linspace(0,5,n);
yi = linspace(0,5,n);
xr = interp1(xi,1:numel(xi),xd,'nearest')';
yr = interp1(yi,1:numel(yi),yd,'nearest')';

z = accumarray([xr' yr'], 1, [n n]);

end