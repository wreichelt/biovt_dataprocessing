%% Data import and evaluation of time measurements of Tecan Multiplatereader
%Wieland Reichelt

clc, clear all, close all

%Set Path


path=['S:\CDLMIB\CD_Data\Module_1\03_RT3\03_Results\Inclusion Body Analytics Development\Solubility'];    %specify path of experimental data
addpath('S:\BPT\wreichelt\Matlab\biovt_dataprocessing');        %specify path of matlab files
cd(path);


    %% Load data to Matlab
tic

filename='20141023_solubility test_WR27.xlsx';             %specify filname of DasGip Export file
sheet_data='Sheet1';                                       %specify sheet with process data 
[data_import,names,raw]=xlsread(filename,sheet_data);      %matrix where online data is stored

toc


%% assign layout timely measurement

time_sec=[];
time_min=[];

%time
first=find(strcmp('Cycle Nr.' ,raw(:,1)));
time_sec=cell2mat(raw(first+1,2:end)); %sec
time_min=time_sec./60; %min

%Data A1=1
last=find(strcmp('End Time:' ,raw(:,1)));
last=last-4;
tecan_data=cell2mat(raw(first+3:last,2:end)); %data only without time achsis

[r,c]=size(tecan_data);





%% define groups and Samples for calculation

i=1;
A=(i:1:i+12);
B=(1+12:1:i+24);
C=(i+24:1:i+36);
D=(i+36:1:i+48);
E=(i+48:1:i+60);
F=(i+60:1:i+72);
G=(i+72:1:i+84);
H=(i+84:1:i+96);

%%
Group=struct;
Group.a1.rows=A(1:6);
Group.a1.conc=3;      %g/l protein concentration
Group.a2.rows=A(7:12);
Group.a2.conc=0.1; 

Group.b1.rows=B(1:6);
Group.b1.conc=3; 
Group.b2.rows=B(7:12);
Group.b2.conc=3;

Group.c1.rows=C(1:6);
Group.c1.conc=3; 
Group.c2.rows=C(7:12);
Group.c2.conc=5;

Group.d1.rows=D(1:6);
Group.d1.conc=3; 
Group.d2.rows=D(7:12);
Group.d2.conc=10;



groups={'a1','a2','b1','b2','c1','c2','d1','d2'};
group_number=length(groups);



for g=1:r 
    j=1;
    for j=1:group_number
        if sum(ismember(Group.(groups{j}).rows,g))>0
        end_od=mean(tecan_data(g,c-10:c));    %calculate mean OD of last 10 values
        
        prot_od(2,g)=Group.(groups{j}).conc;
        prot_od(1,g)=end_od/Group.(groups{j}).conc;
        %tecan_data_con(g,:)=tecan_data(g,:)./ prot_od(1,g);
        Group.(groups{j}).data(1,1:120)=1;
        [z,f]=size(Group.(groups{j}).data);
        Group.(groups{j}).data(z+1,:)=tecan_data(g,:)./ prot_od(1,g);
        end 
    end        
end
    
%%calculate average of groups
for j=1:group_number
Group.(groups{j}).data_mean=mean(Group.(groups{j}).data(2:end,:)) ;
end


%% Solubilisation kinetics


for j=1:group_number
    
     
     x= [];
     y=[];
     x=time_min';                    %x values for calculation of linear regression
     %y=tecan_data(i,:)';             %y for calculation of linear regression
     y=Group.(groups{j}).data_mean';
     %smooth=savGol(y,x,10,10,1);
     xlog=log(x);                    %log the independent variable (time)
     
     %plot(1./xlog(2:end),y(2:end),'o')
     xx= [xlog(2:end),ones(size(xlog,1)-1,1)];
     size(xx)
     size(y(2:end))
     a=regress(y(2:end),xx);
          
     yfit=a(1).*xx(:,1)+a(2);        %calculate values based on fit
     
     %plot original data and fitted data
   figure(j);  
   hold on

     set(figure(j),'name', groups{j});
     plot(x,y, 'color','b','Marker','.')
     plot(x(2:end),yfit, 'color','r')
     ylabel('protein [g/l]','fontsize',12,'FontName','Arial');
     xlabel('sol time [min]','fontsize',12,'FontName','Arial');
 hold off
    end
%%
     
     %calculate R^2 doc Linear regression
     yresid=y-yfit;     %Compute the residual values as a vector signed numbers:
     SSresid = sum(yresid.^2);  %Square the residuals and total them obtain the residual sum of squares:
     SStotal = (length(y)-1) * var(y);  %Compute the total sum of squares of y by multiplying the variance of y by the number of observations minus 1
     rsq = 1 - SSresid/SStotal; %Compute R2 using the formula given in the introduction of this topic
    
     
     C1=plot(x,y,'color','r','Marker','.');
     C2=plot(xfit,yfit,'color','g', 'LineWidth', 3);
     C3=plot([NaN],[NaN], 'LineStyle', 'none'); %to write the lin. equation into the legend
     C4=plot([NaN],[NaN], 'LineStyle', 'none'); %to write R2 into the legend
     title(names(i,1))
    
     %legend
     leg1 =['y = (' num2str(p(1,1)) ')* x + ' num2str(p(1,2))]; % formula of lin. regression
     leg2 =['R^2 = ' num2str(rsq)]; %generate string from varable R2
     legend([C1 C2 C3 C4],{'Messwerte' 'Regression' (leg1) (leg2)},'Location','North'); % insert legend
        
      %check linear regression
     bound(3,i)=0;
     text_check=['Are you satisfied with regression? press 1 for yes and 0 for no: '];
     bound(3,i)=input(text_check); %if this value is 0 while loop is repeated, if not, plot gets printed
     
    end
    
    bound(4,i)=p(1,1);  %a1 into row4
    bound(5,i)=p(1,2);	 %a0 into row5
    bound(6,i)= rsq;   %write R^2 into row 6
     

v2=['Fig' names{i,1}];    %filename of printed plot
print(v2,'-dpng', '-r400')  %print plot
comment= ['Fig' names{i,1} ' got exported!' ];
disp(comment);
close all

stop=input('to stop data analysis press 1, to continue enter: ');
    if stop>0
        break
        
    end
     %%
end
%% creating an export Matrix with Data from Matrix "bound" + header
Beschriftung=[cellstr('well n�');cellstr('name');cellstr('checked');cellstr('k');cellstr('d');cellstr('R^2');cellstr('start');cellstr('end')]; %create labeling
% description of export matrix
[opa, oma]=size(bound);    
%%
data_export(1,2:oma)=names(2:oma,1)'; %header
data_export(1:opa,1)=Beschriftung(:,1); % description in first row
data_export(3:opa,2:oma)=num2cell(bound(3:opa,2:oma)); %export important data from bound matrix: x axis without first column (time); y axis without first two rows
    
%% To export the data stored into the export_data file exportcheck has to be 1 otherwise no export
exportcheck=0;  %exportcheck is set to 0 per default
text_exportcheck='if you want to export data to Excelfile press 1, otherwise 0: '; %text which gets displayed
exportcheck=input(text_exportcheck); %you get asked whether you want to export or not
%%
if exportcheck>0 %if you changed exportcheck to 1 data will get exported
    %%
    askname='put in name for excelfile: ';
    xlsfile1=input(askname, 's');
    xlsfile2=[xlsfile1 '.xlsx'];
    
%%

xlswrite(xlsfile2,data_export,sheet_data);

disp('data was exported to xlsfile')

else
    
end


% data_export=zeros(size(bound)+[2 1]);
% data_export(1,:)=names(:,1)';
% data_export(2,:)= int2str(2);
% %data_export(3:9,:)=int2str(bound);
% data_export(3:9,:)=(bound);
%clear data_export
%%
comment= ['Analysis is finished'];
disp(comment);

