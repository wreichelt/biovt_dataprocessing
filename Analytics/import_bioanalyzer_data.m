function [all_data,number_of_samples]=import_bioanalyzer_data( v )


% Import
files=dir('*.csv');
names={files.name};



for i=1:size(names,2)
    if strfind(names{i},'Ladder')
        Ladder_index=i;
    end
end



if Ladder_index > 0
    
dataArray=readcsvdata(names{Ladder_index});
all_data.('Ladder').Time = cellfun(@str2num,dataArray{1,1}(1:end-1,1));
all_data.('Ladder').Data = cellfun(@str2num,dataArray{1,2}(1:end-1,1));

number_of_samples=size(names,2)-1;
disp('Ladder imported succesfully')
disp([num2str(number_of_samples), ' Samples found'])


for i =1 : number_of_samples
    
    Sample_index=[];
    
    for j=1:number_of_samples+1
        if Sample_index > 0
        elseif strfind(names{j},['Sample',num2str(i)]) > 0
            Sample_index=j;
        end
    end
dataArray=readcsvdata(names{Sample_index});

all_data.(['Sample',num2str(i)]).Time = cellfun(@str2num,dataArray{1,1}(1:end-1,1));
all_data.(['Sample',num2str(i)]).Data = cellfun(@str2num,dataArray{1,2}(1:end-1,1));

disp(['Sample', num2str(i), ' imported successfully'])
end

else
    disp('No Ladder Found')
    disp('Connot process data')
end

