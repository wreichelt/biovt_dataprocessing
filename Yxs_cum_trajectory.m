%% INput
close all
clear all

%%
close all

name='B';
% group=[1 2 3 5 6 7]; % group A number 4 looks weird
group=[1 2 3 4 ]; % group B
load('calc_Ex_group_B');
% 
run('variable_attribution.m')       %for variable attribution
yxs_fit=[];

for j=1:length(group)
k=group(j);
figure(1)
plot(calc{k}(:,1),calc{k}(:,BM_g));
hold on
xlabel('time','fontsize',12,'FontName','Arial','FontAngle','italic')
ylabel('BM_g','fontsize',12,'FontName','Arial','FontAngle','italic')

figure(2)
plot(calc{k}(:,1),calc{k}(:,cumS_xinit));
xlabel('time','fontsize',12,'FontName','Arial','FontAngle','italic')
ylabel('cumS_xinit','fontsize',12,'FontName','Arial','FontAngle','italic')

hold on

figure(3)
plot(calc{k}(:,1),calc{k}(:,Yxs_cum));
xlabel('time','fontsize',12,'FontName','Arial','FontAngle','italic')
ylabel('Yxs_cum','fontsize',12,'FontName','Arial','FontAngle','italic')

hold on

figure(4)
plot(calc{k}(:,cumS_xinit),calc{k}(:,Yxs_cum));
xlabel('cumS_xinit','fontsize',12,'FontName','Arial','FontAngle','italic')
ylabel('Yxs_cum','fontsize',12,'FontName','Arial','FontAngle','italic')
hold on

figure(5)
plot(calc{k}(:,cumS_xinit),calc{k}(:,BM_g));
xlabel('cumS_xinit','fontsize',12,'FontName','Arial','FontAngle','italic')
ylabel('BM [g]','fontsize',12,'FontName','Arial','FontAngle','italic')

hold on

temp=[calc{k}(:,cumS_xinit),calc{k}(:,Yxs_cum)];
yxs_fit=vertcat(yxs_fit, temp);
end
 hold off


 %% put data into one sheet
 close all
plot(yxs_fit(:,1), yxs_fit(:,2))
xlabel('cumS_xinit','fontsize',12,'FontName','Arial','FontAngle','italic')
ylabel('Yxs_cum','fontsize',12,'FontName','Arial','FontAngle','italic')

 
%% Fit


    
     
     x= [];
     y=[];
     x=yxs_fit(:,1);                    %x values for calculation of linear regression
     %y=tecan_data(i,:)';             %y for calculation of linear regression
     y=yxs_fit(:,2);
     xlog=log(x);                    %log the independent variable (time)
     
     %plot(1./xlog(2:end),y(2:end),'o')
     xx= [xlog(2:end),ones(size(xlog,1)-1,1)];
     size(xx)
     size(y(2:end))
     a=regress(y(2:end),xx);
          
     yfit=a(1).*xx(:,1)+a(2);        %calculate values based on fit
     
     %plot original data and fitted data
   figure(5);  
   hold on

     %set(figure(j),'name', groups{j});
     plot(x,y, 'color','b','Marker','.')
     plot(x(2:end),yfit, 'color','r')
    xlabel('cumS_{xinit} [g/g]','fontsize',12,'FontName','Arial','FontAngle','italic')
    ylabel('Yxs_{cum} [g/g]','fontsize',12,'FontName','Arial','FontAngle','italic')

    hold off
   %% save
    cellstr(name)
    Experiment_fit=[];

    equation='a(1).*log(dSn)+a(2)';
    Experiment_fit{1,1}='a1';
    Experiment_fit{1,2}=a(1);
    Experiment_fit{4,1}='a2';
    Experiment_fit{4,2}=a(2);
    Experiment_fit{6,1}='equation';
    Experiment_fit{6,2}=equation;
    save(['Fit_Exp_' name], 'Experiment_fit')
