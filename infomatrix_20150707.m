% File for determination of characteristic process variables
clc, clear all, close all

% addpath('S:\BPT\BPT SHARED DATA\Projects\L_Feeding Strategies\02_Material_Methods\Matlab-Scripts');
% addpath('S:\BPT\wreichelt\Matlab\biovt_dataprocessing');
addpath('C:\Users\MarkusMelanie\Documents\Uni\Masterarbeit\matlab_scripts\scripts workspace');
cd('C:\Users\MarkusMelanie\Documents\Uni\Masterarbeit\exp_data');
%% 1. input
%a.) make sure updated Eval files are in the Data Pool file!

workpackage='osc_w_qs_OUR';                     %specify additional identifier for excel infomatrix version

Eval_group=[31 33 35 37];              %specify the identifier of the selected experiments
% Eval_group=[37];

Reactor={'A' 'B' 'C' 'D'};       %specify the subspecifier of each evaluation group
Pool=struct;
const_Pool=struct;

for j = 1:length(Eval_group)
        Ex=Eval_group(j);
        load(['Eval_Ex_WR' num2str(Ex)]);
        Fex=fieldnames(Ex_group_A.corr);        %get name of experiments
        
      for k=1:length(Fex)                       %reorganize structure according to each experiment
        Fexn=Fex{k};
        exp= [num2str(Ex) num2str(Reactor{k})];
        Pool.(['WR' exp]).calc=Ex_group_A.calc.(Fexn);
        Pool.(['WR' exp]).raw_inter=Ex_group_A.raw_inter.(Fexn);
        Pool.(['WR' exp]).offline_raw=Ex_group_A.offline_raw.(Fexn);
        const_Pool.(['WR' exp])=Ex_group_A.constants;
      end
      
end


%kick unwanted experiments e.g. according to database
delete={'WR31A' 'WR31B' 'WR33B' 'WR33C' 'WR33D'};
Pool=rmfield(Pool,delete);


%c.) select time or substrate for x axsis     
    %x='time';
%      x='cumS_xinit';
     x='dSn_OUR';
 %% still to integrate   
% %d.) eventually select maximal time or substrate manually   
%     x_end.cumS_xinit_min=[];       
%     %x_end.cumS_xinit_min=3;     %de-comment for manual limit settings
%     x_end.time_min=[];
%     %x_end.time_min=50;          %de-comment for manual limit settings
%     %otherwise x achsis is time
    


%% analyse length of x-achsis to find maximum common of all experiments


if isfield(Pool, 'xend');
Pool=rmfield(Pool,'xend');
end

Fex=[];
Fex=fieldnames(Pool);

for i=1:length(Fex)
    Fexn=Fex{i};
    
    if  isfield(Pool.(Fexn),'x_end');
        Pool.(Fexn)=rmfield(Pool.(Fexn),'x_end');
    end
    
    Fstr=[];
    Fstr =fieldnames(Pool.(Fexn));
    min_time_all=[];
    ind_exp=Pool.(Fexn).calc.time(1,1);
    
    for j=1:length(Fstr)
        
        Fstrn=Fstr{j};
        Fvar=fieldnames(Pool.(Fexn).(Fstrn));
        min_time.(Fstrn)=[];
        
        
        for k=1:length(Fvar)
            Fvarn=Fvar{k};
            
            if strcmp(Fvarn, 'DCW')&& strcmp(Fstrn,'offline_raw')
                
            Pool.(Fexn).offline_raw.(Fvarn)(1,:)=[];                                      %delete End Batch data point so that offline starts with EFB
            end
            Pool.(Fexn).(Fstrn).(Fvarn)(:,1)=Pool.(Fexn).(Fstrn).(Fvarn)(:,1)-ind_exp;       %set starting time to 0
            min_time.(Fstrn)(k,1)=max(Pool.(Fexn).(Fstrn).(Fvarn)(:,1));                     %find the maximum time range
        end
        
        min_time_all=[min_time_all; min_time.(Fstrn)];
        
    end
    
    Pool.(Fexn).x_end.time=min(min_time_all);                                %minimal time of all variables in one experiment
    diff=abs(Pool.(Fexn).calc.dSn_OUR(:,1)-Pool.(Fexn).x_end.time);           %the corresponding dSn to the minimal timepoint
    dSn_ind=find(diff == min(diff));
    Pool.(Fexn).x_end.dSn=Pool.(Fexn).calc.dSn_OUR(dSn_ind,2);      %minimal time achsis of all contained variables
    
    
end


%    write into one vector
for i=1:length(Fex)
    
    Fexn=Fex{i};
    Pool.xend.dSn(i)=Pool.(Fexn).x_end.dSn;
    Pool.xend.time(i)=Pool.(Fexn).x_end.time;
    
end

%    find smallest common
if min(Pool.xend.dSn)> 4
Pool.xend.dSn_min=4;
Pool.xend.time_min=min(Pool.xend.time);
else
Pool.xend.dSn_min=min(Pool.xend.dSn);           %minimal dSn over all experiments
Pool.xend.time_min=min(Pool.xend.time);         %minimal time over all experiments
end


%   save indices for common endpoint (dSn dependent on qs!)

dSn_ind=[];
time=[];
Pool.xend.time_ind_all=[];
Pool.xend.dSn_ind_all=[]; 

for i=1:length(Fex)
    
    Fexn=Fex{i};
    Fstr=[];
    Fstr =fieldnames(Pool.(Fexn));
    
    if  strcmp(Fexn,'xend');
    else
        diff = abs(Pool.(Fexn).calc.dSn_OUR(:,2)-Pool.xend.dSn_min);   %find the best fitting dSn according to the global dSn maximum
        dSn_ind=find(diff == min(diff));            %find the index of the minimum of the difference function
        dSn_h=Pool.(Fexn).calc.dSn_OUR(dSn_ind,1);
           
     
        for j=1:length(Fstr)
            
            Fstrn=Fstr{j};
            Fvar=fieldnames(Pool.(Fexn).(Fstrn));
            
            
            
            
            for k=1:length(Fvar)
                
                Fvarn=Fvar{k};
                diff = abs(Pool.(Fexn).(Fstrn).(Fvarn)(:,1)-dSn_h);   %find the best fitting time according to the global dSn maximum
                dSn_h_ind=find(diff == min(diff));
                Pool.xend.dSn_ind_all.(Fexn).(Fstrn).(Fvarn) = dSn_h_ind;  %save the index of the minimum of the difference function specifically for each variable since length of variables is different - and some do not start with 0
                
                
                diff = abs(Pool.(Fexn).(Fstrn).(Fvarn)(:,1)-Pool.xend.time_min);   %find the best timepoint according to the global time maximum
                time_ind=find(diff == min(diff));
                Pool.xend.time_ind_all.(Fexn).(Fstrn).(Fvarn) = time_ind;
                
            end
        end
        
        
    end
end

%% mean calculation /weighted Mean calc

%process parameter
%simple mean calculation within the defined phase

p_var={'pH','Temp', 'DO2', 'gassing','rS','rS_g', 'CER', 'OUR', 'RQ', 'dS_g', 'dS', 'vR', 'Yco2s','Yo2s','Yco2s_cum', 'cumS_xinit'};

%reconciled values
%simple mean calculation within the defined phase
rec_var={'BM_rec','rX_rec','rS_rec', 'my_rec', 'qs_rec','Yxs_rec', 'Yco2s_rec', 'Yo2s_rec','CER_rec', 'OUR_rec', 'h_value' };

%physiological values (contains BM measurements)
%calculation only as mean between two datapoints of BM measuremnts then weighted
physiol_var={'rX', 'C_bal_offline', 'DoR_bal_offline', 'qs', 'qs_cum', 'DCW', 'BM_g', 'BM_mol', 'my', 'Yxs', 'dSg_BMmol', 'Yco2x_cum', 'qo2', 'qco2', 'C_bal_offline'};

%Cumulative C-Balance (only end point is taken into account)
C_balance={'C_bal_cum', 'Yxs_OUR_cum', 'Yxscorr_cum', 'Yxs_cum'};

%product related values (contains product measurements)
%calculation only as mean between two datapoints of product measurement then weighted
product_var={'rpp', 'Ypps', 'Ypps_cum', 'qpp'};

% Rates and Yields corrected with supernatant measurements
corr_var={'rScorr', 'qscorr', 'cumS_xinitcorr', 'Yxscorr'};

titer_var={'PPel_g', 'PSPel', 'Prod_tot'};



Start=1;        %common for all experiments since NANs have been cleared
Stop=20;
descr=struct;
x='dSn_ind_all';
% x='time_ind_all';


Fex=fieldnames(Pool);
descr.(Fexn).online=struct;

for i=1:length(Fex)
    
    Fexn=Fex{i};
    Fstr =fieldnames(Pool.(Fexn));
    
    if  strcmp(Fexn,'xend');        %skip xend
    else
        
        for j=1:length(Fstr)            %loop through variable sets
            Fstrn=Fstr{j};
            
            Fvar=fieldnames(Pool.(Fexn).(Fstrn));
            
            if  strcmp(Fstrn,'x_end');
            else
                for k=1:length(Fvar)        %loop through variables
                    Fvarn=Fvar{k};
                    stop=Pool.xend.(x).(Fexn).(Fstrn).(Fvarn);     %asign end index for every variable according to maximum common time or to maximum common dSn (x)

                    if ismember(Fvarn, p_var)           %loop through process values
                        descr.(Fexn).online.(Fvarn) = mean(Pool.(Fexn).(Fstrn).(Fvarn)(Start:stop,2),1);

                    elseif ismember(Fvarn, rec_var)     %loop through reconciled variables
                        descr.(Fexn).online.(Fvarn)= mean(Pool.(Fexn).(Fstrn).(Fvarn)(Start:stop,2));

                    elseif ismember(Fvarn, physiol_var) %loop through physiological variables based on biomass

                        ind_off= find((Pool.(Fexn).offline_raw.DCW(:,1)- Pool.(Fexn).calc.(Fvarn)(stop,1))<0);                        %find endpoint of offline data according to global maximum
                        var_ind=[];   

                        for z=1:length(ind_off)
                            diff=[];
                            diff = abs(Pool.(Fexn).(Fstrn).(Fvarn)(:,1)-Pool.(Fexn).offline_raw.DCW(ind_off(z),1));         %index of timepoints where offline DCW data is existing
                            temp=find(diff == min(diff));
                            var_ind(z)=temp(1);
                        end

                        var_temp=[Pool.(Fexn).(Fstrn).(Fvarn)(var_ind,:);Pool.(Fexn).(Fstrn).(Fvarn)(stop,:)];             %substitute end offline value by online
                        dt=Pool.xend.time_min;                                                                            %total time span of new BM vector

                        incr=[];
                        for f=1:length(var_temp)-1                                                               %calculate mean time increment
                            incr(f,1)= abs(var_temp(f,1)-var_temp(f+1,1))/dt;                                    %time increment
                            incr(f,2)=(var_temp(f,2)+var_temp(f+1,2))./2;                                        %mean value

                        end

                        descr.(Fexn).phys.(Fvarn)= sum(incr(:,1).*incr(:,2));                                    % calculate the descriptor


                    elseif ismember(Fvarn, product_var)             %loop through product variables based on product measurement

                        ind_off= find((Pool.(Fexn).offline_raw.product(:,1)- Pool.(Fexn).calc.(Fvarn)(stop,1))<0);                        %find endpoint of offline data according to global maximum
                        var_ind=[];

                        for z=1:length(ind_off)
                            diff = abs(Pool.(Fexn).(Fstrn).(Fvarn)(:,1)-Pool.(Fexn).offline_raw.product(ind_off(z),1));         %index of timepoints where offline DCW data is existing
                            temp=find(diff == min(diff));
                            var_ind(z)=temp(1);
                        end

                        var_temp=[Pool.(Fexn).(Fstrn).(Fvarn)(var_ind,:);Pool.(Fexn).(Fstrn).(Fvarn)(stop,:)];             %substitute end offline value by online
                        dt=Pool.xend.time_min;                                                                          %total time span of new BM vector

                        incr=[];
                        for f=1:length(var_temp)-1                                                               %calculate mean time increment
                            incr(f,1)= abs(var_temp(f,1)-var_temp(f+1,1))/dt;                                    %time increment
                            incr(f,2)=(var_temp(f,2)+var_temp(f+1,2))./2;                                        %mean value

                        end

                        descr.(Fexn).prod.(Fvarn)= sum(incr(:,1).*incr(:,2));                                    % calculate the descriptor

                    elseif ismember(Fvarn, C_balance)             %loop through product variables based on product measurement

                        descr.(Fexn).C_bal.(Fvarn)= Pool.(Fexn).(Fstrn).(Fvarn)(stop,2);                                    % calculate the descriptor

                    elseif ismember(Fvarn, titer_var)             %loop through product variables based on product measurement

                        descr.(Fexn).titer.(Fvarn)= Pool.(Fexn).(Fstrn).(Fvarn)(stop,2);                                    % calculate the descriptor
                    
                    elseif ismember(Fvarn, corr_var)             %loop through product variables based on product measurement

                        ind_off= find((Pool.(Fexn).offline_raw.product(:,1)- Pool.(Fexn).calc.(Fvarn)(stop,1))<0);                        %find endpoint of offline data according to global maximum
                        var_ind=[];

                        for z=1:length(ind_off)
                            diff = abs(Pool.(Fexn).(Fstrn).(Fvarn)(:,1)-Pool.(Fexn).offline_raw.product(ind_off(z),1));         %index of timepoints where offline DCW data is existing
                            temp=find(diff == min(diff));
                            var_ind(z)=temp(1);
                        end

                        var_temp=[Pool.(Fexn).(Fstrn).(Fvarn)(var_ind,:);Pool.(Fexn).(Fstrn).(Fvarn)(stop,:)];             %substitute end offline value by online
                        dt=Pool.xend.time_min;                                                                          %total time span of new BM vector

                        incr=[];
                        for f=1:length(var_temp)-1                                                               %calculate mean time increment
                            incr(f,1)= abs(var_temp(f,1)-var_temp(f+1,1))/dt;                                    %time increment
                            incr(f,2)=(var_temp(f,2)+var_temp(f+1,2))./2;                                        %mean value

                        end

                        descr.(Fexn).corr.(Fvarn)= sum(incr(:,1).*incr(:,2));                                    % calculate the descriptor

                    
                    end
                end
            end
        end
    end
end


%%                 elseif ismember(Fvarn, C_balance)             %loop through product variables based on product measurement
%                     
%                     ind_off= find((Pool.(Fexn).offline_raw.product(:,1)- Pool.(Fexn).calc.(Fvarn)(stop,1))<0);                        %find endpoint of offline data according to global maximum
%                     var_ind=[];
%                  
%                     for z=1:length(ind_off)
%                         diff = abs(Pool.(Fexn).(Fstrn).(Fvarn)(:,1)-Pool.(Fexn).offline_raw.product(ind_off(z),1));         %index of timepoints where offline DCW data is existing
%                         temp=find(diff == min(diff));
%                         var_ind(z)=temp(1);
%                     end
%                     
%                     var_temp=[Pool.(Fexn).(Fstrn).(Fvarn)(var_ind,:);Pool.(Fexn).(Fstrn).(Fvarn)(stop,:)];             %substitute end offline value by online
%                     dt=Pool.xend.time_min;                                                                          %total time span of new BM vector
%                     
%                     incr=[];
%                     for f=1:length(var_temp)-1                                                               %calculate mean time increment
%                         incr(f,1)= abs(var_temp(f,1)-var_temp(f+1,1))/dt;                                    %time increment
%                         incr(f,2)=(var_temp(f,2)+var_temp(f+1,2))./2;                                        %mean value
%                         
%                     end
%                     
%                     descr.(Fexn).phys.(Fvarn)= sum(incr(:,1).*incr(:,2));                                    % calculate the descriptor


%% check size of descr for every experiment and write 0 for non existing descriptors

% Fex=[];
% Fex=fieldnames(descr);
% var=[];
% 
% for i=1:length(Fex)
%     
%     Fexn=Fex{i};
%     if not(isfield(descr.(Fexn),'online'))
%         for k=1:length(p_var)
%             var=[];
%             var=p_var(1,k);
%             
%         end    
%     end
%     
%     
% end
%% apend custom descriptors

%% oscillation evaluation

Fex=[];
Fex=fieldnames(Pool);

run('amplitude1.m');

Fex=[];
Fex=fieldnames(Pool);

for i=1:length(Fex)
    Fexn=[];
    Fexn=Fex{i};
    if strcmp(Fexn,'xend')        %skip xend   
    else
    descr.(Fexn).custom.qs_mean=qs_amp{i}(1,2);
    descr.(Fexn).custom.amplitude=qs_amp3{i}(1,3);
    end
end 



%% % %% restructure descr to write in Excel
excel=[];
Fex=fieldnames(descr);
excel.online_header=[];
excel.phys_header=[];
excel.corr_header=[];
excel.prod_header=[];
excel.C_bal_header=[];
excel.cust_header=[];
excel.titer_header=[];

for i=1:length(Fex)
    
    Fexn=Fex{i};
   
    
    %online values 
    if isfield(descr.(Fexn),'online')
        Fvar=[];
        Fvar =fieldnames(descr.(Fexn).online);
    
            if isempty(excel.online_header)
            excel.online_header=Fvar;
            elseif not(isequaln(excel.online_header,Fvar))
                msgbox('variable online missfit')
            end
            
        for k=1:length(Fvar)
            Fvarn=Fvar{k};
            if strcmp(Fvarn,'DCW')                                 %DCW is calc with offline data
              
            else
            excel.online(i,k)=descr.(Fexn).online.(Fvarn);
            end
        end
    end
    
    
    %physiologic values
    if isfield(descr.(Fexn),'phys')
        Fvar=[];
        Fvar =fieldnames(descr.(Fexn).phys);
            
            if isempty(excel.phys_header)
            excel.phys_header=Fvar;
            elseif not(isequaln(excel.phys_header,Fvar))
                msgbox('variable phys missfit')
            end

        for k=1:length(Fvar)
            Fvarn=Fvar{k};
            excel.phys(i,k)=descr.(Fexn).phys.(Fvarn);

        end
    end
    
    if isfield(descr.(Fexn),'corr')
        Fvar=[];
        Fvar =fieldnames(descr.(Fexn).corr);
            
            if isempty(excel.corr_header)
            excel.corr_header=Fvar;
            elseif not(isequaln(excel.corr_header,Fvar))
                msgbox('variable corr missfit')
            end

        for k=1:length(Fvar)
            Fvarn=Fvar{k};
            excel.corr(i,k)=descr.(Fexn).corr.(Fvarn);

        end
    end 
    
    
        %product values
    if isfield(descr.(Fexn),'prod')
        Fvar=[];
        Fvar =fieldnames(descr.(Fexn).prod);
            
            if isempty(excel.prod_header)
            excel.prod_header=Fvar;
            elseif not(isequaln(excel.prod_header,Fvar))
                msgbox('variable prod missfit')
            end

        for k=1:length(Fvar)
            Fvarn=Fvar{k};
            excel.prod(i,k)=descr.(Fexn).prod.(Fvarn);

        end
    end
    
    
    if isfield(descr.(Fexn),'titer')
        Fvar=[];
        Fvar =fieldnames(descr.(Fexn).titer);
            
            if isempty(excel.titer_header)
            excel.titer_header=Fvar;
            elseif not(isequaln(excel.titer_header,Fvar))
                msgbox('variable titer missfit')
            end

        for k=1:length(Fvar)
            Fvarn=Fvar{k};
            excel.titer(i,k)=descr.(Fexn).titer.(Fvarn);

        end
    end

    
     if isfield(descr.(Fexn),'C_bal')
            Fvar=[];
            Fvar=fieldnames(descr.(Fexn).C_bal);

                if isempty(excel.C_bal_header)
                excel.C_bal_header=Fvar;
                elseif not(isequaln(excel.C_bal_header,Fvar))
                    msgbox('variable C balance missfit')
                end

            for k=1:length(Fvar)
                Fvarn=Fvar{k};
                excel.C_bal(i,k)=descr.(Fexn).C_bal.(Fvarn);

            end
     end
     
     
     if isfield(descr.(Fexn),'custom')
        Fvar=[];
        Fvar =fieldnames(descr.(Fexn).custom);
            
            if isempty(excel.cust_header)
            excel.cust_header=Fvar;
            elseif not(isequaln(excel.cust_header,Fvar))
                msgbox('variable custom missfit')
            end

        for k=1:length(Fvar)
            Fvarn=Fvar{k};
            excel.cust(i,k)=descr.(Fexn).custom.(Fvarn);

        end
     end 
end

% align in one file
    
excel.all=[];
excel.all=[excel.online,excel.phys,excel.corr,excel.prod,excel.titer,excel.C_bal,excel.cust];
excel.all_header=[excel.online_header;excel.phys_header;excel.corr_header;excel.prod_header;excel.titer_header;excel.C_bal_header;excel.cust_header]';
%% 6. output

xlswrite([date '_infomatrix_' x 'FABIB' workpackage],excel.all_header);
xlswrite([date '_infomatrix_' x 'FABIB' workpackage], excel.all,'Tabelle1','A3');



%%
    %find timepoint of highest spec titers
     x_max_titer=struct;
     x_max_titer.time=[];
     x_max_titer.dSn=[];
     x_max_titer.index=[];
    
     
     x='time';
    for i=1:length(Ferm)
        cc=Ferm{i};
        x_max_titer.time(i,2)= max(InPh.(cc).('PSPel')(1:x_end.(x).index(i),2)); 
        diff=abs(InPh.(cc).('PSPel')(:,2)-x_max_titer.time(i,2));
        x_max_titer.index(i,:)=find(diff == min(diff));  %->min klappt nicht
        x_max_titer.time(i,1)=InPh.(cc).('PSPel')(x_max_titer.index(i,:),1);
        
    end
    
    
     x='cumS_xinit';
    for i=1:length(Ferm)
        cc=Ferm{i};
        x_max_titer.dSn(i,2)= max(InPh.(cc).('PSPel')(1:x_end.(x).index(i),2)); 
        diff=abs(InPh.(cc).('PSPel')(:,2)-x_max_titer.dSn(i,2));
        x_max_titer.index(i,:)=find(diff == min(diff));  %->min klappt nicht
        x_max_titer.dSn(i,1)=InPh.(cc).('cumS_xinit')(x_max_titer.index(i,:),2);
        
    end
    
    %% get mean product content
    x_mean.spec_titer=struct;
    x_mean.spec_titer.time=[];
    x_mean.spec_titer.dSn=[];
    x_mean.titer.time=[];
    x_mean.titer.dSn=[];    
    
      x='time';
    for i=1:length(Ferm)
        cc=Ferm{i};
        x_mean.spec_titer.time(i,1)=i;
        x_mean.spec_titer.time(i,2)= mean(InPh.(cc).('PSPel')(1:x_end.(x).index(i),2)); 
  
        x_mean.titer.time(i,1)=i;
        x_mean.titer.time(i,2)= mean(InPh.(cc).('PPel')(1:x_end.(x).index(i),2)); 
    end
    
    
      x='cumS_xinit';
    for i=1:length(Ferm)
        cc=Ferm{i};
        x_mean.spec_titer.dSn(i,1)=i;
        x_mean.spec_titer.dSn(i,2)= mean(InPh.(cc).('PSPel')(1:x_end.(x).index(i),2)); 
   
        x_mean.titer.dSn(i,1)=i;
        x_mean.titer.dSn(i,2)= mean(InPh.(cc).('PPel')(1:x_end.(x).index(i),2)); 
    end
