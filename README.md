# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Data processing routine for microbial Bioprocesses
* 0.5
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

## Summary of set up ##

Two categories of Matlab tools:
 - - - - - - - 
#### fermentation data processing####

* data import file "data_import_generic"
    * _Aim: import fermentation data and save fermentation parameters_
    * import online data and convert it into units to fit handshake to rate calculation
    * all variables are stored in objects
    * NANs are cleared from offline files -> so only insert 0 where you actually have measured 0 otherwise leave field empty
    * find induction time
    * set constants parameters to memorize feed concentration/objectives etc.
    * yields a online_raw and a offline_raw file

     
* rate calculation "RateCalculation_wo_dataimport"  
    * _Aim: time dependent calculations_  
    * Data import from struct
    * interpolated values  -> stored as objects togehter with time variable raw struct
    * Calc struct contains all variables which have been calculated from the interpolated online data (raw)
       * (constant/mean values are excluded) e.g. rates/yield/qs  
   
* infomatrix calulation "infomatrix"
    * _Aim: calulation of time independent descriptors_   
    * imports all selected experiments   
    * interpolates all data on one timeachsis   
    * clears NANs   
    * calculates mean of variables within a cerain time/consumed substrate frame   

    - - - - - - - -
#### analytical data processing ####
     
* Tecan data Import    
    * _Aim: Import/Process Tecan Data_   
    * import Data from Tecan file and attribute it to a certain layout (at some point presumably including a graphical interface)
    * Data processing e.g. Fit of log function or yield calculation   
      
   
* Import NTA Data
    * _Aim: Import/Process Tecan Data_   
    * import and calculate numerical value for size of IBs from nano particle tracking analysis

   
   --------------------------
  
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ##


* Writing code   
    * _general: coding in development branch_
    * new tool:     
               - make a new branch      
               - upon function test merge into development branch      
    * new function:    
               - make a new feature    
               - upon function test merge  

* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
Wieland Reichelt
**admin - repository management


* Other community or team contact