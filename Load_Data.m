% Load Selected Data and text from Excel File

clear all; %Clear workspace 

% path=['S:\CDLMIB\CD-L\03_RT3\03_Results\Data Pool'];    %specify path of experimental data
% path=['S:\BPT\BPT SHARED DATA\Projects\L_Feeding Strategies\03_Results\Experiments\' Experiment work_package '\Matlab Auswertung\'];    %specify path of experimental data
path=['S:\BPT\BPT SHARED DATA\Projects\L_Feeding Strategies\05_Publications\Independency of FB-induction phase'];    %specify path of experimental data
addpath('S:\BPT\wreichelt\Matlab\biovt_dataprocessing');        %specify path of matlab files
cd(path);
%%
filename = '30-May-2014_infomatrix_Fab1_WP2_processed2.xls';
sheet_data = 'selection_time PCA (3)' ; %specify Sheet where data can be found 
[ndata, text, alldata] = xlsread(filename, sheet_data); % Load Data
headers = text; % identify headers
Data = ndata; % Here goes the numerical data

% Clear non-used variables
clear filename;
clear sheet_data;
clear ndata;
clear text;