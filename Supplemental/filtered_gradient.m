function [y2,x2] = filtered_gradient(x1,y1,DOrder);
%
%  -----------------------------------------------
%  usage : 
%            [x2,y2] = filtered_gradient(x1,y1,O);
%
%  parameters :
%
%   x1 : x values (x2 will be cropped by 2 times O)
%   y1 : function values to be list to differenciated
%   O  : order of the Filter (an odd integer number)
% 
%  IMPORTANT : x1-values have to be aequidistant !
%
%  -----------------------------------------------
%
  NumPoints = length(y1);
  h = x1(2)-x1(1);
  y2=[];
  m = (DOrder-1)/2;
  for i=m+1:NumPoints-m   
    NewValue = 0;
    for k = 1:m
      %printf("\n NumPoints %d und %d ",i+k,i-k)
      NewValue = NewValue + 3/h * k * (y1(i+k) - y1(i-k)) / (m * (m+1) * (2*m+1));
    end
    y2 = [y2,NewValue];
  end
  x2=x1(m+1:NumPoints-m); 
end
