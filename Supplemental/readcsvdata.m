function dataArray = readcsvdata( v )
%UNTITLED11 Summary of this function goes here
%   Detailed explanation goes here

fid = fopen(v,'r');


% count total number of lines in the input filee for waitbar
lines=0; % line number
tline = fgets(fid);
while ischar(tline)
    lines=lines+1;
    tline = fgets(fid);
end

frewind(fid);

header = fgets(fid); % header

num=findstr(header,',');
rows=length(num);

num=[1 num];
num=diff(num);

unit = fgets(fid); % first line
un=findstr(unit,',');
un=[1 un];
un=diff(un);

z=length(num);

for j=1:z
    headerall{1,j}=header(1:num(1));
    headerall{1,j}=strrep(headerall{1,j},' ','_'); 
    if un(1)>0
    unitall{1,j}=unit(1:un(1));
    unit(1:un(1))=[];
    else
    unitall{1,j}='[-]';    
    end
        
    header(1:num(1))=[];
    
    if length(num)>1
    num(1)=[];
    un(1)=[];
    
    num(1)=num(1)-1;
    un(1)=un(1)-1;
    
    header(1)=[];
    unit(1)=[];
    end
    
end

    header(1)=[];
    unit(1)=[];
    
    j=z+1;
    headerall{1,j}=header;
    headerall{1,j}(end)=[];
   
    unitall{1,j}=unit;
    
fclose(fid);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%

delimiter = ',';
%if nargin<=2
    startRow = 19;
    endRow = inf;
%end
formatSpec=[];
for j=1:length(headerall)

formatSpec =[formatSpec '%s']; 
end
formatSpec=[formatSpec '%[^\n\r]'];

fileID = fopen(v,'r');

dataArray = textscan(fileID, formatSpec, endRow(1)-startRow(1)+1, 'Delimiter', delimiter, 'HeaderLines', startRow(1)-1, 'ReturnOnError', false);

fclose(fileID);