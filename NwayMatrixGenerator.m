%% Create 3 way matrix
% Load Calc Struct File
WR5 = calc
clear calc
%% Load Second Calc Struct File
WR7 = calc
clear calc

%% Put Batches in Matrix
F1 = WR5 {1,1}
F2 = WR5 {1,2}
F3 = WR5 {1,3}
F4 = WR5 {1,4}
F5 = WR7 {1,1}
F6 = WR7 {1,2}
F7 = WR7 {1,3}
F8 = WR7 {1,4}

clear WR7
clear WR5

%% what you want
header = {'qs', '�' 'Yxs' 'RQ' 'qpp' 'qps'}; 

%% take what you want
%   'time'    'qs',     '�'     'Yxs'     'RQ'    'qpp'     'qps'};
F1 = [F1(:,1) F1(:,13) F1(:,14) F1(:,15) F1(:,18) F1(:,44) F1(:,49)] ;
F2 = [F2(:,1) F2(:,13) F2(:,14) F2(:,15) F2(:,18) F2(:,44) F2(:,49)] ;
F3 = [F3(:,1) F3(:,13) F3(:,14) F3(:,15) F3(:,18) F3(:,44) F3(:,49)] ;
F4 = [F4(:,1) F4(:,13) F4(:,14) F4(:,15) F4(:,18) F4(:,44) F4(:,49)] ;
F5 = [F5(:,1) F5(:,13) F5(:,14) F5(:,15) F5(:,18) F5(:,44) F5(:,49)] ;
F6 = [F6(:,1) F6(:,13) F6(:,14) F6(:,15) F6(:,18) F6(:,44) F6(:,49)] ;
F7 = [F7(:,1) F7(:,13) F7(:,14) F7(:,15) F7(:,18) F7(:,44) F7(:,49)] ;
F8 = [F8(:,1) F8(:,13) F8(:,14) F8(:,15) F8(:,18) F8(:,44) F8(:,49)] ;

%% Remove NaNs
F1 = F1(0== sum(isnan(F1), 2),:);
F2 = F2(0== sum(isnan(F2), 2),:);
F3 = F3(0== sum(isnan(F3), 2),:);
F4 = F4(0== sum(isnan(F4), 2),:);
F5 = F5(0== sum(isnan(F5), 2),:);
F6 = F6(0== sum(isnan(F6), 2),:);
F7 = F7(0== sum(isnan(F7), 2),:);
F8 = F8(0== sum(isnan(F8), 2),:);

%% Create new time axis
c =(min(F1,[],1)); % Start for interpolation is lowest value
start = c(1,1); % 
c =(max(F1,[],1)); %end of interpolation is 
ende = c(1,1);
axis = (start: 0.1: ende);
axis = axis'

%% Interpolate Data on new time axis
F1 =  interp1(F1(:,1),F1(:,(2:7)),axis,'pchip')

%%
F2 =  interp1(F2(:,1),F2(:,(2:7)),axis,'pchip')
F3 =  interp1(F3(:,1),F3(:,(2:7)),axis,'pchip')
F4 =  interp1(F4(:,1),F4(:,(2:7)),axis,'pchip')
F5 =  interp1(F5(:,1),F5(:,(2:7)),axis,'pchip')
F6 =  interp1(F6(:,1),F6(:,(2:7)),axis,'pchip')
F7 =  interp1(F7(:,1),F7(:,(2:7)),axis,'pchip')
F8 =  interp1(F8(:,1),F8(:,(2:7)),axis,'pchip')
%%
Matrix = [];
Matrix (:,:,1) = F1;
Matrix (:,:,2) = F2;
Matrix (:,:,3) = F3;
Matrix (:,:,4) = F4;
Matrix (:,:,5) = F5;
Matrix (:,:,6) = F6;
Matrix (:,:,7) = F7;
Matrix (:,:,8) = F8;
%%
clearvars -except Matrix

